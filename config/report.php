<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\pjax;
use app\models\Bill;
use app\models\BillSearch;
use kartik\daterange\DateRangePicker; 
/* @var $this yii\web\View */
/* @var $searchModel app\models\BillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'bill-report';
$this->params['breadcrumbs'][] = $this->title;
?>

 <?php  echo $this->render('_billsearch', ['model' => $searchModel]); ?>



<link href="<?= Yii::$app->homeUrl?>js/DataTables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<br>
 <div class="row">
        
      
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">

                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                            No of bills issued    </div>

                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$tenure?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
  
      
       <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">No of Paid bills 
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$subsector?></div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
                                </div>
                               
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        


        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">No of outstanding bills 
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$pending?></div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
                                </div>
                               
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">

                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                               	Total amount  </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$total_amount?></div>
                        </div>
                        <div class="col-auto"><p class="h5 mb-0 font-weight-bold text-gray-800">&#8360;</p>
                            <i  class=" fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

         <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">

                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                               	Total Collection  </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$total_paidamount?></div>
                        </div>
                        <div class="col-auto"><p class="h5 mb-0 font-weight-bold text-gray-800">&#8360;</p>
                            <i  class=" fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

          <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">

                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                               	Total outstanding amount  </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$total_unpaidamount?></div>
                        </div>
                        <div class="col-auto"><p class="h5 mb-0 font-weight-bold text-gray-800">&#8360;</p>
                            <i  class=" fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> 
         

 <div class="plot-view">
     <div class="container-fluid">
     <div class="card shadow mb-4"> 
    
              <div class="card-body">
              <div class="table-responsive">
              <table class="table table-bordered" id="table" width="100%" cellspacing="0">
                      <thead>
                          <tr>
                                <th>No. of Bills</th>
                                <th>Paid Bills</th>
                                <th>OUTSTANDING BILLS</th>
                                <th>TOTAL AMOUNT</th>
                                <th>TOTAL PAID AMOUNT</th>
                                <th>TOTAL UN-PAID AMOUNT</th>
                              
                          </tr>
                        </thead>
                        <tbody>
                         
                              <tr>
                                  <td><?= $tenure?></td>
                                  <td><?= $subsector?></td>
                                  <td><?= $pending?></td>
                                  <td><?= $total_amount?></td>
                                  <td><?= $total_paidamount?></td>
                                  <td><?= $total_unpaidamount?></td>
 
                              </tr>
                        </tbody>        
                        </table>
                       
                            </div>
                        </div>
                    </div>

                </div>
            </div>

<script src="<?= Yii::$app->homeUrl?>js/jQuery/jquery-3.3.1.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/datatables.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/Buttons/js/buttons.dataTables.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/Buttons/js/buttons.print.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/pdfmake/pdfmake.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/JSZip/jszip.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/Buttons/js/buttons.html5.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/pdfmake/vfs_fonts.js"></script>
<script type="text/javascript">
   $('#table').DataTable({
   dom: 'Bfrtip',

   buttons: [ {extend: 'print', className: 'btn-outline-primary'},
              {extend: 'excel', className: 'btn-outline-warning'},
              {extend: 'pdf', className: 'btn-outline-danger'} ]

});
</script>