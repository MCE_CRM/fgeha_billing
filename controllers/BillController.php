<?php

namespace app\controllers;

use Yii;
use app\models\Bill;
use app\models\SubSectors;
use app\models\Sectors;
use app\models\BillSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Activity; 
use app\models\Services; 
use\yii\web\UploadedFile; 

/**
 * BillController implements the CRUD actions for Bill model.
 */
class BillController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='bill/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Bill models.
     * @return mixed
     */
    public function actionIndex()
    {
      
        $searchModel = new BillSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bill model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = 'generatebill';

        $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' views the bill of consumer '.$model->consumer_id;
        $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
        return $this->render('view', [
            'val' => $model, 
        ]);
    }

    public function actionBarcode()
    {
        $model = new Bill();
        $data = '';
        $display = 'none';

        return $this->render('barCode_form', [
            'model' => $model,
            'data1' => $data,
            'dis' => $display,
        ]);

       
    }
 
     public function actionUpdatestatus()
    {

        //$model = new Bill();
        //$a= ($_POST['b_id']);
        $id = $_POST['bill_id'];
        $check = $_POST['check'];

     

        if ($check==0) {
            
        
        
      [$one, $two]=(explode("_", $id));
        //  echo "<pre>";
        // print_r($one);
        // exit;

       
     
        $model=Bill::findOne($one);
        
         
        if($model!= null)
        {
            
            Yii::$app->db->createCommand()->update('bill', ['status' => 1], ['id' =>$one])->execute();
           //Bill::update(['status' => 1], ['like', 'id', $id]);
           //$model->status=1;
           //$('#s-msg').show();
          
           $data = "Record Updated Successfully!";
           $display = "block";
           $t_bill = $model->total_bill;
           $model->payment = $t_bill;

           if($model->save())
           {

            return $this->render('barCode_form', [
                'model' => $model,
                'data1' => $data,
                'dis' => $display,
            ]);
           }
           else
           {

            $data = "Something Went Wrong!";
            $display = "block";

            return $this->render('barCode_form', [
                'model' => $model,
                'data1' => $data,
                'dis' => $display,
            ]);
           }

         }
         else
         {
             $data = "Record Not Found!";
             $display = "block";

             return $this->render('barCode_form', [
                'model' => $model,
                'data1' => $data,
                'dis' => $display,
            ]);
            
         }}else{

      [$one, $two]=(explode("_", $id));

       
     
        $model=Bill::findOne($one);
        
         
        if($model!= null)
        {
            
            Yii::$app->db->createCommand()->update('bill', ['status' => 1], ['id' =>$one])->execute();
           //Bill::update(['status' => 1], ['like', 'id', $id]);
           //$model->status=1;
           //$('#s-msg').show();
          
           $data = "Record Updated Successfully!";
           $display = "block";
           $t_bill = $model->total_after_due_date;
           $model->payment = $t_bill;

           if($model->save())
           {

            return $this->render('barCode_form', [
                'model' => $model,
                'data1' => $data,
                'dis' => $display,
            ]);
           }
           else
           {

            $data = "Something Went Wrong!";
            $display = "block";

            return $this->render('barCode_form', [
                'model' => $model,
                'data1' => $data,
                'dis' => $display,
            ]);
           }

         }
         else
         {
             $data = "Record Not Found!";
             $display = "block";

             return $this->render('barCode_form', [
                'model' => $model,
                'data1' => $data,
                'dis' => $display,
            ]);
            
         }
         }}
        
    
     public function actionSectorPrint()
    {
        
        return $this->render('sectorprint');
    } 

     public function actionUpdateduedate()
    {
          $model = new Bill();           
        if ($model->load(Yii::$app->request->post())) {
            
            
            $model->tinure = $_POST['Bill']['tinure'];
           

            $model->sector=$_POST['Bill']['sector'];

            $due_date = $_POST['due_date'];
             


            \Yii::$app->db->createCommand("UPDATE bill SET due_date= '$due_date'  WHERE tinure='$model->tinure' AND sector='$model->sector'")->execute();
            
        
           
            $model->save();
              
}
    
        return $this->render('updateduedate', [
            'model' => $model,
        ]);
    }
       

    /**
     * Creates a new Bill model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Bill();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $activitycreate = Yii::$app->user->identity->username.' Created the new bill for consumer '.$_POST['Bill']['consumer_id'];

           $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Bill model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
       
        $model = $this->findModel($id);
        $data = "Record Updated Successfully!";
        $display = "block";
        $olds= $model->document;
        $model->updated_by=Yii::$app->user->identity->id;
        $model->updated_on = date('Y-m-d H:i:s');
          // echo "<pre>";
          //           print_r($olds);
                    // exit();
        $old = json_decode($model->document);

        if (!empty($old)) {
           $new = implode(",",$old);

         if ($model->load(Yii::$app->request->post())) {
            $files =  $model->file;
            
            $model->file = UploadedFile::getInstances($model, 'file');
            if (!empty($model->file )) {

            if ($model->file && $model->validate()) {
                foreach ($model->file as $file) {
                   
                   
                $file->saveAs('uploads/' . $file->baseName . '.' . $file->extension);

                $names[]=$file->name;
                $names++;
                $total_names = $names;
                  
                }

                $merge=(array_merge($total_names,$old));
                $name = json_encode($merge);

                $model->document =  $name;
                   if ($model->save()) {
                    $model->updated_by=Yii::$app->user->identity->id;
                    $model->updated_on = date('Y-m-d H:i:s');
                 Yii::$app->session->setFlash('success', 'Record updated Successfully');
                   
               }
            }}}

    }else{
        


         if ($model->load(Yii::$app->request->post())) {
            $files =  $model->file;
            
            $model->file = UploadedFile::getInstances($model, 'file');
            if (!empty($model->file )) {

            if ($model->file && $model->validate()) {
                foreach ($model->file as $file) {
                    // $i++;   
                $file->saveAs('uploads/' . $file->baseName . '.' . $file->extension);

                $names[]=$file->name;
                $names++;
                $total_names = json_encode($names);
                  
                }
                
                $model->document =  $total_names;
                
             if ($model->save()) {
                 $model->updated_by=Yii::$app->user->identity->id;
                 $model->updated_on = date('Y-m-d H:i:s');
                  Yii::$app->session->setFlash('success', 'Record updated Successfully');
                   
               } 

  

            }}
        }
    }

          
        // if ($model->load(Yii::$app->request->post())) {
        //     $files =  $model->file;
            
        //     $model->file = UploadedFile::getInstance($model,'file');
        //    if (!empty($model->file )) {
              
         // echo "<pre>";
         // print_r($model->file);
         // exit();
        //     $model->file->saveAs('uploads/'. $files.$model->file->name);
             
        //     $model->document =  $files.$model->file->name;
        //     // echo "<pre>";
        //     // print_r($model->document);
        //     // exit();

            // $model->save();
              

        //     $activitycreate = Yii::$app->user->identity->username.' Updated the bill for consumer '.$_POST['Bill']['consumer_id'];

        //     $activity = New Activity();
        // $activit = $activity->activityrecord($activitycreate, true);
        //     return $this->redirect(['index', 'id' => $model->id]);
        // } else{
        //     $model->save();
        //     $activitycreate = Yii::$app->user->identity->username.' Updated the bill for consumer '.$_POST['Bill']['consumer_id'];

        //     $activity = New Activity();
        // $activit = $activity->activityrecord($activitycreate, true);
        //      return $this->redirect(['index', 'id' => $model->id]);
        // } }
        $service = $model->other_charges;
        $totalbill= $model->total_bill;
        $payment = $model->payment;
        $arrear = $model->total_bill - $model->payment ;
        
        $model->arrears = $arrear;
        if($model->payment<$totalbill){
        $query = \Yii::$app->db->createCommand("UPDATE plot SET instalment= $model->arrears WHERE id=$model->consumer_id")->execute();
        // echo $query;exit;
        }else{
            $query = \Yii::$app->db->createCommand("UPDATE plot SET instalment= '0' WHERE id=$model->consumer_id")->execute();
        }
        // if ($model->load(Yii::$app->request->post())) {
           
        //     $charge=0;
        //         $services = Services::find()->all();
        //                 foreach ($services as $val1)
        //                 {
        //                     $service = Services::find()->select('charges')->from('services')->where(['id'=>$val1->id])->all();  

        //                 foreach ($service as $serve)
        //                 {
        //                     $charge += $serve['charges'];
    
        //                 }} 
        //         $service_charges = Services::find()->select(['water_bill'=>'service_name','charges'])->from('services')->one();
                             
              
        //         $model->other_charges= $charge ;
        //         $model->total_bill = $charge +  $totalbill;
        //         if ($model->save()) {
        //             $model->updated_by=Yii::$app->user->identity->id;
        //             $model->updated_on = date('Y-m-d H:i:s');
        //              Yii::$app->session->setFlash('success', 'Record updated Successfully');
                      
        //           } 
        //         }

                 
            // echo"<pre>";
            // print_r($model);exit;
              
              if ($model->save()) {
               
                    return $this->render('update', [
                    'model' => $model,
                    
            
        ]); Yii::$app->session->setFlash('success', 'Record updated Successfully');
               } 

      
    }
    /**
     * Deletes an existing Bill model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    { 
         $this->findModel($id)->delete();
        $activitycreate = Yii::$app->user->identity->username.' Deleted the bill of  consumer '.$model->id;

       $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
        Yii::$app->session->setFlash('success', 'Record Deleted Successfully');

        return $this->redirect(['index']);
         Yii::$app->session->setFlash('success', 'Record Deleted Successfully');
    }  


    public function actionCorrectionReport()
    {
        return $this->render('billcorrection');
    }


    public function actionBillReport()
    {
        if(isset($_POST['tenuresearch'])) 
        {
        $Tenuresearch = $_POST['tenuresearch'];
        
        
        $sectorsearch = $_POST['subsector'];
        // echo $sectorsearch ;exit;
        $sdate = $_POST['start_date'];
        $edate = $_POST['end_date'];

        $start_date ='';
        $end_date ='';
        
        if(isset($_GET['BillSearch']['issue_date']))
        {
            $datetime = explode(' - ',$_GET['BillSearch']['issue_date']);
            $start_date = date('Y-m-d', strtotime($issuedate[0]));
            $end_date = date('Y-m-d', strtotime($datetime[1]));
        }
        
           $searchModel = new BillSearch();
           $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // $this->layout = 'dashboard';
        if(!empty($sdate))
        {
            $tenure= Bill::find()->where([
                'or',
                ['billing_months'=>$Tenuresearch],
                ['sector'=>$sectorsearch],
                // ['issue_date'=>$sdate]
                ])->orwhere(['and', "issue_date>='$sdate'", "issue_date<='$edate'"])->count();


            // echo "<pre>";
            // print_r($tenure);
            // exit;
        }else
        {
            $tenure= Bill::find()->where([
                'or',
                ['billing_months'=>$Tenuresearch],
                ['sector'=>$sectorsearch],
                ['issue_date'=>$sdate]
            ])->count();
            // echo "<pre>";
            // print_r($tenure);
            // exit;            
        }
        /*count all Paid Bills*/
        if(!empty($sdate))
        {
            $subsector= Bill::find()->where([
                'or',
               
                ['billing_months'=>$Tenuresearch],
                ['sector'=>$sectorsearch],
                // ['issue_date'=>$sdate]
            ])->orwhere( ['status'=>'1'])->andwhere(['and', "issue_date>='$sdate'", "issue_date<='$edate'"])->count();
        }else
        {
            $subsector= Bill::find()->where([
                'or',
               
                ['billing_months'=>$Tenuresearch],
                ['sector'=>$sectorsearch],
                // ['issue_date'=>$sdate]
                ])->andwhere( ['status'=>'1'])->count();
        }
        // /*Total Pending Bills */ 
                if(!empty($sdate))
                {
                    $pending= Bill::find()->where([
                        'and',
                       
                        ['billing_months'=>$Tenuresearch],
                        ['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                    ])->orwhere( ['status'=>'0'])->andwhere(['and', "issue_date>='$sdate'", "issue_date<='$edate'"])->count();
                }else
                {
                    $pending= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                        ['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                        ])->andwhere(['status'=>'0'])->count();
                }
        // /*Total Amount*/
          if(!empty($sdate))
                {
                    $total_amount= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                        ['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                    ])->orwhere(['and', "issue_date>='$sdate'", "issue_date<='$edate'"])->sum('total_bill');
                    if ($total_amount==0) {
                       $total_amount=0;
                    }else{
                        $total_amount=$total_amount;
                    }
                }else
                {
                    $total_amount= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                        ['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                        ])->sum('total_bill');
                     if ($total_amount==0) {
                       $total_amount=0;
                    }else{
                        $total_amount=$total_amount;
                    }
                }

        // /*Total Paid Amount*/
          if(!empty($sdate))
                {
                    $total_paidamount= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                        ['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                    ])->orwhere(['status'=>'1'])->andwhere(['and', "issue_date>='$sdate'", "issue_date<='$edate'"])->sum('payment');
                     if ($total_paidamount==0) {
                       $total_paidamount=0;
                    }else{
                        $total_paidamount=$total_paidamount;
                    }
                }else
                {
                    $total_paidamount= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                        ['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                        ])->andwhere(['status'=>'1'])->sum('payment');

                      if ($total_paidamount==0) {
                       $total_paidamount=0;
                    }else{
                        $total_paidamount=$total_paidamount;
                    }
                }

         // /*Total unPaid Amount*/
          if(!empty($sdate))
                {
                    $total_unpaidamount= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                        ['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                    ])->orwhere(['status'=>'0'])->andwhere(['and',"issue_date>='$sdate'", "issue_date<='$edate'"])->sum('total_bill');
                     if ($total_unpaidamount==0) {
                       $total_unpaidamount=0;
                    }else{
                        $total_unpaidamount=$total_unpaidamount;
                    }
                }else
                {
                    $total_unpaidamount= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                        ['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                        ])->andwhere(['status'=>'0'])->sum('total_bill');
                     if ($total_unpaidamount==0) {
                       $total_unpaidamount=0;
                    }else{
                        $total_unpaidamount=$total_unpaidamount;
                    }
                }
           



        return $this->render('report',
            [
 
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'tenure'=>$tenure,
                'subsector'=>$subsector,
                'pending'=>$pending,
                'total_amount'=>$total_amount,
                'total_paidamount' =>$total_paidamount,
                'total_unpaidamount'=>$total_unpaidamount

        ]);

         }else{

            $start_date ='';
        $end_date ='';
        
        if(isset($_GET['BillSearch']['created_on']))
        {
            $datetime = explode(' - ',$_GET['BillSearch']['created_on']);
            $start_date = date('Y-m-d', strtotime($datetime[0]));
            $end_date = date('Y-m-d', strtotime($datetime[1]));
        }
        
           $searchModel = new BillSearch();

        // $this->layout = 'dashboard';
     
            $tenure= Bill::find()->count();

       
        /*count all Paid Bills*/
       
            $subsector= Bill::find()->where(['status'=>'1'])->count();
       
        // /*Total Pending Bills */ 
               
                    $pending= Bill::find()->andwhere(['status'=>'0'])->count();
               
        // /*Total Amount*/
       
              
                    $total_amount= Bill::find()->sum('total_bill');
                    if ($total_amount==0) {
                       $total_amount=0;
                    }else{
                        $total_amount=$total_amount;
                    }
               
        // /*Total Paid Amount*/
         
                    $total_paidamount= Bill::find()->where(['status'=>'1'])->sum('total_bill');
                     if ($total_paidamount==0) {
                       $total_paidamount=0;
                    }else{
                        $total_paidamount=$total_paidamount;
                    }
                
         // /*Total unPaid Amount*/
       
                    $total_unpaidamount= Bill::find()->where(['status'=>'0'])->sum('total_bill');
                     if ($total_unpaidamount==0) {
                       $total_unpaidamount=0;
                    }else{
                        $total_unpaidamount=$total_unpaidamount;
                    }
               
               return $this->render('report',
            [
 
                'searchModel' => $searchModel,
                // 'dataProvider' => $dataProvider,
                'tenure'=>$tenure,
                'subsector'=>$subsector,
                'pending'=>$pending,
                'total_amount'=>$total_amount,
                'total_paidamount' =>$total_paidamount,
                'total_unpaidamount'=>$total_unpaidamount

        ]);
            }
    }

    public function actionOverallReport()
    {
        if(isset($_POST['tenuresearch'])) 
        {
        $Tenuresearch = $_POST['tenuresearch'];
        
        $sdate = $_POST['start_date'];
        $edate = $_POST['end_date'];

        $start_date =$sdate;
        $end_date =$edate;
        
        if(isset($_GET['BillSearch']['issue_date']))
        {
            $datetime = explode(' - ',$_GET['BillSearch']['issue_date']);
            $start_date = date('Y-m-d', strtotime($issuedate[0]));
            $end_date = date('Y-m-d', strtotime($datetime[1]));
        }
        
           $searchModel = new BillSearch();
           $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
           $sector = Sectors::find()->all();
            $subsectors = SubSectors::find()->where(['sector_id'=>$sector->id])->all();
        // $this->layout = 'dashboard';
        if(!empty($sdate))
        {
            $tenure= Bill::find()->where([
                'or',
                ['billing_months'=>$Tenuresearch],
                //['sector'=>$sectorsearch],
                // ['issue_date'=>$sdate]
                ])->orwhere(['and', "issue_date>='$sdate'", "issue_date<='$edate'"])->count();


            // echo "<pre>";
            // print_r($tenure);
            // exit;
        }else
        {
            $tenure= Bill::find()->where([
                'or',
                ['billing_months'=>$Tenuresearch],
                //['sector'=>$sectorsearch],
                ['issue_date'=>$sdate]
            ])->count();
            // echo "<pre>";
            // print_r($tenure);
            // exit;            
        }
        /*count all Paid Bills*/
        if(!empty($sdate))
        {
            $subsector= Bill::find()->where([
                'or',
               
                ['billing_months'=>$Tenuresearch],
               // ['sector'=>$sectorsearch],
                // ['issue_date'=>$sdate]
            ])->orwhere( ['status'=>'1'])->andwhere(['and', "issue_date>='$sdate'", "issue_date<='$edate'"])->count();
        }else
        {
            $subsector= Bill::find()->where([
                'or',
               
                ['billing_months'=>$Tenuresearch],
               // ['sector'=>$sectorsearch],
                // ['issue_date'=>$sdate]
                ])->andwhere( ['status'=>'1'])->count();
        }
        // /*Total Pending Bills */ 
                if(!empty($sdate))
                {
                    $pending= Bill::find()->where([
                        'and',
                       
                        ['billing_months'=>$Tenuresearch],
                       // ['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                    ])->orwhere( ['status'=>'0'])->andwhere(['and', "issue_date>='$sdate'", "issue_date<='$edate'"])->count();
                }else
                {
                    $pending= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                       // ['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                        ])->andwhere(['status'=>'0'])->count();
                }
        // /*Total Amount*/
          if(!empty($sdate))
                {
                    $total_amount= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                        //['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                    ])->orwhere(['and', "issue_date>='$sdate'", "issue_date<='$edate'"])->sum('total_bill');
                    if ($total_amount==0) {
                       $total_amount=0;
                    }else{
                        $total_amount=$total_amount;
                    }
                }else
                {
                    $total_amount= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                      //  ['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                        ])->sum('total_bill');
                     if ($total_amount==0) {
                       $total_amount=0;
                    }else{
                        $total_amount=$total_amount;
                    }
                }

        // /*Total Paid Amount*/
          if(!empty($sdate))
                {
                    $total_paidamount= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                      //  ['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                    ])->orwhere(['status'=>'1'])->andwhere(['and', "issue_date>='$sdate'", "issue_date<='$edate'"])->sum('payment');
                     if ($total_paidamount==0) {
                       $total_paidamount=0;
                    }else{
                        $total_paidamount=$total_paidamount;
                    }
                }else
                {
                    $total_paidamount= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                        //['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                        ])->andwhere(['status'=>'1'])->sum('total_bill');

                      if ($total_paidamount==0) {
                       $total_paidamount=0;
                    }else{
                        $total_paidamount=$total_paidamount;
                    }
                }

         // /*Total unPaid Amount*/
          if(!empty($sdate))
                {
                    $total_unpaidamount= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                        //['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                    ])->orwhere(['status'=>'0'])->andwhere(['and',"issue_date>='$sdate'", "issue_date<='$edate'"])->sum('total_bill');
                     if ($total_unpaidamount==0) {
                       $total_unpaidamount=0;
                    }else{
                        $total_unpaidamount=$total_unpaidamount;
                    }
                }else
                {
                    $total_unpaidamount= Bill::find()->where([
                        'or',
                       
                        ['billing_months'=>$Tenuresearch],
                        //['sector'=>$sectorsearch],
                        // ['issue_date'=>$sdate]
                        ])->andwhere(['status'=>'0'])->sum('total_bill');
                     if ($total_unpaidamount==0) {
                       $total_unpaidamount=0;
                    }else{
                        $total_unpaidamount=$total_unpaidamount;
                    }
                }
           



        return $this->render('overallreport',
            [
 
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'sector'=>$sector,

                'subsectors '=> $subsectors, 
                'end_date '=> $end_date,
                'Tenuresearch' =>$Tenuresearch,
                'subsector'=>$subsector,
                'pending'=>$pending,
                'total_amount'=>$total_amount,
                'total_paidamount' =>$total_paidamount,
                'total_unpaidamount'=>$total_unpaidamount

        ]); 

         }else{

            $start_date ='';
        $end_date ='';
        
        if(isset($_GET['BillSearch']['created_on']))
        {
            $datetime = explode(' - ',$_GET['BillSearch']['created_on']);
            $start_date = date('Y-m-d', strtotime($datetime[0]));
            $end_date = date('Y-m-d', strtotime($datetime[1]));
        }
        
           $searchModel = new BillSearch();


         $sector = Sectors::find()->all();

         foreach ($sector as $value) {
             $result = $value->id;
         }
         
                
          $subsectors = SubSectors::find()->select('id','name')->where(['sector_id'=>$result])->all();



        /*count all Paid Bills*/
       
            $subsector= Bill::find()->where(['status'=>'1'])->count();
       
        // /*Total Pending Bills */ 
               
                    $pending= Bill::find()->andwhere(['status'=>'0'])->count();
               
        // /*Total Amount*/
       
              
                    $total_amount= Bill::find()->sum('total_bill');
                    if ($total_amount==0) {
                       $total_amount=0;
                    }else{
                        $total_amount=$total_amount;
                    }
               
        // /*Total Paid Amount*/
         
                    $total_paidamount= Bill::find()->where(['status'=>'1'])->sum('total_bill');
                     if ($total_paidamount==0) {
                       $total_paidamount=0;
                    }else{
                        $total_paidamount=$total_paidamount;
                    }
                
         // /*Total unPaid Amount*/
       
                    $total_unpaidamount= Bill::find()->where(['status'=>'0'])->sum('total_bill');
                     if ($total_unpaidamount==0) {
                       $total_unpaidamount=0;
                    }else{
                        $total_unpaidamount=$total_unpaidamount;
                    }
               
               return $this->render('overallreport',
            [
 
                'searchModel' => $searchModel,
                // 'dataProvider' => $dataProvider,
                 //'paid_amount'=>  $paid_amount,
                //'subsec'=>$subsec,
                'start_date '=> $start_date, 
                'end_date '=> $end_date,
                'subsectors'=>$subsectors,
                'pending'=>$pending,
                'total_amount'=>$total_amount,
                'total_paidamount' =>$total_paidamount,
                'total_unpaidamount'=>$total_unpaidamount

        ]);
            }
    }




    /**
     * Finds the Bill model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bill the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

      public function actionRemove()
        {
           
                  \Yii::$app->db->createCommand("DELETE FROM `bill`
WHERE id BETWEEN '1480' AND '6272';")->execute();
        
            
        }

    protected function findModel($id)
    {
        if (($model = Bill::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

     
}
