<?php

namespace app\controllers;
// require "vendor/phpmailer/autoload.php";
use app\models\Bill;
use app\models\Category;
use app\models\ConsumerServices;
use app\models\Services;
use app\models\Sectors;
use app\models\Plot;
use app\models\Tenure;
use app\models\Sms;
use Yii;
use app\models\BillGenerate;
use app\models\BillGenerateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Activity;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator; 





/**
 * BillGenerateController implements the CRUD actions for BillGenerate model.
 */
class BillGenerateController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='BillGenerate/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ], 
            ], 
        ];

        return $behaviors;
    }

    /**
     * Lists all BillGenerate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BillGenerateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BillGenerate model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)

    {
       $tenure = \app\models\BillGenerate::find()->select('tenure_id')->from('bill_generate')->where('id='.$id)->one();

        $this->layout = 'generatebill';
         $bills = \app\models\Bill::find()->Where(['tinure'=>$tenure->tenure_id])->all();
       // // $bills;
       //   echo "<pre>";
       //   print_r($bills);
       //   exit();
        return $this->render('view', [
            'model' => $bills,

        ]); 
    }
    
     public function actionSectorView()

    {   
        if (isset($_POST['submit'])) {
        
        $tenure = $_POST['tenure'];
        $sector = $_POST['sector'];
        

       $tenure = \app\models\BillGenerate::find()->select('tenure_id')->from('bill_generate')->where('id='.$tenure)->one();

        $this->layout = 'generatebill';
         $bills = \app\models\Bill::find()->Where(['sector'=>$sector])->all();
       // // $bills;
       //   echo "<pre>";
       //   print_r($bills);
       //   exit();
     }
        return $this->render('view', [
            'model' => $bills,

        ]); 
    }

   
    
     public function actionConsumercreate()
    {
 
        $model = new BillGenerate();

        if ($model->load(Yii::$app->request->post())  ) {
            $consumer = Plot::find()->where(['file_no'=>$_POST['BillGenerate']['file_no']])->all();
            // echo "<pre>";
            // print_r($consumer);
            // exit();
            if (!empty($consumer)) {
            
            foreach ($consumer as $k=>$val){
                $cat_detail = Category::findOne($val->cat_id);
                $sector_detail = Sectors::findall($val->sector);
                // $tinure_detail = Tenure::findall();
                // echo "<pre>";
                // print_r($tinure_detail);
                // exit();
                $bill = New Bill(); 
                $bill->consumer_id = $val->id;
                $bill->cnic = $val->cnic;
                $bill->email= $val->email;
                $bill->phone= $val->phone;
                $bill->tinure = $_POST['BillGenerate']['tenure_id']; 
                $tinure_detail = Tenure::findOne($bill->tinure);
                $bill->no_of_months = $tinure_detail->t_interval;

                $charge=0;
                $services = ConsumerServices::find()->where(['consumer_id'=>$val->id] )->all();
                        foreach ($services as $val1)
                        {
                            $service = Services::find()->select('charges')->from('services')->where(['id'=>$val1->service_id])->all();  

                        foreach ($service as $serve)
                        {
                            $charge += $serve['charges']* $tinure_detail->t_interval;
    
                        }} 
                $service_charges = Services::find()->select(['water_bill'=>'service_name','charges'])->from('services')->one();
                             
                $bill->water_bill= $charge;

                $count=0;
                $count = \app\models\Bill::find()->where('consumer_id='.$val->id )->count();
             

                    if($count>0) 
                    {
                        $arrears = \app\models\Bill::find()->select('total_bill,total_after_due_date,status')->from('bill')->Where(['consumer_id'=>$bill->consumer_id]) ->limit(1)->orderBy(['id' =>SORT_DESC])->one();
                        if ($arrears->status==0)
                        {
                           $bill->arrears =$arrears->total_after_due_date; 
                        }
                        else
                        {
                            $bill->arrears=0;
                        }   
                    }
                    else
                    {
                         $bill->arrears = 0 ;
                    }

                $bill->conservancy_amount = $cat_detail->conservancy*$tinure_detail->t_interval;

                $bill->total_water_and_conservancy_amount = $bill->conservancy_amount+$charge;

                $bill->per_month_charges = $bill->total_water_and_conservancy_amount/$tinure_detail->t_interval;
            
                // $bill->sector = $_POST['BillGenerate']['sector_id']; 

                // check credit availability  
                if (!empty($val->balance)) 
                { 
                    $bill->advanced_pay = $val->balance;
                }
                else
                { 
                    $bill->advanced_pay=0;
                }

                // check debiit availability 
              

                $bill->instalment= $val->instalment;

                if ($val->debit>=$bill->instalment) {
                   
                if ($bill->instalment<=0) {
                     $bill->debit = $val->debit;
                     \Yii::$app->db->createCommand("UPDATE plot SET debit= '0' WHERE id=$val->id")->execute();


                }
                else
                {
                   $bill->instalment= $val->instalment;
                   $update= abs($val->debit - $bill->instalment);
                   $val->debit = $update;
                
                   \Yii::$app->db->createCommand("UPDATE plot SET debit= $val->debit  WHERE id=$val->id")->execute();
                   // $bill->debit = $val->debit ;
                   if ($val->debit<$val->instalment) {
                    \Yii::$app->db->createCommand("UPDATE plot SET instalment= '0' WHERE id=$val->id")->execute();
                   }
                }}
                else{

                    $bill->debit=$val->debit;
                     $query = \Yii::$app->db->createCommand("UPDATE plot SET instalment= '0' WHERE id=$val->id")->execute();
                }


                // Bill calcltaions 
               
                $bill->advanced_pay = $val->balance;
                if (!empty($bill->advanced_pay)) 
                {  
                    // if there is credit availabe && greater than bill charges  then this script runs.....
                    if ($bill->advanced_pay >  $cat_detail->conservancy+$bill->arrears+$charge)
                    {
                        $bill->total_bill=  abs($cat_detail->conservancy+$bill->arrears+$charge - $bill->advanced_pay +  $bill->debit+$bill->instalment);
                        $total = $bill->total_bill;

                        $bill->total_bill= 0;

                        \Yii::$app->db->createCommand("UPDATE plot SET balance=  $total  WHERE id=$val->id")->execute();
                    }
                    // if there is credit availabe && less than bill charges  then this script runs 
                    else
                    {
                        $bill->total_bill=  abs($cat_detail->conservancy+$bill->arrears+$bill->instalment+$charge - $bill->advanced_pay);
                        $total = $bill->total_bill;
                        
                        $bill->total_bill = $total;
                        \Yii::$app->db->createCommand("UPDATE plot SET balance= '0'  WHERE id=$val->id")->execute();

                        // \Yii::$app->db->createCommand("UPDATE plot SET debit= '0'  WHERE id=$val->id")->execute();

                    }
                }
                // if there is no credit available 
                else{
                $bill->total_bill =$bill->conservancy_amount+$bill->arrears+$charge + $bill->debit+$bill->instalment;
                 // \Yii::$app->db->createCommand("UPDATE plot SET debit= '0'  WHERE id=$val->id")->execute();
                }


                $bill->after_due_date_charges =round($bill->total_bill*0.1);  
                $bill->total_after_due_date = $bill->after_due_date_charges+$bill->total_bill;
                $tinure = \app\models\Tenure::find()->select('tenure_name')->from('tenure')->where('id ='.$bill->tinure)->one();
                $bill->billing_months = $tinure->tenure_name; 
                $bill->issue_date = date('Y-m-d');
                $bill->due_date = $_POST['due-date'];
                $bill->created_on = date('Y-m-d H:i:s');
                $bill->created_by = Yii::$app->user->identity->id;
                $bill->dues_amount = 0;
                // $bill->debit = $val->debit;
                $bill->payment = 0; 
                $bill->arrears_period = 0;
                $bill->status = 0;
                $bill->water_arrears=0;
                
                // sending bill generation mail
                // \Yii::$app->mail->compose()
                //     ->setFrom("asadali55772@gmail.com")
                //     ->setTo($bill->email)
                //         ->setSubject('Dear Customer,
                //     Your bill for'.' '. $bill->billing_months.' '. 'is Rs'.' '.$bill->total_bill.' '. 
                //     'Please Pay your bill within the due date. you can print the bill by given link'.' '.'http://localhost/maaliksoft/billing/web/site/print-bill'.
                //     ' ' . 'FGEHA' )
                //     ->send();
 

                // sending bill generation sms
                    $phone = $bill->phone;
                    $msg = 'Dear Customer,
                    Your bill for'.' '. $bill->billing_months.' '. 'is Rs'.' '.$bill->total_bill.' '. 
                    'Please Pay your bill within the due date. you can print the bill by given link'.' '.'http://localhost/maaliksoft/billing/web/site/print-bill'.
                    ' ' . 'FGEHA';
                    $isSmsSent = Sms::sendsms($phone,$msg, true);
                    // return json_encode(array('status' => $isSmsSent));
                    $activitycreate = Yii::$app->user->identity->username.' Genertaed The new bill for Consumer '.$_POST['BillGenerate']['file_no'];

                    $activity = New Activity();
                    $activit = $activity->activityrecord($activitycreate, true);
                    $bill->document='null';
                    $bill->barcode_id='null';
                    
                if($bill->save()){
                $barcode = new BarcodeGenerator();
                $barcode->setText($bill->id.'_'.$val->id); 
                             
                $barcode->setType(BarcodeGenerator::Code128);
                $barcode->setScale(2);
                $barcode->setThickness(25);
                $barcode->setFontSize(10);
                $code = $barcode->generate();
                $bill->barcode_id=$code;
                if($bill->save()){

                      }
                else{
                    echo '<pre>';
                    echo print_r($bill);
                    exit;
                }
                   
                }
                else{
                    echo '<pre>';
                    echo print_r($bill);
                    exit;
                }

            }}else
            {
                echo "<script>alert('No consumer found for ');</script>";
                 return $this->render('consumercreate', [
            'model' => $model,
        ]);
            }
             
            $model->status = 1;
            $model->print_status = 0;
            $model->created_by = Yii::$app->user->identity->id;
            $model->created_on = date('Y-m-d H:i:s');
            if($model->validate($model))
            {
                if($model->save())
                { 
                    return $this->redirect(['index', 'id' => $model->id]);
                }
            }
        }
         
        return $this->render('consumercreate', [
            'model' => $model,
        ]);
    }
    
     
     public function actionSubsectorcreate()
    {
 
        $model = new BillGenerate();

        if ($model->load(Yii::$app->request->post())  ) {
            $consumer = Plot::find()->where(['sub_sector'=>$_POST['BillGenerate']['sector_id']])->all();

            if (!empty($consumer)) {
            
            
           
            foreach ($consumer as $k=>$val){
                $cat_detail = Category::findOne($val->cat_id);
                $sector_detail = Sectors::findall($val->sector);
                $bill = New Bill(); 
                $bill->consumer_id = $val->id;
                $bill->cnic = $val->cnic;
                $bill->email= $val->email;
                $bill->phone= $val->phone;
                $bill->tinure = $_POST['BillGenerate']['tenure_id']; 
                $tinure_detail = Tenure::findOne($bill->tinure);
                $bill->no_of_months = $tinure_detail->t_interval;

                $charge=0;
                $services = ConsumerServices::find()->where(['consumer_id'=>$val->id] )->all();
                        foreach ($services as $val1)
                        {
                            $service = Services::find()->select('charges')->from('services')->where(['id'=>$val1->service_id])->all();  

                        foreach ($service as $serve)
                        {
                            $charge += $serve['charges']* $tinure_detail->t_interval;
    
                        }} 
                $service_charges = Services::find()->select(['water_bill'=>'service_name','charges'])->from('services')->one();
                             
                $bill->water_bill= $charge;

                $count=0;
                $count = \app\models\Bill::find()->where('consumer_id='.$val->id )->groupBy('status')->count();

                    if($count>0)
                    {
                        $arrears = \app\models\Bill::find()->select('total_bill,total_after_due_date,status')->from('bill')->Where(['consumer_id'=>$bill->consumer_id]) ->limit(1)->orderBy(['id' =>SORT_DESC])->one();
                        if ($arrears->status==0)
                        {
                           $bill->arrears =$arrears->total_after_due_date; 
                        }
                        else
                        {
                            $bill->arrears=0;
                        }   
                    }
                    else
                    {
                         $bill->arrears = 0 ;
                    }

                $bill->conservancy_amount = $cat_detail->conservancy*$tinure_detail->t_interval;

                $bill->total_water_and_conservancy_amount = $bill->conservancy_amount+$charge;

                $bill->per_month_charges = $bill->total_water_and_conservancy_amount/$tinure_detail->t_interval;
            
                $bill->sector = $_POST['BillGenerate']['sector_id']; 

                // check credit availability  
                if (!empty($val->balance)) 
                { 
                    $bill->advanced_pay = $val->balance;
                }
                else
                { 
                    $bill->advanced_pay=0;
                }

                // check debiit availability 
              

                $bill->instalment= $val->instalment;

                if ($val->debit>=$bill->instalment) {
                   
                if ($bill->instalment<=0) {
                     $bill->debit = $val->debit;
                     \Yii::$app->db->createCommand("UPDATE plot SET debit= '0' WHERE id=$val->id")->execute();


                }
                else
                {
                   $bill->instalment= $val->instalment;
                   $update= abs($val->debit - $bill->instalment);
                   $val->debit = $update;
                
                   \Yii::$app->db->createCommand("UPDATE plot SET debit= $val->debit  WHERE id=$val->id")->execute();
                   // $bill->debit = $val->debit ;
                   if ($val->debit<$val->instalment) {
                    \Yii::$app->db->createCommand("UPDATE plot SET instalment= '0' WHERE id=$val->id")->execute();
                   }
                }}
                else{

                    $bill->debit=$val->debit;
                     $query = \Yii::$app->db->createCommand("UPDATE plot SET instalment= '0' WHERE id=$val->id")->execute();
                }


                // Bill calcltaions 
               
                $bill->advanced_pay = $val->balance;
                if (!empty($bill->advanced_pay)) 
                {  
                    // if there is credit availabe && greater than bill charges  then this script runs.....
                    if ($bill->advanced_pay >  $cat_detail->conservancy+$bill->arrears+$charge)
                    {
                        $bill->total_bill=  abs($cat_detail->conservancy+$bill->arrears+$charge - $bill->advanced_pay +  $bill->debit+$bill->instalment);
                        $total = $bill->total_bill;

                        $bill->total_bill= 0;

                        \Yii::$app->db->createCommand("UPDATE plot SET balance=  $total  WHERE id=$val->id")->execute();
                    }
                    // if there is credit availabe && less than bill charges  then this script runs 
                    else
                    {
                        $bill->total_bill=  abs($cat_detail->conservancy+$bill->arrears+$bill->instalment+$charge - $bill->advanced_pay);
                        $total = $bill->total_bill;
                        
                        $bill->total_bill = $total;
                        \Yii::$app->db->createCommand("UPDATE plot SET balance= '0'  WHERE id=$val->id")->execute();

                        // \Yii::$app->db->createCommand("UPDATE plot SET debit= '0'  WHERE id=$val->id")->execute();

                    }
                }
                // if there is no credit available 
                else{
                $bill->total_bill =$bill->conservancy_amount+$bill->arrears+$charge + $bill->debit+$bill->instalment;
                 // \Yii::$app->db->createCommand("UPDATE plot SET debit= '0'  WHERE id=$val->id")->execute();
                }


                $bill->after_due_date_charges =round($bill->total_bill*0.1);  
                $bill->total_after_due_date = $bill->after_due_date_charges+$bill->total_bill;
                $tinure = \app\models\Tenure::find()->select('tenure_name')->from('tenure')->where('id ='.$bill->tinure)->one();
                $bill->billing_months = $tinure->tenure_name; 
                $bill->issue_date = date('Y-m-d');
                $bill->due_date = $_POST['due-date'];
                $bill->created_on = date('Y-m-d H:i:s');
                $bill->created_by = Yii::$app->user->identity->id;
                $bill->dues_amount = 0;
                // $bill->debit = $val->debit;
                $bill->payment = 0; 
                $bill->arrears_period = 0;
                $bill->status = 0;
                $bill->water_arrears=0;
                
                // sending bill generation mail
                \Yii::$app->mail->compose()
                    ->setFrom('maaliktest@gmail.com')
                    ->setTo($bill->email)
                        ->setSubject('Dear Customer,
                    Your bill for'.' '. $bill->billing_months.' '. 'is Rs'.' '.$bill->total_bill.' '. 
                    'Please Pay your bill within the due date. you can print the bill by given link'.' '.'http://localhost/maaliksoft/billing/web/site/print-bill'.
                    ' ' . 'FGEHA' )
                    ->send();
 

                // sending bill generation sms
                    $phone = $bill->phone;
                    $msg = 'Dear Customer,
                    Your bill for'.' '. $bill->billing_months.' '. 'is Rs'.' '.$bill->total_bill.' '. 
                    'Please Pay your bill within the due date. you can print the bill by given link'.' '.'http://localhost/maaliksoft/billing/web/site/print-bill'.
                    ' ' . 'FGEHA';
                    $isSmsSent = Sms::sendsms($phone,$msg, true);
                    // return json_encode(array('status' => $isSmsSent));
                    $activitycreate = Yii::$app->user->identity->username.' Genertaed The new bill for Sector '.$_POST['BillGenerate']['sector_id'];

                    $activity = New Activity();
                    $activit = $activity->activityrecord($activitycreate, true);
                    
                if($bill->save()){

                $barcode = new BarcodeGenerator();
                $barcode->setText($bill->id.'_'.$val->id);               
                $barcode->setType(BarcodeGenerator::Code128);
                $barcode->setScale(2);
                $barcode->setThickness(25);
                $barcode->setFontSize(10);
                $code = $barcode->generate();
                $bill->barcode_id=$code;
                if($bill->save()){

                      }
                else{
                    echo '<pre>';
                    echo print_r($bill);
                    exit;
                }
                   
                }
                else{
                    echo '<pre>';
                    echo print_r($bill);
                    exit;
                }

            }}else
            {
                echo "<script>alert('No consumer found for this Sector');</script>";
                 return $this->render('sectorcreate', [
            'model' => $model,
        ]);
            }
             
            $model->status = 1;
            $model->print_status = 0;
            $model->created_by = Yii::$app->user->identity->id;
            $model->created_on = date('Y-m-d H:i:s');
            // echo "<pre>";
            // print_r( $model->created_on);
            // exit();
            if($model->validate($model))
            {
                if($model->save())
                { 
                    return $this->redirect(['index', 'id' => $model->id]);
                }
            }
        }
         
        return $this->render('subsectorcreate', [
            'model' => $model,
        ]);
    }



    public function actionSectorcreate()
    {
 
        $model = new BillGenerate();

        if ($model->load(Yii::$app->request->post())  ) {
            $consumer = Plot::find()->where(['sector'=>$_POST['BillGenerate']['sector_id']])->all();
            if (!empty($consumer)) {
            
            
           
            foreach ($consumer as $k=>$val){
                $cat_detail = Category::findOne($val->cat_id);
                $sector_detail = Sectors::findall($val->sector);
                // $tinure_detail = Tenure::findall();
                // echo "<pre>";
                // print_r($tinure_detail);
                // exit();
                $bill = New Bill(); 
                $bill->consumer_id = $val->id;
                $bill->cnic = $val->cnic;
                $bill->email= $val->email;
                $bill->phone= $val->phone;
                $bill->tinure = $_POST['BillGenerate']['tenure_id']; 
                $tinure_detail = Tenure::findOne($bill->tinure);
                $bill->no_of_months = $tinure_detail->t_interval;

                $charge=0;
                $services = ConsumerServices::find()->where(['consumer_id'=>$val->id] )->all();
                        foreach ($services as $val1)
                        {
                            $service = Services::find()->select('charges')->from('services')->where(['id'=>$val1->service_id])->all();  

                        foreach ($service as $serve)
                        {
                            $charge += $serve['charges']* $tinure_detail->t_interval;
    
                        }} 
                $service_charges = Services::find()->select(['water_bill'=>'service_name','charges'])->from('services')->one();
                             
                $bill->water_bill= $charge;

                $count=0;
                $count = \app\models\Bill::find()->where('consumer_id='.$val->id )->groupBy('status')->count();

                    if($count>0)
                    {
                        $arrears = \app\models\Bill::find()->select('total_bill,total_after_due_date,status')->from('bill')->Where(['consumer_id'=>$bill->consumer_id]) ->limit(1)->orderBy(['id' =>SORT_DESC])->one();
                        if ($arrears->status==0)
                        {
                           $bill->arrears =$arrears->total_after_due_date; 
                        }
                        else
                        {
                            $bill->arrears=0;
                        }   
                    }
                    else
                    {
                         $bill->arrears = 0 ;
                    }

                $bill->conservancy_amount = $cat_detail->conservancy*$tinure_detail->t_interval;

                $bill->total_water_and_conservancy_amount = $bill->conservancy_amount+$charge;

                $bill->per_month_charges = $bill->total_water_and_conservancy_amount/$tinure_detail->t_interval;
            
                $bill->sector = $_POST['BillGenerate']['sector_id']; 

                // check credit availability  
                if (!empty($val->balance)) 
                { 
                    $bill->advanced_pay = $val->balance;
                }
                else
                { 
                    $bill->advanced_pay=0;
                }

                // check debiit availability 
              

                $bill->instalment= $val->instalment;

                if ($val->debit>=$bill->instalment) {
                   
                if ($bill->instalment<=0) {
                     $bill->debit = $val->debit;
                     \Yii::$app->db->createCommand("UPDATE plot SET debit= '0' WHERE id=$val->id")->execute();


                }
                else
                {
                   $bill->instalment= $val->instalment;
                   $update= abs($val->debit - $bill->instalment);
                   $val->debit = $update;
                
                   \Yii::$app->db->createCommand("UPDATE plot SET debit= $val->debit  WHERE id=$val->id")->execute();
                   // $bill->debit = $val->debit ;
                   if ($val->debit<$val->instalment) {
                    \Yii::$app->db->createCommand("UPDATE plot SET instalment= '0' WHERE id=$val->id")->execute();
                   }
                }}
                else{

                    $bill->debit=$val->debit;
                     $query = \Yii::$app->db->createCommand("UPDATE plot SET instalment= '0' WHERE id=$val->id")->execute();
                }


                // Bill calcltaions 
               
                $bill->advanced_pay = $val->balance;
                if (!empty($bill->advanced_pay)) 
                {  
                    // if there is credit availabe && greater than bill charges  then this script runs.....
                    if ($bill->advanced_pay >  $cat_detail->conservancy+$bill->arrears+$charge)
                    {
                        $bill->total_bill=  abs($cat_detail->conservancy+$bill->arrears+$charge - $bill->advanced_pay +  $bill->debit+$bill->instalment);
                        $total = $bill->total_bill;

                        $bill->total_bill= 0;

                        \Yii::$app->db->createCommand("UPDATE plot SET balance=  $total  WHERE id=$val->id")->execute();
                    }
                    // if there is credit availabe && less than bill charges  then this script runs 
                    else
                    {
                        $bill->total_bill=  abs($cat_detail->conservancy+$bill->arrears+$bill->instalment+$charge - $bill->advanced_pay);
                        $total = $bill->total_bill;
                        
                        $bill->total_bill = $total;
                        \Yii::$app->db->createCommand("UPDATE plot SET balance= '0'  WHERE id=$val->id")->execute();

                        // \Yii::$app->db->createCommand("UPDATE plot SET debit= '0'  WHERE id=$val->id")->execute();

                    }
                }
                // if there is no credit available 
                else{
                $bill->total_bill =$bill->conservancy_amount+$bill->arrears+$charge + $bill->debit+$bill->instalment;
                 // \Yii::$app->db->createCommand("UPDATE plot SET debit= '0'  WHERE id=$val->id")->execute();
                }


                $bill->after_due_date_charges =round($bill->total_bill*0.1);  
                $bill->total_after_due_date = $bill->after_due_date_charges+$bill->total_bill;
                $tinure = \app\models\Tenure::find()->select('tenure_name')->from('tenure')->where('id ='.$bill->tinure)->one();
                $bill->billing_months = $tinure->tenure_name; 
                $bill->issue_date = date('Y-m-d');
                $bill->due_date = $_POST['due-date'];
                $bill->created_on = date('Y-m-d H:i:s');
                $bill->created_by = Yii::$app->user->identity->id;
                $bill->dues_amount = 0;
                // $bill->debit = $val->debit;
                $bill->payment = 0; 
                $bill->arrears_period = 0;
                $bill->status = 0;
                $bill->water_arrears=0;
                $barcode = new BarcodeGenerator();
                $barcode->setText($val->id);                
                $barcode->setType(BarcodeGenerator::Code128);
                $barcode->setScale(2);
                $barcode->setThickness(25);
                $barcode->setFontSize(10);
                $code = $barcode->generate();
                $bill->barcode_id=$code;
                // sending bill generation mail
                \Yii::$app->mail->compose()
                    ->setFrom('maaliktest@gmail.com')
                    ->setTo($bill->email)
                        ->setSubject('Dear Customer,
                    Your bill for'.' '. $bill->billing_months.' '. 'is Rs'.' '.$bill->total_bill.' '. 
                    'Please Pay your bill within the due date. you can print the bill by given link'.' '.'http://localhost/maaliksoft/billing/web/site/print-bill'.
                    ' ' . 'FGEHA' )
                    ->send();
 

                // sending bill generation sms
                    $phone = $bill->phone;
                    $msg = 'Dear Customer,
                    Your bill for'.' '. $bill->billing_months.' '. 'is Rs'.' '.$bill->total_bill.' '. 
                    'Please Pay your bill within the due date. you can print the bill by given link'.' '.'http://localhost/maaliksoft/billing/web/site/print-bill'.
                    ' ' . 'FGEHA';
                    $isSmsSent = Sms::sendsms($phone,$msg, true);
                    // return json_encode(array('status' => $isSmsSent));
                    $activitycreate = Yii::$app->user->identity->username.' Genertaed The new bill for Sector '.$_POST['BillGenerate']['sector_id'];

                    $activity = New Activity();
                    $activit = $activity->activityrecord($activitycreate, true);
                    
                if($bill->save()){
                   
                }
                else{
                    echo '<pre>';
                    echo print_r($bill);
                    exit;
                }

            }}else
            {
                echo "<script>alert('No consumer found for this Sector');</script>";
                 return $this->render('sectorcreate', [
            'model' => $model,
        ]);
            }
             
            $model->status = 1;
            $model->print_status = 0;
            $model->created_by = Yii::$app->user->identity->id;
            $model->created_on = date('Y-m-d H:i:s');
            if($model->validate($model))
            {
                if($model->save())
                { 
                    return $this->redirect(['index', 'id' => $model->id]);
                }
            }
        }
         
        return $this->render('sectorcreate', [
            'model' => $model,
        ]);
    }
 
    /**
     * Creates a new BillGenerate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


    public function actionCreate()
    {
        $model = new BillGenerate();

        if ($model->load(Yii::$app->request->post())  ) {
            $consumer = Plot::find()->all();
           
            foreach ($consumer as $k=>$val){
                $cat_detail = Category::findOne($val->cat_id);
                
                $bill = New Bill();
                $bill->consumer_id = $val->id;
                $bill->cnic = $val->cnic;
                $bill->email= $val->email;
                $bill->phone= $val->phone;
                $bill->tinure = $_POST['BillGenerate']['tenure_id']; 
                $tinure_detail = Tenure::findOne($bill->tinure);

                $bill->no_of_months = $tinure_detail->t_interval;
                $bill->sector=$val->sector;

                $charge=0;
                $services = ConsumerServices::find()->where(['consumer_id'=>$val->id] )->all();
                        foreach ($services as $val1)
                        {
                            $service = Services::find()->select('charges')->from('services')->where(['id'=>$val1->service_id])->all();  

                        foreach ($service as $serve)
                        {
                            $charge += $serve['charges']* $tinure_detail->t_interval;
    
                        }} 
                $service_charges = Services::find()->select(['water_bill'=>'service_name','charges'])->from('services')->one();
                             
                $bill->water_bill= $charge ;

                $count=0;
                $count = \app\models\Bill::find()->where('consumer_id='.$val->id )->groupBy('status')->count();

                    if($count>0)
                    {
                        $arrears = \app\models\Bill::find()->select('total_bill,total_after_due_date,status')->from('bill')->Where(['consumer_id'=>$bill->consumer_id]) ->limit(1)->orderBy(['id' =>SORT_DESC])->one();
                        if ($arrears->status==0)
                        {
                           $bill->arrears =$arrears->total_after_due_date; 
                        }
                        else
                        {
                            $bill->arrears=0;
                        }   
                    }
                    else
                    {
                         $bill->arrears = 0 ;
                    }

                $bill->conservancy_amount = $cat_detail->conservancy*$tinure_detail->t_interval;

                $bill->total_water_and_conservancy_amount = $bill->conservancy_amount+$charge;
                $bill->per_month_charges = $bill->total_water_and_conservancy_amount/$tinure_detail->t_interval;               

                // check credit availability  
                if (!empty($val->balance)) 
                { 
                    $bill->advanced_pay = $val->balance;
                }
                else
                { 
                    $bill->advanced_pay=0;
                }

                // check debiit availability 
              

                $bill->instalment= $val->instalment;

                if ($val->debit>=$bill->instalment) {
                   
                if ($bill->instalment<=0) {
                     $bill->debit = $val->debit;
                     \Yii::$app->db->createCommand("UPDATE plot SET debit= '0' WHERE id=$val->id")->execute();

                }
                else
                {
                   $bill->instalment= $val->instalment;
                   $update= abs($val->debit - $bill->instalment);
                   $val->debit = $update;
                
                   \Yii::$app->db->createCommand("UPDATE plot SET debit= $val->debit  WHERE id=$val->id")->execute();
                   // $bill->debit = $val->debit ;
                   if ($val->debit<$val->instalment) {
                    \Yii::$app->db->createCommand("UPDATE plot SET instalment= '0' WHERE id=$val->id")->execute();
                   }
                }}
                else{

                    $bill->debit=$val->debit;


                    // $val->debit=0;


                     $query = \Yii::$app->db->createCommand("UPDATE plot SET instalment= '0' WHERE id=$val->id")->execute();
                }


                // Bill calcltaions 
               
                $bill->advanced_pay = $val->balance;
                if (!empty($bill->advanced_pay)) 
                {  
                    // if there is credit availabe && greater than bill charges  then this script runs.....
                    if ($bill->advanced_pay >  $cat_detail->conservancy+$bill->arrears+$charge)
                    {
                        $bill->total_bill=  abs($cat_detail->conservancy+$bill->arrears+$charge - $bill->advanced_pay +  $bill->debit+$bill->instalment);
                        $total = $bill->total_bill;

                        $bill->total_bill= 0;

                        \Yii::$app->db->createCommand("UPDATE plot SET balance=  $total  WHERE id=$val->id")->execute();
                    }
                    // if there is credit availabe && less than bill charges  then this script runs 
                    else
                    {
                        $bill->total_bill=  abs($cat_detail->conservancy+$bill->arrears+$bill->instalment+$charge - $bill->advanced_pay);
                        $total = $bill->total_bill;
                        
                        $bill->total_bill = $total;
                        \Yii::$app->db->createCommand("UPDATE plot SET balance= '0'  WHERE id=$val->id")->execute();

                        // \Yii::$app->db->createCommand("UPDATE plot SET debit= '0'  WHERE id=$val->id")->execute();

                    }
                }
                // if there is no credit available 
                else{
                $bill->total_bill =$bill->conservancy_amount+$bill->arrears+$charge + $bill->debit+$bill->instalment;
                 // \Yii::$app->db->createCommand("UPDATE plot SET debit= '0'  WHERE id=$val->id")->execute();
                }



                $bill->after_due_date_charges =round($bill->total_bill*0.1);  
                $bill->total_after_due_date = $bill->after_due_date_charges+$bill->total_bill;
                $tinure = \app\models\Tenure::find()->select('tenure_name')->from('tenure')->where('id ='.$bill->tinure)->one();
                $bill->billing_months = $tinure->tenure_name; 
                $bill->issue_date = date('Y-m-d');
                $bill->due_date = $_POST['due-date'];
                $bill->created_on = date('Y-m-d H:i:s');
                $bill->created_by = Yii::$app->user->identity->id;
                $bill->dues_amount = 0;
                // $bill->debit = $val->debit;
                $bill->payment = 0; 
                $bill->arrears_period = 0;
                $bill->status = 0;
                $bill->water_arrears=0;
                $barcode = new BarcodeGenerator();
                $barcode->setText($val->id);                
                $barcode->setType(BarcodeGenerator::Code128);
                $barcode->setScale(2);
                $barcode->setThickness(25);
                $barcode->setFontSize(10);
                $code = $barcode->generate();
                $bill->barcode_id=$code;

                // sending bill generation mail
                \Yii::$app->mail->compose()
                    ->setFrom('maaliktest@gmail.com')
                    ->setTo($bill->email)
                    ->setSubject('Dear Customer,
                    Your bill for'.' '. $bill->billing_months.' '. 'is Rs'.' '.$bill->total_bill.' '. 
                    'Please Pay your bill within the due date. you can print the bill by given link'.' '.'http://localhost/maaliksoft/billing/web/site/print-bill'.
                    ' ' . 'FGEHA' )
                    ->send();

                // sending bill generation sms
                    $phone = $bill->phone;
                    $msg = 'Dear Customer,
                    Your bill for'.' '. $bill->billing_months.' '. 'is Rs'.' '.$bill->total_bill.' '. 
                    'Please Pay your bill within the due date. you can print the bill by given link'.' '.'http://localhost/maaliksoft/billing/web/site/print-bill'.
                    ' ' . 'FGEHA';
                    $isSmsSent = Sms::sendsms($phone,$msg, true);
                    // return json_encode(array('status' => $isSmsSent));
                    $activitycreate = Yii::$app->user->identity->username.' Generated the new Bill for tinure '.$_POST['BillGenerate']['tenure_id'];

                    $activity = New Activity();
                    $activit = $activity->activityrecord($activitycreate, true);

                if($bill->save()){
                   
                }
                else{
                    echo '<pre>';
                    echo print_r($bill);
                    exit;
                }

            }
             
            $model->status = 1;
            $model->print_status = 0;
            $model->created_by = Yii::$app->user->identity->id;
            $model->created_on = date('Y-m-d H:i:s');
   
            if($model->validate($model))
            {

                if($model->save())
                {

                    return $this->redirect(['index', 'id' => $model->id]);
                }
            }
        }
         
        return $this->render('create', [
            'model' => $model,
        ]);
    }

     public function actionSendSms() {
        $phone = '03440160561';
        $msg = 'test';
        $isSmsSent = Sms::sendsms($phone, true);
        echo json_encode(array('status' => $isSmsSent));

    }
    /**
     * Updates an existing BillGenerate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
      
        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $activitycreate = Yii::$app->user->identity->username.' Updated the  Bill for tinure '.$_POST['BillGenerate']['tenure_id'];

                    $activity = New Activity();
                    $activit = $activity->activityrecord($activitycreate, true);
           
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BillGenerate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' Deleted the bill of '.$model->tenure_id;
        $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
        return $this->redirect(['index']);
    }

    /**
     * Finds the BillGenerate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BillGenerate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BillGenerate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
