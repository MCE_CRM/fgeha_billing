<?php

namespace app\controllers;

use Yii;
use app\models\Payable;
use app\models\PayableSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl; 
use app\models\Activity;

/**
 * PayableController implements the CRUD actions for Payable model.
 */
class PayableController extends Controller
{
    /**
     * {@inheritdoc}
     */
        public function behaviors()
    {
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='Payable/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ], 
        ];

        return $behaviors;
    }

    /**
     * Lists all Payable models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PayableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Payable model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' views the payable of consumer '.$model->consumer_id;

            $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
        return $this->render('view', [
            'model' => $model,
        ]); 
    }

    /**
     * Creates a new Payable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Payable();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             $activitycreate = Yii::$app->user->identity->username.' Created The Payables of consumer'.$model->consumer_id;
            $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Payable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             $activitycreate = Yii::$app->user->identity->username.' updated The Payables of consumer'.$model->consumer_id;
            $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Payable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' Deleted the payable of consumer '.$model->consumer_id;
        $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payable::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
