<?php

namespace app\controllers;

use Yii;
use app\models\Plot;
use app\models\PlotSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\SubSectors;
use app\models\Sectors;
use app\models\Services;
use app\models\Category;
use app\models\ConsumerServices;
use app\models\Activity;
use yii\web\UploadedFile;
/**
 * PlotController implements the CRUD actions for Plot model.
 */
class PlotController extends Controller
{
    /**
     * {@inheritdoc}
     */
   public function behaviors()
    {
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='BillGenerate/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ], 
            ], 
        ];

        return $behaviors;
    }

    /**
     * Lists all Plot models.
     * @return mixed
     */
    public function actionIndex()
        {
            $searchModel = new PlotSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

    /**
     * Displays a single Plot model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
        { 
             $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' views the consumer '.$model->allottee_name;
        $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
        return $this->render('view', [
            'model' => $model,
        ]); 
        }

     /**
     * Creates a action for import Excelsheet.
     */


    public function actionImport(){
        $model = new Plot();
       return $this->render('import', [
                    'model' => $model,

                ]);
    }
      
    public function actionImportExcel()
            {
                $model = new Plot();
               
            if ($model->load(Yii::$app->request->post())) {
             $model->document= $_POST['Plot']['document'];
             

            // echo "<pre>";
            // print_r($files);
            // exit();
              
            
            $model->file = UploadedFile::getInstance($model,'document');


           if (!empty($model->file )) {

            $model->file->saveAs('uploads/'. $model->document.$model->file->name);

           
            $model->document =  $model->document.$model->file->name;
            
           
            $model->save();
          
            }}
        
                 $inputFile = 'uploads/'.$model->file->name;

                try
                {

                    $inputFileType =\PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel =$objReader->load($inputFile);
                } 
                catch (Exception $e)
                {
                    die('Error');
                }
            $sheet = $objPHPExcel->getSheet(0);  
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            for ($row = 1; $row <= $highestRow; $row++ )
            {
                $rowData = $sheet->rangetoArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,False);
                if ($row == 1)
                {

                    continue;
                }

                $plot = new Plot();

                // echo "<pre>";
                // print_r($plot);
                // exit();
               
                $plot->file_no = $rowData[0][0];
                $plot->cat_id = $rowData[0][1];
                $category = Category::find()->where(['conservancy'=>$plot->cat_id])->one();

                

                     if ($plot->cat_id=='500') {
                         
                        $plot->cat_id= $category->id;

                    }
                    elseif ($plot->cat_id=='356') {
                        $plot->cat_id= $category->id;
                      
                    }
                    elseif ($plot->cat_id=='272') {
                        $plot->cat_id= $category->id;
                       
                    }
                     elseif ($plot->cat_id=='200') {
                        $plot->cat_id= $category->id;
                      
                    }
                    elseif ($plot->cat_id=='111') {
                        $plot->cat_id= $category->id;
                       
                     }
                    elseif ($plot->cat_id=='139') {
                        $plot->cat_id= $category->id;
                       
                     } 
                    elseif ($plot->cat_id=='234') {
                        $plot->cat_id= $category->id;
                       
                     } 
                    elseif ($plot->cat_id=='667') {
                        $plot->cat_id= $category->id;
                       
                     } 
                    elseif ($plot->cat_id=='251') {
                        $plot->cat_id= $category->id;
                       
                     }
                    elseif ($plot->cat_id=='340') {
                        $plot->cat_id= $category->id;
                       
                     }
                    elseif ($plot->cat_id=='412') {
                        $plot->cat_id= $category->id;
                       
                     }
                    elseif ($plot->cat_id=='496') {
                        $plot->cat_id= $category->id;
                       
                     }       
                 $plot->connection_date = $rowData[0][2];
                 $plot->allottee_name = $rowData[0][3];
                 // echo "<pre>";
                 // print_r($plot->allottee_name);
                 // exit();
                 $plot->email = $rowData[0][4];
                 $plot->phone = $rowData[0][5];
                 $plot->cnic = $rowData[0][6];
                 $plot->balance = $rowData[0][7];
                 $plot->debit = $rowData[0][8];
                 if ($plot->debit<0) {
                    $plot->balance = abs($plot->debit);
                    $plot->debit=0;
                     // echo "<pre>";
                     // print_r($plot->balance);
                     // exit();
                 }
                 
                 $plot->instalment = $rowData[0][9];
                 $plot->sector = $rowData[0][10];
                 $plot->sub_sector = $rowData[0][11];
                 $plot->possession_date = $rowData[0][12];
                 $plot->house_no = $rowData[0][13]; 
                 $plot->street_no = $rowData[0][14];
                 $plot->reference_no = $rowData[0][15];
                 $plot->consumer_no = $rowData[0][16];
                 $plot->created_date = date('Y-m-d H:i:s');
                 $plot->created_by = Yii::$app->user->identity->id;

                
                 $plot->save();
                 $services = New ConsumerServices();
                
                 if (!empty($services)) {
                     
                 
                 $services->service_id = $rowData[0][17];
                  if ( $services->service_id=='140') {
                         
                        $services->service_id= '2';

                    }else{
                         $services->service_id= '0';
                    }

                 $services->consumer_id = $plot->id;
                 if($services->save()){
                 }
                else{
                    echo '<pre>';
                    echo print_r($services);
                    exit;                
                }
            }
 
                 
                echo "<pre>";
                 print_r($plot->getErrors());
                 // exit();
                 //     echo "<pre>";
                 // print_r($plot);
                 //  exit();
                

            }
              //echo "<pre>";
             //     print_r($plot);
             //     exit();
              return $this->redirect('index');
            die('okay');
            }    

     



     /**
     * Creates a action onchange for sectors and subsectors.
     */
    public $enableCsrfValidation = false;

    public function actionList($id)
        {   
           $count = SubSectors::find()->where(['sector_id'=>$id])->count();
            $branches = SubSectors::find()->where(['sector_id'=>$id])->all();
            if ($count > 0) {
                foreach ($branches as $sector ) {
                    echo "<option value'".$sector->id."''>" .$sector->name."</option>";
                }
            }else{
                echo "something went wrong";
            }
        } 


          public function actionUser()
    {
        $this->layout = 'main';
        $bill = \app\models\User::find()->all();
        // echo "<pre>";
        //     print_r($bill);
        //     exit();
          
       return $this->render('user', [
            'model' => $bill,]);
    }

    

    /**
        * submit add sector using Ajax.
    */
   public function actionAddsector()
        {
            $model = new sectors();
            if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->sector_name = $_POST['sectors']['sector_name'];       
                if($model->save()){
                return $this->redirect(['create']);
                }else{
                   echo "not";
                }
                }}
        } 
    /**
     * Creates a new Plot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed 
     */
    public function actionCreate()
        {
            $model = new Plot();
            if ($model->load(Yii::$app->request->post())) {
                $file_no= $_POST['Plot']['file_no'];
                // echo"<pre>";print_r($file_no);exit;
                $sub_sector= $_POST['Plot']['sub_sector'];
                $plot_no =$_POST['Plot']['house_no'];
                $street_no = $_POST['Plot']['street_no'];
                $allpllot= Plot::find()->where(['file_no'=>$file_no])->andwhere(['sub_sector'=>$sub_sector])->andwhere(['house_no'=>$plot_no])->andwhere(['street_no'=>$street_no])->all();
                //echo"<pre>";print_r($allpllot);exit;
                if(!empty($allpllot)){
                    Yii::$app->session->setFlash('danger', 'Counsmer already exisit with same file no against this sector');
                    return $this->render('create', [
                        'model' => $model,
    
                    ]);
                }
                

                $service = $model->services;
                $model->services='1';
                $model->created_date = date('Y-m-d H:i:s');
                $model->possession_date = $_POST['possession_date'];
                $model->connection_date =  $_POST['connection_date'];
                $model->end_date = date('Y-m-d ');
				$model->document = 'd';

                $model->created_by = Yii::$app->user->identity->id;
                $activitycreate = Yii::$app->user->identity->username.' Created the new consumer '.$_POST['Plot']['allottee_name']; 
                $activity = New Activity(); 
                $activit = $activity->activityrecord($activitycreate, true);
                if ($model->validate()){
                    if($model->save()){
                         Yii::$app->session->setFlash('success', 'Record Created Successfully');
                        
                        $cat= Category::find()->select('category_name')->where(['id'=>$model->cat_id])->one();
                        if ($cat->category_name=='Category-1 50x90') {
                            $model->consumer_no = 'CAT-1/'.$model->sub_sector.'-'.$model->id;
                             if($model->save()){
                            } 
                            else{
                                    echo '<pre>';
                                    echo print_r($model);
                                    exit;
                            }
                        } 
                        elseif ($cat->category_name=='Category-2 40x80') {
                            $model->consumer_no = 'CAT-2/'.$model->sub_sector.'-'.$model->id;
                            if($model->save()){
                            }
                            else{
                                    echo '<pre>';
                                    echo print_r($model);
                                    exit;
                            }
                        }
                        
                        elseif ($cat->category_name=='Category-3 35x70') {
                            $model->consumer_no = 'CAT-3/'.$model->sub_sector.'-'.$model->id;
                            if($model->save()){
                            }
                            else{
                                    echo '<pre>';
                                    echo print_r($model);
                                    exit;
                            }
                        }  

                        elseif ($cat->category_name=='Category-4 30x60') {
                            $model->consumer_no = 'CAT-4/'.$model->sub_sector.'-'.$model->id;
                            if($model->save()){
                            }
                            else{
                                    echo '<pre>';
                                    echo print_r($model);
                                    exit;
                            }
                        }


                       elseif ($cat->category_name=='Category-5 25x40') {
                            $model->consumer_no = 'CAT-5/'.$model->sub_sector.'-'.$model->id;
                            if($model->save()){
                            }
                            else{
                                    echo '<pre>';
                                    echo print_r($model);
                                    exit;
                            }
                        } 

                        
                            if (!empty($service)) {
                              

                        foreach ($service as $val)
                        { 
                            $services = New ConsumerServices();
                            $services->service_id = $val;
                            $services->consumer_id = $model->id;
                            if($services->save()){

                            }
                            else{
                                    echo '<pre>';
                                    echo print_r($services);
                                    exit;
                            }
                        }}

                        //  if (!empty($service)) {
                        //     $model->services = '1';
                        // }
                        return $this->redirect(['view', 'id' => $model->id]);
                        }
                        else{
                            echo '<pre>';
                            print_r($model);
                            exit;
                        }}
                        else{
                            $errors = $model->errors;

                        }}
                return $this->render('create', [
                    'model' => $model,

                ]);
        }
     public function actionBill($id)
        {
            $tenure = \app\models\Plot::find()->select('id')->from('plot')->where('id='.$id)->one();
            $this->layout = 'main';
             $bills = \app\models\Bill::find()->Where(['consumer_id'=>$tenure->id])->all();
            return $this->render('bill', [
                'model' => $bills,
            ]);
        }
    /**
     * Updates an existing Plot model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
        {
            $model = $this->findModel($id);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                 Yii::$app->session->setFlash('success', 'Record updated Successfully');
                $activitycreate = Yii::$app->user->identity->username.' updated The consumer '.$_POST['Plot']['allottee_name'];

           $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
                return $this->redirect(['view', 'id' => $model->id]);
            }
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    /**
     * Deletes an existing Plot model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
        {
             $this->findModel($id)->delete();
        $activitycreate = Yii::$app->user->identity->username.' Deleted the consumer '.$model->allottee_name;
         $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
        Yii::$app->session->setFlash('success', 'Record Deleted Successfully');
        return $this->redirect(['index']);
        Yii::$app->session->setFlash('success', 'Record Deleted Successfully');
        }
    /**
     * Finds the Plot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Plot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
        {
            if (($model = Plot::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
}
