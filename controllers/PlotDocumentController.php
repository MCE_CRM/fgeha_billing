<?php

namespace app\controllers;

use Yii;
use app\models\PlotDocument;
use app\models\PlotDocumentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * PlotDocumentController implements the CRUD actions for PlotDocument model.
 */
class PlotDocumentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PlotDocument models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlotDocumentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PlotDocument model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PlotDocument model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function getName($id)
    {
        $name = \app\models\EstateWing::find()->where(['id'=>$id])->one();
        // var_dump($name->name);
        // die;
      return $name->name;
    }
    public function actionCreate()
    {
        $id=$_GET['id'];
        $tenure = \app\models\Plot::find()->select('id')->from('plot')->where('id='.$id)->one();
        //$this->layout = 'main';
        $bills = \app\models\PlotDocument::find()->Where(['plotId'=>$tenure->id])->all();
        // var_dump($bills);
        // die;
        $searchModel = new PlotDocumentSearch();
        $dataProvider = $searchModel->searchfileById(Yii::$app->request->queryParams);
        $model = new PlotDocument();
        
        // $olds= $model->document;
        // $old = json_decode($model->document); 
        if ($model->load(Yii::$app->request->post())) {
            $files =  $model->document;
            
            $model->document = UploadedFile::getInstances($model, 'document');
           
            if (!empty($model->document )) {
                
           // if ($model->document && $model->validate()) {
            //     var_dump($model->document);
            // die;
                foreach ($model->document as $file) {
                    // $i++;   
                $file->saveAs('uploads/' . $file->baseName . '.' . $file->extension);

                $names[]=$file->name;
                $names++;
                $total_names = json_encode($names);
                  
                }
        
                $model->document =  $total_names;
            // $files =  $model->document;
            
            // $model->document = UploadedFile::getInstance($model, 'document');
            // if (!empty($model->document )) {

            // if ($model->document && $model->validate()) {
            //     foreach ($model->document as $file) {
                   
                   
            //     $file->saveAs('uploads/' . $model->document->baseName . '.' . $model->document->extension);

            //     // $names[]=$file->name;
            //     // $names++;
            //     // $total_names = $names;
            //     }
            //     // $merge=(array_merge($total_names,$old));
            //     // $name = json_encode($merge);

            //    // $model->document =  $name;
            //        if ($model->save()) {
            //      Yii::$app->session->setFlash('success', 'Record updated Successfully');
                   
            //    }
            // }

            // }
           
            $model->plotId = $id;
            if($model->save()){
                return $this->render('create', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);Yii::$app->session->setFlash('success', 'Record updated Successfully');
            }}
        //}
    }

        return $this->render('create', [
            'model' => $model,
            'bills' => $bills,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing PlotDocument model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PlotDocument model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PlotDocument model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PlotDocument the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PlotDocument::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionDownload($id) 
{ 
    $download = PlotDocument::findOne($id); 
    $path=Yii::getAlias('@webroot').'/uploads/'.$download->document;

    if (file_exists($path)) {
        return Yii::$app->response->sendFile($path);
    }
}
}
