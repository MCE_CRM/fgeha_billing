<?php

namespace app\controllers;

use Yii;
use app\models\Sectors;
use app\models\sectorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Activity; 

/**
 * SectorController implements the CRUD actions for sectors model.
 */
class SectorController extends Controller
{
    /**
     * {@inheritdoc}
     */
        public function behaviors()
    {
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='sector/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ], 
        ];

        return $behaviors;
    }


    /**
     * Lists all sectors models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Sectorsearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single sectors model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' views the sector '.$model->sector_name;
         $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new sectors model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new sectors();

        if ($model->load(Yii::$app->request->post()) )
        {

            if($model->validate()){
            $model->created_on=date('Y-m-d H:i:s');
            $model->created_by = Yii::$app->user->identity->id;
            
            $model->updated_on=date('y-m-d H:i:s');
            $model->updated_by = Yii::$app->user->identity->id;
            $activitycreate = Yii::$app->user->identity->username.' Created The new sector '.$_POST['sectors']['sector_name'];

            $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
              
            $model->save(false);

            return $this->redirect(['plot/create', 'id' => $model->id]);
            }
            else{
                echo "<pre>";
                print_r($model);

            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing sectors model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $activitycreate = Yii::$app->user->identity->username.' Updated The sector  '.$_POST['sectors']['sector_name'];

             $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing sectors model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
         $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' Deleted the sector'.$model->sector_name;
         $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
        return $this->redirect(['index']);

        
    }

    /**
     * Finds the sectors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return sectors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = sectors::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
