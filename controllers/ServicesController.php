<?php

namespace app\controllers;

use Yii;
use app\models\Services;
use app\models\servicesSearch; 
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl; 
use app\models\Activity;
/**
 * ServicesController implements the CRUD actions for services model.
 */
class ServicesController extends Controller
{
    /**
     * {@inheritdoc}
     */
        public function behaviors()
    {
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='services/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ], 
        ];

        return $behaviors;
    }


    /**
     * Lists all services models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new servicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single services model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' views the services'.$model->service_name;
        $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
        return $this->render('view', [
            'model' => $model,
        ]); 
    }

    /**
     * Creates a new services model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    { 
        $model = new services();

        if ($model->load(Yii::$app->request->post())){
            $model->created_by = Yii::$app->user->id;
            $model->created_on = date('Y-m-d H:i:s');
            $model->created_by = Yii::$app->user->id;
            $model->updated_on = date('Y-m-d H:i:s');
            $activitycreate = Yii::$app->user->identity->username.' Created the new service '.$_POST['Services']['service_name'];

           $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
            if ($model->validate()){
                    if($model->save()){
                        return $this->redirect(['view', 'id' => $model->id]);
                    }else{
                        echo '<pre>';
                        print_r($model);
                        exit;
                    }
                }else{
                    $errors = $model->errors;
                    print_r($errors);exit;
                }
                }
                return $this->render('create', [
                    'model' => $model,
                ]);
    }

    /**
     * Updates an existing services model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $activitycreate = Yii::$app->user->identity->username.' updated the service '.$_POST['services']['service_name'];

           $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing services model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
         $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' Deleted the service '.$model->service_name;
        $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
        return $this->redirect(['index']);
    }

    /**
     * Finds the services model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return services the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = services::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
