<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Bill;
use app\models\Activity;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['bill-print','logout','print-bill'],
                        'allow' => true,
                        
                    ],
                    [
                        
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['GET'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }



      public function actionUserupdate($id){
        $model = User::findone($id);
        if($model->load(yii::$app->request->post()) && $model->save()){
            $model->email = ($_POST['User']['email']);
            $model->username = ($_POST['User']['username']);
            $model->password_hash = password_hash($_POST['User']['password_hash'],1);
            $model->auth_key = md5(random_bytes(5));
            $model->password_reset_token = password_hash(random_bytes(10), PASSWORD_DEFAULT);
            if($model->save()){
            return $this->redirect(['user', 'id'=> $model->id]);
        }}
        else{

        return $this->render('update', ['model'=>$model]);
    }}


     public function actionUserpass($id){
        $model = User::findone($id);
        if($model->load(yii::$app->request->post()) && $model->save()){
            $model->email = ($_POST['User']['email']);
            $model->username = ($_POST['User']['username']);
            $model->password_hash = password_hash($_POST['User']['password_hash'],1);
            $model->auth_key = md5(random_bytes(5));
            $model->password_reset_token = password_hash(random_bytes(10), PASSWORD_DEFAULT);
            if($model->save()){
            return $this->redirect(['profile', 'id'=> $model->id]);
        }}
        else{

        return $this->render('update', ['model'=>$model]);
    }}
      public function actionProfile()
    {
     $tenure = Yii::$app->user->identity->id;
        
        $this->layout = 'main';
         $bills = \app\models\User::find()->Where(['id'=>Yii::$app->user->identity->id])->all();
          
       return $this->render('profile', [
            'model' => $bills,

        ]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        /**
         * Sum Quarterly Income
         */

        


        $monthly = Yii::$app->db->createCommand("SELECT sum(total_bill) FROM bill WHERE  DATE_SUB(CURDATE(), INTERVAL 4 MONTH)AND status=1");
        $m_sum = $monthly->queryScalar();
          if ( $m_sum==0) {
            $m_sum='0';
        }else{
            $m_sum=$m_sum;
        }
        /**
         * Sum Yearly Income
         */

        // SELECT * FROM `dt_table` WHERE  date between  DATE_FORMAT(CURDATE() ,'%Y-01-01') AND CURDATE()

        $yearly = Yii::$app->db->createCommand("SELECT sum(total_bill) FROM bill WHERE DATE_FORMAT(CURDATE() ,'%Y-01-01') AND CURDATE() AND status=1");
        $y_sum = $yearly->queryScalar();
         if ( $y_sum==0) {
            $y_sum='0';
        }else{
            $y_sum=$y_sum;
        }

        /**
         * percentage calculation
         */

        $total_amount = Bill::find()->sum('total_bill');
        $paid_amount = Bill::find()->where(['status'=>1])->sum('total_bill');
        // $percentage = round($paid_amount*100/$total_amount);
        $percentage = 0;

        /**
        *Tenure bill Graph
        */ 
        $label_arry =  array();
        $k =0;
        $label1 = \app\models\Bill::find()->select( 'sum(total_bill) as total_bill')->from('bill')->groupBy('tinure')->where('status=1') ->all();
            foreach($label1 as $valu){
            $label_arry[$k] = $valu->total_bill;
            $k++;
            }  

        /**
        *bill Pie Graph
        */ 
        $pie =  array();
        $k =0;
        $label3 = \app\models\Bill::find()->select( 'sum(total_bill) as total_bill')->from('bill')->groupBy('sector')->where('status=1') ->all();
            foreach($label3 as $valu){
            $pie[$k] = $valu->total_bill;
            $k++;
            }      

        /**
        *Tenure Month Graph
        */ 

        $label_arr =  array();
        $i =0;
        $label = \app\models\Tenure::find()->all();
            foreach($label as $value){
            $label_arr[$i] = $value->tenure_name;
            $i++;
            }

        /**
        *Tenure Month pie Graph
        */ 

        $sector =  array();
        $i =0;
        $label2 = \app\models\Sectors::find()->all();
            foreach($label2 as $value){
            $sector[$i] = $value->sector_name;
            $i++;
            }    
         
        return $this->render('index', ['monthly' => $m_sum, 'yearly' => $y_sum, 'label' => $label_arr,
                    'bill' => $label_arry, 'percentage'=>$percentage, 'totalbill'=>$pie, 'sector'=>$sector]);
    

    }

    /** 
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
 
        $model->password = '';
        $this->layout = 'blank';
        return $this->render('login', [
            'model' => $model,
        ]);
    } 

    /**
    *Bill Print
    *
    */

    public function actionPrintBill(){

      $this->layout = 'blank';
       return $this->render('printbill');
    }

    public function actionBillPrint()
    {
       $this->layout = 'generatebill';
       $billprint = $_POST['searchbill'];
       $bills = \app\models\Plot::find()->Where(['file_no'=>$billprint])->all();
       foreach ($bills as $val){

     $bill = \app\models\Bill::find()->Where(['consumer_id'=>$val->id ])->orderby(['created_on'=>SORT_DESC])->all();

         return $this->render('billprint', [
            'model' => $bill, 
        ]);
        }  
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        $this->redirect('login');
    }

      public function actionUpdate($id)

    {
        $model = User::findone($id);
        if($model->load(yii::$app->request->post()) && $model->save()){
            return $this->redirect(['profile', 'id'=> $model->id]);
        }
        else{

        return $this->render('update', ['model'=>$model]);
    }
    }

    // New User

     public function actionCreate()
    {
        $model = new User();
        if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
            // form inputs are valid, do something here
            $model->username = $_POST['User']['username'];
            $model->email = $_POST['User']['email'];
            $model->password_hash = password_hash($_POST['User']['password_hash'],1);
            $model->auth_key = md5(random_bytes(5));
            $model->password_reset_token = password_hash(random_bytes(10), PASSWORD_DEFAULT);
			$model->created_at = date('Y-m-d H:i:s');
			$model->updated_at = date('Y-m-d H:i:s');
            if($model->save()){
            return $this->redirect(['plot/user']);
            }else{
                echo '<pre>';
                print_r($model);
                echo '</pre>';die();
            } 
        }
    }

    return $this->render('_form', [
        'model' => $model,
    ]);
}


   

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

       public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['user']);
    }
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


      public function actionBackup()
    {
        return $this->render('back');
    }

    // public function drop()
    // {
    //     DROP TABLE bill;
    // }


     public function actionRemove()
        {
           
                  \Yii::$app->db->createCommand("UPDATE bill SET payment= 0 , status = 0  WHERE status=1")->execute();
        
            
        }

    
}
