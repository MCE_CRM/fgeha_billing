<?php

namespace app\controllers;

use Yii;
use app\models\sub_sectors;
use app\models\sub_sectorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl; 
use app\models\Activity;

/**
 * Sub_sectorController implements the CRUD actions for sub_sectors model.
 */
class Sub_sectorController extends Controller
{
    /**
     * {@inheritdoc}
     */
       public function behaviors()
    {
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='sub_sector/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ], 
        ];

        return $behaviors;
    }


    /**
     * Lists all sub_sectors models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new sub_sectorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single sub_sectors model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' views the sub-sector '.$model->name;
        $activity = Activity::activityrecord($activitycreate, true);
        return $this->render('view', [
            'model' => $model,
        ]); 
    }

     // public function actionLists($id)
     // {
     //     $countsubsectors = SubSectors:: find()
     //     -> where(['sector_id'=> $id])
     //     -> count();

     //      $subsectors = SubSectors:: find()
     //     -> where(['sector_id'=> $id])
     //     -> all();

     //     if ( $countsubsectors>0) {
     //         foreach ($subsectors as $subsectors) {
     //             echo "<option value='".$subsectors->id."'>".$subsectors->sector_name."</option>";
     //         }
     //     }
     //     else{
     //        echo "<option>-</option>";
     //     }
     // }    


    /**
     * Creates a new sub_sectors model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new sub_sectors();

         if ($model->load(Yii::$app->request->post()) )
        {

            if($model->validate()){
                $model->created_on=date('Y-m-d H:i:s');
            $model->created_by = Yii::$app->user->id;
            
            $model->updated_on=date('y-m-d H:i:s');
            $model->updated_by = Yii::$app->user->id;
            $activitycreate = Yii::$app->user->identity->username.' Created the new sub-sector '.$_POST['subsectors']['name'];

            $activity = Activity::activityrecord($activitycreate, true);
              
            $model->save(false);

            return $this->redirect(['view', 'id' => $model->id]);
            }
            else{
                print_r($model);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing sub_sectors model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $activitycreate = Yii::$app->user->identity->username.' updated The sub-sector '.$_POST['subsectors']['name'];

            $activity = Activity::activityrecord($activitycreate, true);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing sub_sectors model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' Deleted the '.$model->name;
        $activity = Activity::activityrecord($activitycreate, true);
        return $this->redirect(['index']);
    }

    /**
     * Finds the sub_sectors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return sub_sectors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = sub_sectors::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
