<?php

namespace app\controllers;

use Yii;
use app\models\SubSectors;
use app\models\subsectorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Activity;

/**
 * SubsectorController implements the CRUD actions for subsectors model.
 */
class SubsectorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    
        public function behaviors()
    {
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='subsectors/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ], 
        ];

        return $behaviors;
    }


    /**
     * Lists all subsectors models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new subsectorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single subsectors model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' views the sub-Sector  '.$model->name;
        $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
        return $this->render('view', [
            'val' => $model, 
        ]);
    }

    /**
     * Creates a new subsectors model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SubSectors();

       
        if ($model->load(Yii::$app->request->post()) )
        {

            if($model->validate()){
            $model->created_on=date('Y-m-d');
            $model->created_by = Yii::$app->user->identity->id;
            
            $model->updated_on=date('y-m-d');
            $model->updated_by = Yii::$app->user->identity->id;
             $activitycreate = Yii::$app->user->identity->username.' Created The new subsector '.$_POST['SubSectors']['name'];

           $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);
              
            $model->save(false);

            return $this->redirect(['plot/create', 'id' => $model->id]);
            }
            else{
                echo "<pre>";
                print_r($model);

            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing subsectors model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->updated_by = Yii::$app->user->identity->id;
             $activitycreate = Yii::$app->user->identity->username.' Updated The subsector '.$_POST['SubSectors']['name'];

           $activity = New Activity();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing subsectors model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $activitycreate = Yii::$app->user->identity->username.' Deleted the subsector '.$model->name;

       $activity = New Activity();
        $activit = $activity->activityrecord($activitycreate, true);

        return $this->redirect(['index']);
    }

    /**
     * Finds the subsectors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return subsectors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = subsectors::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
