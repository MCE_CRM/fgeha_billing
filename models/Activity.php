<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property int $id
 * @property string $activity
 * @property int $created_by
 * @property string $created_on
 */
class Activity extends \yii\db\ActiveRecord
{
    /** 
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'activity', 'created_by', 'created_on'], 'required'],
            [['id', 'created_by'], 'integer'],
            [['created_on'], 'safe'],
            [['activity'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'activity' => 'Activity',
            'un.username' => 'Created By',
            'created_on' => 'Created On',
        ];
    }

    public function activityrecord($activitycreate)
    {
        $activity = new Activity();
            $activity->activity = $activitycreate;
            $activity->created_on = date("Y-m-d H:i:s");
            $activity->created_by = Yii::$app->user->id;
            $activity->save();
            // echo "<pre>";
            // print_r($activity);
            // exit();
    }
   

                 public function getun()
                {
                    return $this->hasOne(User::className(),['id' => 'created_by']);
                }
}
