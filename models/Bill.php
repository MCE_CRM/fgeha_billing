<?php

namespace app\models;

use Yii;
 
/** 
 * This is the model class for table "bill".
 *
 * @property int $id
 * @property int $consumer_id
 * @property int $total_months_conservancy
 * @property int $water_arrears
 * @property string $water_remarks
 * @property int $conservancy_amount
 * @property int $total_water_and_conservancy_amount
 * @property int $tinure 
 * @property int $dues_amount
 * @property int $per_month_charges
 * @property int $no_of_months
 * @property int $total_amount_of_current_months
 * @property int $advanced_pay
 * @property int $arrears

* @property int $water_bill 
 * @property int $arrears_period
 * @property int $total_bill
 * @property int $after_due_date_charges
 * @property int $total_after_due_date
 * @property int $balance_arears
 * @property string $remarks
 * @property int $conservancy_charges_per_month

 * @property string $billing_months
 * @property string $issue_date
 * @property string $due_date 
  * @property string $cnic
 * @property string $reference_no
 */
class Bill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public static function tableName()
    {
        return 'bill';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['consumer_id', 'water_arrears',  'conservancy_amount', 'dues_amount',  'arrears', 'arrears_period', 'total_bill', 'after_due_date_charges', 'total_after_due_date', 'issue_date', 'due_date','status'], 'required'],
            [['file'],'file', 'maxFiles'=>1000],
            [['consumer_id', 'total_months_conservancy', 'water_arrears', 'conservancy_amount', 'total_water_and_conservancy_amount', 'tinure', 'dues_amount', 'per_month_charges', 'no_of_months', 'total_amount_of_current_months', 'advanced_pay', 'arrears', 'arrears_period', 'total_bill', 'after_due_date_charges', 'total_after_due_date', 'balance_arears','payment','debit','other_charges'], 'integer'],
            [['issue_date', 'water_bill', 'due_date','created_on','updated_on',  'advanced_pay',  'tinure','billing_months','email','cnic','sector'], 'safe'],
            [['water_remarks','document'], 'string', 'max' => 250],
            [['remarks'], 'string', 'max' => 350],
            [['billing_months'], 'string', 'max' => 255],
            [['reference_no'], 'string', 'max' => 13],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Bill ID',
            'consumer_id' => 'Name',
            'cnic'=>'Cnic',
            'email'=>'Email',
            'total_months_conservancy' => 'Total Months Conservancy',
            'water_arrears' => 'Water Arrears',
            'water_remarks' => 'Water Remarks',
            'conservancy_amount' => 'Conservancy Amount',
            'total_water_and_conservancy_amount' => 'Total Water And Conservancy Amount',
            'tinure' => 'Tinure',
            'sector' => 'Sector',
            'dues_amount' => 'Dues Amount',
            'per_month_charges' => 'Per Month Charges',
            'no_of_months' => 'No Of Months',
            'total_amount_of_current_months' => 'Total Amount Of Current Months',
            'advanced_pay' => 'Advanced Pay',
            'debit' => 'debit Pay',
            'arrears' => 'Arrears',
             'water_bill' => 'water_bill',
            'arrears_period' => 'Arrears Period',
            'total_bill' => 'Total Bill',
            'Status'=>'Status',
            'after_due_date_charges' => 'After Due Date Charges',
            'total_after_due_date' => 'Total After Due Date',
            'balance_arears' => 'Balance Arears',
            'remarks' => 'Remarks',
            'conservancy_charges_per_month' => 'Conservancy Charges Per Month',
            'billing_months' => 'Tenure',
            'issue_date' => 'Issue Date',
            'due_date' => 'Due Date',
            'reference_no' => 'Reference No',
        ];
    }

   
            public function getconsumerId()
            {
                return $this->hasOne(Plot::className(),['id' => 'consumer_id']);
            }

            public function getfileNo()
            {
                return $this->hasOne(Plot::className(),['id' => 'consumer_id']);
            }
            public function getsectorName()
            {
                return $this->hasOne(Sectors::className(),['id' => 'sector']);
            }

            public function getsubsectorName()
            {
                return $this->hasOne(SubSectors::className(),['name' => 'sector']);
            }

            public function gettenureName()
            {
                return $this->hasOne(Tenure::className(),['id' => 'tinure']);
            }    
               


   
}
 