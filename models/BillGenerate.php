<?php

namespace app\models;


use Yii;

/**
 * This is the model class for table "bill_generate".
 *
 * @property int $id
 * @property int $tenure_id
 * @property int $status
 * @property int $print_status
 * @property string $created_on
 * @property int $created_by
 * @property string|null $updated_on
 * @property int|null $updated_by
 */
class BillGenerate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bill_generate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'status', 'print_status', 'created_on', 'created_by'], 'required'],
            [[ 'status', 'print_status', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on','sector_id','tenure_id','file_no'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tenure_id' => 'Tenure Name',
            'sector_id' => 'Sector Name',
            'status' => 'Status',
            'file_no'=>'File No',
            'print_status' => 'Print Status',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function gettenureId()
    {
        return $this->hasOne(Tenure::className(),['id' => 'tenure_id']);
    }
    
}
