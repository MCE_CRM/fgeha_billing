<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BillGenerate;

/**
 * BillGenerateSearch represents the model behind the search form of `app\models\BillGenerate`.
 */
class BillGenerateSearch extends BillGenerate
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id',  'created_by', 'updated_by'], 'integer'],
            [['created_on','tenure_id', 'status', 'print_status',  'updated_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BillGenerate::find();
        $query->leftjoin('tenure','tenure.id=bill_generate.tenure_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            
            'status' => $this->status,
            'print_status' => $this->print_status,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);
         $query->andFilterWhere(['like', 'tenure.tenure_name', $this->tenure_id])
        ->andFilterWhere(['like', 'bill_generate.created_on', $this->created_on]);

        return $dataProvider;
    }
}
