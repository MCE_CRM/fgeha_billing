<?php

namespace app\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Bill;
use DateTime;
 
/**
 * BillSearch represents the model behind the search form of `app\models\Bill`.
 */
class BillSearch extends Bill
{
    /**
     * {@inheritdoc}
     */
     public $file_no;
    public function rules()
    {
        return [
            [['id','total_months_conservancy','water_arrears', 'conservancy_amount', 'total_water_and_conservancy_amount', 'tinure', 'dues_amount', 'per_month_charges', 'no_of_months', 'total_amount_of_current_months', 'advanced_pay', 'arrears', 'arrears_period', 'total_bill', 'after_due_date_charges', 'total_after_due_date', 'balance_arears'], 'integer'],
            [['water_remarks','consumer_id', 'status','file_no', 'cnic',  'allottee_name','remarks', 'issue_date','billing_months', 'status','sector','due_date', 'reference_no'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bill::find();
   
        $query->leftjoin('plot','plot.id=bill.consumer_id');
  
      
   
        $query->leftjoin('sub_sectors','sub_sectors.name=bill.sector');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;

        }
         if ( ! is_null($this->issue_date) && strpos($this->issue_date, '-') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->issue_date);
             
            // $start_date = DateTime::createFromFormat('YYYY/MM/DD', $start_date);

            $start_date = date("Y-m-d", strtotime($start_date));
           

            $end_date = date("Y-m-d", strtotime($end_date));
          
            $query->andFilterWhere(['between','bill.issue_date', $start_date, $end_date]);



        }
 
    
        // grid filtering conditions
        $query->andFilterWhere([
            // 'id' => $this->id,
            'total_months_conservancy' => $this->total_months_conservancy,
            'water_arrears' => $this->water_arrears,
            'conservancy_amount' => $this->conservancy_amount,
            'total_water_and_conservancy_amount' => $this->total_water_and_conservancy_amount,
            'tinure' => $this->tinure,
            'dues_amount' => $this->dues_amount,
            'per_month_charges' => $this->per_month_charges,
            'no_of_months' => $this->no_of_months,
            'total_amount_of_current_months' => $this->total_amount_of_current_months,
            'advanced_pay' => $this->advanced_pay,
            'arrears' => $this->arrears,
            'arrears_period' => $this->arrears_period,
            'total_bill' => $this->total_bill,
            'after_due_date_charges' => $this->after_due_date_charges,
            'total_after_due_date' => $this->total_after_due_date,
            'balance_arears' => $this->balance_arears, 
            'due_date' => $this->due_date,
           
        ]);
 
        $query->andFilterWhere(['like', 'water_remarks', $this->water_remarks])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
               ->andFilterWhere(['=', 'bill.id', $this->id])
              // ->andFilterWhere(['like', 'issue_date', $this->issue_date])
            ->andFilterWhere(['like', 'bill.status', $this->status])
            ->andFilterWhere(['like', 'sub_sectors.name', $this->sector])
                 ->andFilterWhere(['like', 'billing_months', $this->billing_months])
              ->andFilterWhere(['like', 'plot.allottee_name', $this->consumer_id])
             ->andFilterWhere(['like', 'plot.cnic', $this->cnic])
              ->andFilterWhere(['like', 'plot.file_no', $this->file_no])
            // ->andFilterWhere(['like', 'bill.billing_months', $this->billing_months])
            ->andFilterWhere(['like', 'reference_no', $this->reference_no]);
            

        return $dataProvider;
    }
} 
 