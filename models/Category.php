<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $category_name
 * @property string $water_bill
 * @property string $conservancy
 * @property int|null $siz_width
 * @property int|null $siz_height
 * @property int|null $created_by
 * @property string|null $created_on 
 * @property int|null $updated_by
 * @property string|null $updated_on
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name',  'conservancy'], 'required'],
            [['siz_width', 'siz_height', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['category_name', 'conservancy'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
            
            'conservancy' => 'Conservancy',
            'siz_width' => 'Size Width',
            'siz_height' => 'Size Height',
            'un.username' => 'Created By',
            'created_on' => 'Created On',
            'name.username' => 'Updated By',
            'updated_on' => 'Updated On',
        ];
    }

                public function getname()
                {
                    return $this->hasOne(User::className(),['id' => 'updated_by']);
                }

                 public function getun()
                {
                    return $this->hasOne(User::className(),['id' => 'created_by']);
                }

}
