<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consumer_services".
 *
 * @property int $id
 * @property int $consumer_id
 * @property int $service_id
 * @property int|null $created_by
 * @property string|null $created_on
 * @property int|null $updated_by
 * @property string|null $updated_on
 */
class ConsumerServices extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consumer_services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['consumer_id', 'service_id'], 'safe'],
            [[ 'created_by', 'updated_by'], 'integer'],
            [['created_on','consumer_id', 'updated_on'], 'safe'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'consumer_id' => 'Consumer ID',
            'service_id' => 'Service ID',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
        ];
    }
}
