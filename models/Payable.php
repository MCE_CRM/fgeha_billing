<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payable".
 *
 * @property int $id
 * @property int $consumer_id
 * @property int $total_months_conservancy
 * @property int $water_arrears
 * @property string $water_remarks
 * @property int $conservancy_amount
 * @property int $total_water_and_conservancy_amount
 * @property int $tinure
 * @property int $dues_amount
 * @property int $per_month_charges
 * @property int $no_of_months
 * @property int $total_amount_of_current_months
 * @property int $advanced_pay
 * @property int $arrears
 * @property int $arrears_period
 * @property int $total_bill
 * @property int $after_due_date_charges
 * @property int $total_after_due_date
 * @property int $balance_arears
 * @property string $remarks
 * @property int $water_charges_per_month
 * @property int $conservancy_charges_per_month
 * @property int $four_months_water_charges
 * @property int $four_months_conservancy_charges
 * @property string $billing_months
 * @property string $issue_date
 * @property string $due_date
 */
class Payable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payable';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['consumer_id', 'total_months_conservancy', 'water_arrears', 'water_remarks', 'conservancy_amount', 'total_water_and_conservancy_amount', 'tinure', 'dues_amount', 'per_month_charges', 'no_of_months', 'total_amount_of_current_months', 'advanced_pay', 'arrears', 'arrears_period', 'total_bill', 'after_due_date_charges', 'total_after_due_date', 'balance_arears', 'remarks', 'water_charges_per_month', 'conservancy_charges_per_month', 'four_months_water_charges', 'four_months_conservancy_charges', 'billing_months', 'issue_date', 'due_date'], 'required'],
            [['consumer_id', 'total_months_conservancy', 'water_arrears', 'conservancy_amount', 'total_water_and_conservancy_amount', 'tinure', 'dues_amount', 'per_month_charges', 'no_of_months', 'total_amount_of_current_months', 'advanced_pay', 'arrears', 'arrears_period', 'total_bill', 'after_due_date_charges', 'total_after_due_date', 'balance_arears', 'water_charges_per_month', 'conservancy_charges_per_month', 'four_months_water_charges', 'four_months_conservancy_charges'], 'integer'],
            [['issue_date', 'due_date'], 'safe'],
            [['water_remarks'], 'string', 'max' => 250],
            [['remarks'], 'string', 'max' => 350],
            [['billing_months'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Bill No',
            'consumer_id' => 'Name',
            'total_months_conservancy' => 'Total Months Conservancy',
            'water_arrears' => 'Water Arrears',
            'water_remarks' => 'Water Remarks',
            'conservancy_amount' => 'Conservancy Amount',
            'total_water_and_conservancy_amount' => 'Total Water And Conservancy Amount',
            'tinure' => 'Tinure',
            'dues_amount' => 'Dues Amount',
            'per_month_charges' => 'Per Month Charges',
            'no_of_months' => 'No Of Months',
            'total_amount_of_current_months' => 'Total Amount Of Current Months',
            'advanced_pay' => 'Advanced Pay',
            'arrears' => 'Arrears',
            'arrears_period' => 'Arrears Period',
            'total_bill' => 'Total Bill',
            'after_due_date_charges' => 'After Due Date Charges',
            'total_after_due_date' => 'Total After Due Date',
            'balance_arears' => 'Balance Arears',
            'remarks' => 'Remarks',
            'water_charges_per_month' => 'Water Charges Per Month',
            'conservancy_charges_per_month' => 'Conservancy Charges Per Month',
            'four_months_water_charges' => 'Four Months Water Charges',
            'four_months_conservancy_charges' => 'Four Months Conservancy Charges',
            'billing_months' => 'Billing Months',
            'issue_date' => 'Issue Date',
            'due_date' => 'Due Date',
        ];
    }

     public function getconsumerId()
                { 
                    return $this->hasOne(Plot::className(),['id' => 'consumer_id']);
                }

}
