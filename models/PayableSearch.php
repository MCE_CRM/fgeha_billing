<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Payable;
 
/**
 * PayableSearch represents the model behind the search form of `app\models\Payable`.
 */
class PayableSearch extends Payable
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id',  'total_months_conservancy', 'water_arrears', 'conservancy_amount', 'total_water_and_conservancy_amount', 'tinure', 'dues_amount', 'per_month_charges', 'no_of_months', 'total_amount_of_current_months', 'advanced_pay', 'arrears', 'arrears_period', 'total_bill', 'after_due_date_charges', 'total_after_due_date', 'balance_arears', 'water_charges_per_month', 'conservancy_charges_per_month', 'four_months_water_charges', 'four_months_conservancy_charges'], 'integer'],
            [['water_remarks', 'remarks', 'consumer_id','billing_months', 'issue_date', 'due_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bill::find();
        $query->leftjoin('plot','plot.id=bill.consumer_id');
        // echo"<pre>";
        // print_r($query);
        // exit();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            // 'id' => $this->id,
            'total_months_conservancy' => $this->total_months_conservancy,
            'water_arrears' => $this->water_arrears,
            'conservancy_amount' => $this->conservancy_amount,
            'total_water_and_conservancy_amount' => $this->total_water_and_conservancy_amount,
            'tinure' => $this->tinure,
            'dues_amount' => $this->dues_amount,
            'per_month_charges' => $this->per_month_charges,
            'no_of_months' => $this->no_of_months,
            'total_amount_of_current_months' => $this->total_amount_of_current_months,
            'advanced_pay' => $this->advanced_pay,
            'arrears' => $this->arrears,
            'arrears_period' => $this->arrears_period,
            'total_bill' => $this->total_bill,
            'after_due_date_charges' => $this->after_due_date_charges,
            'total_after_due_date' => $this->total_after_due_date,
            'balance_arears' => $this->balance_arears,
            'water_charges_per_month' => $this->water_charges_per_month,
            'conservancy_charges_per_month' => $this->conservancy_charges_per_month,
            'four_months_water_charges' => $this->four_months_water_charges,
            'four_months_conservancy_charges' => $this->four_months_conservancy_charges,
            'issue_date' => $this->issue_date,
            'due_date' => $this->due_date,
        ]);

        $query->andFilterWhere(['like', 'water_remarks', $this->water_remarks])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
             ->andFilterWhere(['like', 'bill.id', $this->id])
            ->andFilterWhere(['like', 'plot.allottee_name', $this->consumer_id])
            ->andFilterWhere(['like', 'plot.sub_sector', $this->remarks])
            ->andFilterWhere(['like', 'billing_months', $this->billing_months]);

        return $dataProvider;
    }
}
