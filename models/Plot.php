<?php

namespace app\models;

use Yii;
use app\models\Sectors;
use app\models\Subsectors;
use app\models\Services;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plot".
 *
 * @property int $id
 * @property string $file_no
 * @property string $connection_date
 * @property int $created_by
 * @property string $allottee_name
 * @property string $sector
 @property string $sub_sector
 * @property string $possession_date
 * @property string $end_date
 * @property string $created_date
 * @property string $house_no
 * @property string $street_no
 * @property string $reference_no
 * @property string $consumer_no
 */
class Plot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
     public $file;
    public static function tableName()
    {
        return 'plot';
    } 

    /**
     * {@inheritdoc} 
     */
    public function rules()
    {
        return [
            [['file_no',  'created_by', 'allottee_name', 'sector', 'house_no', 'street_no'], 'safe'],
            [['possession_date', 'created_date','debit','balance','house_no'], 'safe'],
            [['created_by','instalment','phone','cnic'], 'integer'],
            [['services', 'street_no', 'sector', 'sub_sector', 'connection_date', 'cat_id','plot_type','no_of_installement'], 'safe'],
            [['file_no',  'allottee_name','email', 'end_date', 'consumer_no','document'], 'string', 'max' => 255],
            [['reference_no', 'consumer_no','covered_area'], 'string', 'max' => 255],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_no' => 'File No',
            'cat_id' => 'Category Name',
            'instalment'=>'instalment',
            'connection_date' => 'Connection Date',
            'created_by' => 'Created By',
            'services' => 'Services',
            'allottee_name' => 'Allottee Name',
            'sector.sectorName' => 'Sector',
            'sub_sector' => 'Sub Sector',
            'possession_date' => 'Possession Date',
            'end_date' => 'End Date',
            'created_date' => 'Created Date',
            'house_no' => 'House No',
            'street_no' => 'Street No',
            'phone' => 'Phone No',
            'email' => 'Email',
            'cnic' => 'CNIC',
            'balance' => 'Credit',
            'debit'   => 'Debit',
            'reference_no' => 'Reference No',
            'consumer_no' => 'Consumer No',
        ];
    }

             public function getcategoryName()
                {
                    return $this->hasOne(Category::className(),['id' => 'cat_id']);
                }

               public function getsectorName()
                {
                    return $this->hasOne(Sectors::className(),['id' => 'sector']);
                }

                public function getsubsectorName()
                {
                    return $this->hasOne(Sectors::className(),['id' => 'sector']);
                }
       
}
