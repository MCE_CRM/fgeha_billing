<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plot_document".
 *
 * @property int $id
 * @property int $plotId
 * @property int $flieNameId
 * @property string $document
 *
 * @property Plot $plot
 * @property EstateWing $flieName
 */
class PlotDocument extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plot_document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plotId', 'flieNameId', 'document'], 'required'],
            [['plotId', 'flieNameId'], 'integer'],
            [['document'],'file', 'maxFiles'=>1000],
            // [['document'], 'document', 'maxFiles' => 10],
            // [['document'], 'string', 'safe'],
            [['plotId'], 'exist', 'skipOnError' => true, 'targetClass' => Plot::className(), 'targetAttribute' => ['plotId' => 'id']],
            [['flieNameId'], 'exist', 'skipOnError' => true, 'targetClass' => EstateWing::className(), 'targetAttribute' => ['flieNameId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plotId' => 'Plot Name',
            'flieNameId' => 'Flie Name',
            'document' => 'Document',
        ];
    }

    /**
     * Gets query for [[Plot]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlot()
    {
        return $this->hasOne(Plot::className(), ['id' => 'plotId']);
    }

    /**
     * Gets query for [[FlieName]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFlieName()
    {
        return $this->hasOne(EstateWing::className(), ['id' => 'flieNameId']);
    }
}
