<?php

namespace app\models;
 
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Plot;

/**
 * PlotSearch represents the model behind the search form of `app\models\Plot`.
 */
class PlotSearch extends Plot 
{
    /** 
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by'], 'integer'],
            [['file_no', 'connection_date', 'allottee_name', 'sector', 'sub_sector', 'possession_date', 'end_date', 'created_date', 'house_no', 'street_no', 'reference_no', 'consumer_no'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * 
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Plot::find();
        $query->leftjoin('sectors','sectors.id=plot.sector');



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'connection_date' => $this->connection_date,
            'created_by' => $this->created_by,
            'possession_date' => $this->possession_date,
            'end_date' => $this->end_date,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'file_no', $this->file_no])
            ->andFilterWhere(['like', 'allottee_name', $this->allottee_name])
            ->andFilterWhere(['like', 'sectors.sector_name', $this->sector])
             ->andFilterWhere(['like', 'sub_sector', $this->sub_sector])
            ->andFilterWhere(['=', 'house_no', $this->house_no])
            ->andFilterWhere(['=', 'street_no', $this->street_no])
            ->andFilterWhere(['like', 'reference_no', $this->reference_no])
            ->andFilterWhere(['like', 'consumer_no', $this->consumer_no]);

        return $dataProvider;
    }
}
