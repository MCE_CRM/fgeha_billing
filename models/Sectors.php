<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sectors".
 *
 * @property int $id
 * @property string $sector_name
 * @property string $created_on
 * @property string $created_by
 * @property string|null $updated_on
 * @property string|null $updated_by
 */
class Sectors extends \yii\db\ActiveRecord
{ 
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sectors';
    }
 
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sector_name' ], 'required'],
            [['created_on', 'sector_id', 'updated_on','created_on', 'created_by'], 'safe'],
            [['sector_name', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    } 

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sector_name' => 'Sector Name',
            'sector_id'=>'sector_id',
            'created_on' => 'Created On',
           'un.username' => 'Created By',
            'updated_on' => 'Updated On',
            'name.username' => 'Updated By',
        ];
    }

     public function getname()
                {
                    return $this->hasOne(User::className(),['id' => 'updated_by']);
                }

                 public function getun()
                {
                    return $this->hasOne(User::className(),['id' => 'created_by']);
                }
}
