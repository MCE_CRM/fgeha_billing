<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "services".
 *
 * @property int $id
 * @property string $service_name
 * @property string|null $charges
 * @property int|null $cat_id
 * @property string $created_on
 * @property string $created_by 
 * @property string $updated_on
 * @property string $updated_by
 */
class Services extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_name'], 'required'],
            [[ 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['service_name', 'charges'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_name' => 'Service Name',
            'charges' => 'Charges',
           
            'created_on' => 'Created On',
            'un.username' => 'Created By',
            'updated_on' => 'Updated On',
            'name.username' => 'Updated By',
        ];
    }

     public function getname()
                {
                    return $this->hasOne(User::className(),['id' => 'updated_by']);
                }

                 public function getun()
                {
                    return $this->hasOne(User::className(),['id' => 'created_by']);
                }
}
