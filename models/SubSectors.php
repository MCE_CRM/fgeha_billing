<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_sectors".
 *
 * @property int $id
 * @property string $name
 * @property int $sector_id
 * @property string $created_by
 * @property int $created_on
 * @property string|null $updated_by
 * @property int|null $updated_on
 */
class SubSectors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sub_sectors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'sector_id'], 'required'],
            [['sector_id', 'created_on', 'updated_on'], 'integer'],
            [['name', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sector_id' => 'Sector ID',
            'un.username' => 'Created By',
            'created_on' => 'Created On',
            'name.username' => 'Updated By',
            'updated_on' => 'Updated On',
        ];
    }

    public function getname()
                {
                    return $this->hasOne(User::className(),['id' => 'updated_by']);
                }

                 public function getun()
                {
                    return $this->hasOne(User::className(),['id' => 'created_by']);
                }
}
