<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tenure".
 *
 * @property int $id
 * @property string $tenure_name
 * @property string $created_on
 * @property string $created_by
 * @property string $updated_on
 * @property string $updated_by
 */
class Tenure extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tenure';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tenure_name',], 'required'],
            [['created_on', 'created_by', 'updated_on', 'updated_by'], 'safe'],
            [['tenure_name','t_interval', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tenure_name' => 'Tenure Name',
            't_interval'=> 'Interval',
            'created_on' => 'Created On',
            'un.username' => 'Created By',
            'updated_on' => 'Updated On',
            'name.username' => 'Updated By',
        ];
    }
      public function getname()
                {
                    return $this->hasOne(User::className(),['id' => 'updated_by']);
                }

                 public function getun()
                {
                    return $this->hasOne(User::className(),['id' => 'created_by']);
                }
}
