<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\activitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

      <div class="card">
        <div class="row">
            <div class="col-lg-12">

                <div class="row" style="margin: 10px;">

                <!--    <div class="col-lg-3">-->

                <!--    <?= $form->field($model, 'id') ?>-->

                <!--</div>-->

                 <div class="col-lg-3">

                    <?= $form->field($model, 'activity') ?>

                </div>

                 <div class="col-lg-3">

                    <?= $form->field($model, 'created_by') ?>

                </div>

                <!-- <div class="col-lg-3">-->

                <!--    <?= $form->field($model, 'created_on') ?>-->

                <!--</div>-->
                  <div class="col-lg-3">

                </div>
                 <div class="col-lg-3">

                </div>

     <div class="col-lg-3">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>
</div>
</div>
</div>
</div>

    <?php ActiveForm::end(); ?>

</div>
