<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\activitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Activities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <!--<?= Html::a('Create Activity', ['create'], ['class' => 'btn btn-success']) ?>-->
    </p>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'activity',
            'un.username',
            [
                'attribute'=>'created_on',
                'format' => ['datetime', 'php:d-m-Y H:i:s']
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div> 
