<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Tenure;
use app\models\Sectors;

/* @var $this yii\web\View */
/* @var $model app\models\BillGenerate */
/* @var $form yii\widgets\ActiveForm */
?> 

<div class="bill-generate-form">

    <div class="card mb-4">
        <div class="card-header">
            Bill Genrate
        </div>
        <div class="card-body">
             <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'tenure_id')->dropDownList(
            ArrayHelper::map(tenure::find()->all(),'id', 'tenure_name'),
            ['prompt'=>'Select Tenure'])?>
           
            <?= $form->field($model, 'sector_id')->dropDownList(
    		ArrayHelper::map(sectors::find()->all(),'id', 'sector_name'),
    		['prompt'=>'Select Sector'])?>
                <div class="form-group">
                    <label for="date">Due Date</label>
                    <input type="Date" class="form-control" name ="due-date" id="date">
                </div>
             

            <?= Html::submitButton(Yii::t('app', 'Genrate Bill'), ['class' => 'btn btn-primary']) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>




