<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; 
use yii\helpers\ArrayHelper;
use app\models\Tenure;
use app\models\SubSectors;

/* @var $this yii\web\View */
/* @var $model app\models\BillGenerate */
/* @var $form yii\widgets\ActiveForm */
?> 

<div class="bill-generate-form">

    <div class="card mb-4">
        <div class="card-header">
            Bill Genrate
        </div>
        <div class="card-body">
             <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'tenure_id')->dropDownList(
            ArrayHelper::map(tenure::find()->where(['status'=>1])->all(),'id', 'tenure_name'),
            ['prompt'=>'Select Tenure'])?>
           
            <?= $form->field($model, 'sector_id')->dropDownList(
            ArrayHelper::map(SubSectors::find()->where(['status'=>1])->all(),'name', 'name'),
            ['prompt'=>'Select Sector'])?>
                <div class="form-group">
                    <label for="date">Due Date</label>
                    <input type="Date" class="form-control" name ="due-date" id="date">
                </div>
             

            <?= Html::submitButton(Yii::t('app', 'Genrate Bill'), ['id'=>'getData','class' => 'btn btn-primary']) ?>
            <div id="result" style="margin-top:10px"> </div>
            <div id="loader" style="display:none;">
            <br>
              <img src="\billing\web\img\ajax-loader.gif"/>
             </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            
 <script>
  $(document).ready(function(){

    $("#getData").on("click",function(){

     $.ajax({

        type:"GET",
        url: "/billing/web/bill-generate/create",

        beforeSend: function(){
            $("#loader").show();
            $("#result").html("In Progress...");
        },

        complete:function(){
            $("#loader").show();
        },

        success:function(){
           // $("#loader").hide();
            //$("#result").html("Please Wait!");
            
        },

        error:function(){
            $("#loader").hide();
            $("#result").html("Some Error Occured");
           
        }

     });


    });

  });
</script>



