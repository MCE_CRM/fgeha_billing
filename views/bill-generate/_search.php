<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Bill;
use app\models\BillSearch;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\BillGenerateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bill-generate-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="card">
        <div class="row">
            <div class="col-lg-12">

                <div class="row" style="margin: 10px;">


                 <div class="col-lg-3">

              <?= $form->field($model, 'tenure_id')->dropDownList( ArrayHelper::map(bill::find()->all(),'billing_months', 'billing_months'), ['prompt'=>'All Tenure'])?>

                </div>

                <!--  <div class="col-lg-2">

                    <?= $form->field($model, 'status') ?>

                </div> -->
<!-- 
                 <div class="col-lg-2">

                    <?= $form->field($model, 'print_status') ?>

                </div> -->

               
                    <?php // echo $form->field($model, 'created_by') ?>

                    <?php // echo $form->field($model, 'updated_on') ?>

                    <?php // echo $form->field($model, 'updated_by') ?>
                     <div class="col-lg-2"></div>
                       <div class="col-lg-2">
                       </div>
                        <div class="col-lg-2">
                       </div>
                            <div class="col-lg-2">
                       </div>
                      

    <div class="col-lg-2">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>
</div>
</div>
</div>
</div>

    <?php ActiveForm::end(); ?>

</div>
