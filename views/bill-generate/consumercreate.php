<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BillGenerate */

$this->title = Yii::t('app', 'Create Bill Generate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bill Generates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bill-generate-create">


    <?= $this->render('_formconsumer', [
        'model' => $model,
    ]) ?>

   

</div> 
