<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
 
/* @var $this yii\web\View */
/* @var $searchModel app\models\BillGenerateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bill Generates');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bill-generate-index">
 
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Generate Tinure Bills'), ['create'], ['class' => 'btn btn-success']) ?>  &nbsp;&nbsp;

        <?= Html::a(Yii::t('app', 'Generate Sector Bills'), ['sectorcreate'], ['class' => 'btn btn-success']) ?> &nbsp;&nbsp;

         <?= Html::a(Yii::t('app', 'Generate Sub-Sector Bills'), ['subsectorcreate'], ['class' => 'btn btn-success']) ?> &nbsp;&nbsp;

        <?= Html::a(Yii::t('app', 'Generate Consumer Bills'), ['consumercreate'], ['class' => 'btn btn-success']) ?>
    
        <div class="alert">
  <!-- <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> -->
  <?php $var ?>
</div>  
    </p>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
            [    
                'attribute'=>'tenure_id',
                'value'=>'tenureId.tenure_name',
            ],
           
            //'selectop',
            [    
                
                'attribute'=>'status',
                'value'=>function ($model, $widget) {
                 
                    if($model->status ==1){
                        return( "All bills generated");
                    } else{
                        return( "pending");
                    }

                },

            ],
            // 'status',
            // 'print_status',
                     [    
                
                'attribute'=>'print_status',
                'value'=>function ($model, $widget) {
                 
                    if($model->print_status ==1){
                        return( "All bills printed");
                    } else{
                        return( "pending");
                    }

                },

            ],
            [
                'attribute'=>'created_on',
                'format' => ['datetime', 'php:d-m-Y H:i:s']
            ],
            //'created_by',
            //'updated_on',
            //'updated_by',

             ['class' => 'yii\grid\ActionColumn',
            'header'=>'Action',
            'headerOptions' => ['width' => '80'],
            'template' => '{View}{Update}{Delete}',
            'buttons'=> [
                
                'View' => function ($url,$model,$key){
                    return html::a('<i class="fa fa-eye" style="font-size:16px;color:#00cc66 "></i>',Url::to(['/bill-generate/view', 'id' => $model->id]),);
                },
               
            ]
        ], 
        
        ],
    ]); ?>


</div>
