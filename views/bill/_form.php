<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\bill;
use yii\helpers\Url;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Bill */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bill-form">
   

     
     <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


<?php 
    if (yii::$app->session->hasFlash('success')) {?>
        <div class="alert alert-success">
   <strong ><?php echo yii::$app->session->getFlash('success');?></strong> 
</div>
       
 <?php   }
?>


    
 <?php  $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
     <div class="form-group row">
        <div class="col-sm-4">
 
    <?= $form->field($model, 'consumer_id')->textInput() ?>

    <!--<?= $form->field($model, 'total_months_conservancy')->textInput() ?>-->
</div>
   
      <div class="col-sm-4">
    <?= $form->field($model, 'water_bill')->textInput() ?>

    <!--<?= $form->field($model, 'water_remarks')->textInput(['maxlength' => true]) ?>-->
</div> <div class="col-sm-4">
    <?= $form->field($model, 'conservancy_amount')->textInput() ?>

    <!--<?= $form->field($model, 'total_water_and_conservancy_amount')->textInput() ?>-->
</div> <div class="col-sm-4">
    <?= $form->field($model, 'tinure')->textInput() ?>

    <?= $form->field($model, 'dues_amount')->textInput() ?>
</div> <div class="col-sm-4">
    <?= $form->field($model, 'per_month_charges')->textInput() ?>

    <?= $form->field($model, 'no_of_months')->textInput() ?>
</div> <div class="col-sm-4">
    <?= $form->field($model, 'total_amount_of_current_months')->textInput() ?>

    <?= $form->field($model, 'advanced_pay')->textInput() ?>
    </div>
    <div class="col-sm-4">
     <?= $form->field($model, 'payment')->textInput() ?>
 
    <?= $form->field($model, 'arrears')->textInput() ?>
</div>
   
    
 <div class="col-sm-4">
    <?= $form->field($model, 'total_bill')->textInput() ?>

    <?= $form->field($model, 'after_due_date_charges')->textInput() ?>
</div> <div class="col-sm-4">
    <?= $form->field($model, 'total_after_due_date')->textInput() ?>

    <?= $form->field($model, 'debit')->textInput() ?>
</div> <div class="col-sm-4">
    <?= $form->field($model, 'remarks')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropdownList(['0' => 'pending', '1' => 'paid']) ?>
     </div> <div class="col-sm-4">
    <?= $form->field($model, 'billing_months')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'issue_date')->textInput() ?>
</div> <div class="col-sm-4">
    <?= $form->field($model, 'due_date')->textInput() ?>

    <?= $form->field($model, 'reference_no')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-4">
    <?= $form->field($model, 'arrears_period')->textInput() ?>
    </div>
    <div class="col-sm-4">
        <br>
    <?= $form->field($model, 'file[]')->fileInput(['multiple'=> true]) ?>
    </div>
    <div class="col-sm-4">
  <br>
  <?= $form->field($model, 'other_charges')->checkboxlist( ArrayHelper::map(\app\models\Services::find()->all(), 'id', 'service_name'));
                   
  ?>
  </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


<script type="text/javascript"> jQuery(document).ready(function () {
$('#bill-water_bill').blur(function(){
        if( $(this).val() ) {
        var totalbill = parseInt($('#bill-total_bill').val());
        var watercharges = parseInt($('#bill-water_bill').val());
        var conservancycharges = parseInt($('#bill-conservancy_amount').val());
        var debit = parseInt($('#bill-debit').val());
        var after_due_date_charges = parseInt($('#bill-after_due_date_charges').val());
        var total_after_due_date = parseInt($('#bill-total_after_due_date').val());
        var arrears = parseInt($('#bill-arrears').val());

        
        var total = watercharges + conservancycharges + debit + arrears;
        var percentage=Math.round((total*10)/100);
        var aftertoal= Math.round(total+percentage);

        $('#bill-total_bill').val(total);
        $('#bill-after_due_date_charges').val(percentage); 
        $('#bill-total_after_due_date').val(aftertoal);  
         }else{        
        $('#bill-water_bill').val()=0;  
         } 
})});
</script>

<script type="text/javascript"> jQuery(document).ready(function () {
$('#bill-conservancy_amount').blur(function(){
        if( $(this).val() ) {
        var totalbill = parseInt($('#bill-total_bill').val());
        var watercharges = parseInt($('#bill-water_bill').val());
        var conservancycharges = parseInt($('#bill-conservancy_amount').val());
        var debit = parseInt($('#bill-debit').val());
        var after_due_date_charges = parseInt($('#bill-after_due_date_charges').val());
        var total_after_due_date = parseInt($('#bill-total_after_due_date').val());
        var arrears = parseInt($('#bill-arrears').val());

        
        var total = watercharges + conservancycharges + debit + arrears;
        var percentage=Math.round((total*10)/100);
        var aftertoal= Math.round(total+percentage);

        $('#bill-total_bill').val(total);
        $('#bill-after_due_date_charges').val(percentage); 
        $('#bill-total_after_due_date').val(aftertoal);  
         }else{        
        $('#bill-water_bill').val()=0;  
         } 
})});
</script>

<script type="text/javascript"> jQuery(document).ready(function () {
$('#bill-debit').blur(function(){
        if( $(this).val() ) {
        var totalbill = parseInt($('#bill-total_bill').val());
        var watercharges = parseInt($('#bill-water_bill').val());
        var conservancycharges = parseInt($('#bill-conservancy_amount').val());
        var debit = parseInt($('#bill-debit').val());
        var after_due_date_charges = parseInt($('#bill-after_due_date_charges').val());
        var total_after_due_date = parseInt($('#bill-total_after_due_date').val());
        var arrears = parseInt($('#bill-arrears').val());

        
        var total = watercharges + conservancycharges + debit + arrears;
        var percentage=Math.round((total*10)/100);
        var aftertoal= Math.round(total+percentage);

        $('#bill-total_bill').val(total);
        $('#bill-after_due_date_charges').val(percentage); 
        $('#bill-total_after_due_date').val(aftertoal);  
         }else{        
        $('#bill-water_bill').val()=0;  
         } 
})});
</script>

<script type="text/javascript"> jQuery(document).ready(function () {
$('#bill-arrears').blur(function(){
        if( $(this).val() ) {
        var totalbill = parseInt($('#bill-total_bill').val());
        var watercharges = parseInt($('#bill-water_bill').val());
        var conservancycharges = parseInt($('#bill-conservancy_amount').val());
        var debit = parseInt($('#bill-debit').val());
        var after_due_date_charges = parseInt($('#bill-after_due_date_charges').val());
        var total_after_due_date = parseInt($('#bill-total_after_due_date').val());
        var arrears = parseInt($('#bill-arrears').val());

        
        var total = watercharges + conservancycharges + debit + arrears;
        var percentage=Math.round((total*10)/100);
        var aftertoal= Math.round(total+percentage);

        $('#bill-total_bill').val(total);
        $('#bill-after_due_date_charges').val(percentage); 
        $('#bill-total_after_due_date').val(aftertoal);  
         }else{        
        $('#bill-water_bill').val()=0;  
         } 
})});
</script>


</div>


