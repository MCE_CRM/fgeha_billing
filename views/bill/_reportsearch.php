<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Bill;
use app\models\BillSearch;
use yii\helpers\ArrayHelper;
use kartik\daterange\DateRangePicker; 



/* @var $this yii\web\View */
/* @var $model app\models\BillSearch */
/* @var $form yii\widgets\ActiveForm */
?> 

<div class="bill-search"> 

    <?php $form = ActiveForm::begin([
        'action' => ['overall-report'],
        'method' => 'POST',

    ]); ?>

    <div class="card">
        <div class="row">
            <div class="col-lg-12">

                <div class="row" style="margin: 10px;">


                        <?php // echo $form->field($model, 'consumer_id') ?>

                        

                        <?php // echo $form->field($model, 'total_months_conservancy') ?>

                        <?php //echo $form->field($model, 'water_arrears') ?>

                        <?php // echo $form->field($model, 'water_remarks') ?>

                        

                        <?php // echo $form->field($model, 'cnic') ?>

                        
                           
                        <?php // echo $form->field($model, 'fileNo') ?>

                       

                        <?php // echo $form->field($model, 'total_water_and_conservancy_amount') ?>

                        <?php // echo $form->field($model, 'tinure') ?>

                        <?php // echo $form->field($model, 'dues_amount') ?>

                        <?php // echo $form->field($model, 'per_month_charges') ?>

                        <?php // echo $form->field($model, 'no_of_months') ?>

                        <?php // echo $form->field($model, 'total_amount_of_current_months') ?>

                        <?php // echo $form->field($model, 'advanced_pay') ?>

                        <?php // echo $form->field($model, 'arrears') ?>

                        <?php // echo $form->field($model, 'arrears_period') ?>

                        <?php // echo $form->field($model, 'total_bill') ?>

                        <?php // echo $form->field($model, 'after_due_date_charges') ?>

                        <?php // echo $form->field($model, 'total_after_due_date') ?>

                        <?php // echo $form->field($model, 'balance_arears') ?>

                        <?php // echo $form->field($model, 'remarks') ?>

                        <?php // echo $form->field($model, 'water_charges_per_month') ?>

                        <?php // echo $form->field($model, 'conservancy_charges_per_month') ?>

                        <?php // echo $form->field($model, 'four_months_water_charges') ?>

                        <?php // echo $form->field($model, 'four_months_conservancy_charges') ?>

                        <div class="col-lg-2">

                 
                         <?= $form->field($model, 'billing_months')->dropDownList( ArrayHelper::map(bill::find()->all(),'billing_months', 'billing_months'), ['prompt'=>'All Tenure', 'name'=>'tenuresearch'])?>

                            </div>
                

                        <?php // echo $form->field($model, 'issue_date') ?>

                        <?php // echo $form->field($model, 'due_date') ?>

               

                        <?php // echo $form->field($model, 'cnic') ?>

                        
                    

               <!--    <?= $form->field($model, 'sector')->dropDownList( ArrayHelper::map(bill::find()->all(),'subsectorName.name', 'subsectorName.name'), ['prompt'=>'All', 'name'=>'subsector'])?> -->


                   

                  <!--  <div class="col-lg-2">


                    <?php  echo $form->field($model, 'status')->dropDownList(['1'=>'paid' , '0' => 'pending' ], ['prompt'=>'All', 'name'=>'status'])?>

                    

                       </div> --> 

             <!------------------------------ Date Range Picker Starts------------------>

                <div class="col-lg-3">
                    <label>Select Date</label>
                <input type="date"  name="start_date" class="form-control">

                </div>
                <div class="col-lg-3">
                    <label>Select Date</label>
                <input type="date"  name="end_date" class="form-control">

                </div>
           

            <!------------------------------ Date Range Picker Ends------------------>
                 
       
          <div class="col-lg-2"></div>
          <div class="col-lg-2"></div>

        

     <div class="col-lg-2">
       
       <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
         </div>


     </div>
       </div>
         </div>
           </div>

    <?php ActiveForm::end(); ?>