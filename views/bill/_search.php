<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Bill;
use app\models\BillSearch; 

/* @var $this yii\web\View */
/* @var $model app\models\BillSearch */
/* @var $form yii\widgets\ActiveForm */
?> 

<div class="bill-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="card">
        <div class="row">
            <div class="col-lg-12">

                <div class="row" style="margin: 10px;">
                     
                    <div class="col-lg-3">
                           
                        <?php  echo $form->field($model, 'id') ?>

                    </div>
                    
                    <div class="col-lg-3">
                           
                        <?php  echo $form->field($model, 'file_no') ?>

                           </div>

                    <div class="col-lg-3">

                        <?php  echo $form->field($model, 'consumer_id') ?>

                        </div>
                        <div class="col-lg-3">

                     <?= $form->field($model, 'sector')->dropDownList( ArrayHelper::map(bill::find()->all(),'subsectorName.name', 'subsectorName.name'), ['prompt'=>'All'])?> </div>

                      <div class="col-lg-3">
                           
                        <?= $form->field($model, 'status')->dropdownList(['0' => 'pending', '1' => 'paid'],['prompt'=>'All']) ?>

                    </div>
                        <?php //echo $form->field($model, 'water_arrears') ?>

                        <?php // echo $form->field($model, 'water_remarks') ?>

                        <?php // echo $form->field($model, 'total_water_and_conservancy_amount') ?>

                        <?php // echo $form->field($model, 'tinure') ?>

                        <?php // echo $form->field($model, 'dues_amount') ?>

                        <?php // echo $form->field($model, 'per_month_charges') ?>

                        <?php // echo $form->field($model, 'no_of_months') ?>

                        <?php // echo $form->field($model, 'total_amount_of_current_months') ?>

                        <?php // echo $form->field($model, 'advanced_pay') ?>

                        <?php // echo $form->field($model, 'arrears') ?>

                        <?php // echo $form->field($model, 'arrears_period') ?>

                        <?php // echo $form->field($model, 'total_bill') ?>

                        <?php // echo $form->field($model, 'after_due_date_charges') ?>

                        <?php // echo $form->field($model, 'total_after_due_date') ?>

                        <?php // echo $form->field($model, 'balance_arears') ?>

                        <?php // echo $form->field($model, 'remarks') ?>

                        <?php // echo $form->field($model, 'water_charges_per_month') ?>

                        <?php // echo $form->field($model, 'conservancy_charges_per_month') ?>

                        <?php // echo $form->field($model, 'four_months_water_charges') ?>

                        <?php // echo $form->field($model, 'four_months_conservancy_charges') ?>

                        <?php // echo $form->field($model, 'billing_months') ?>

                        <?php // echo $form->field($model, 'issue_date') ?>

                        <?php // echo $form->field($model, 'due_date') ?>

                        <?php // echo $form->field($model, 'reference_no') ?>
                     
                        

     <div class="col-lg-3">
        <br>
       
       <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>
     </div>
      </div>
       </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
