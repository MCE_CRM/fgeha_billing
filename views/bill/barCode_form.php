<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\bill;
use yii\helpers\Url;
use kartik\widgets\FileInput;


/* @var $this yii\web\View */
/* @var $model app\models\Bill */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bill-form">
   

    
 <?php  $form = ActiveForm::begin(['action' => 'updatestatus','options' => ['enctype'=>'multipart/form-data']]); ?>
  <!-- 
 
 <div class="form-group">
    <label for="text">Consumer ID:</label>
    <input type="text" class="form-control" id="c_id">
  </div> -->
  <div class="form-group">
    <label for="text">Bill ID:</label>
    <input type="text" name="bill_id" class="form-control form-control-user" id="b_id">
  </div>
        <input type="hidden" name="check" value="0">
         
        <div class="form-group">
            <div class="custom-control custom-checkbox small">
            <input type="checkbox" class="custom-control-input" id="customCheck" name="check" value="1" >
            <label class="custom-control-label" for="customCheck" style="font-size: 18px">Bill after due date</label>
            </div>
        </div>
  <div class="alert alert-success" id="s-msg" style="display:<?= $dis?>;">
   <strong ><?= $data1?></strong> 
</div>
  
  


  <?= Html::submitButton('Update', ['class' => 'btn btn-success ' , 'name' => 'update-button']) ?>


     

    <?php ActiveForm::end(); ?>

</div>



