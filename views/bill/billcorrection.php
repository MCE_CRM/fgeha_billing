<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\pjax;
use app\models\Bill;
use app\models\Sectors;
use app\models\SubSectors;
use app\models\BillSearch;
use kartik\daterange\DateRangePicker; 
/* @var $this yii\web\View */
/* @var $searchModel app\models\BillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'bill-report';
$this->params['breadcrumbs'][] = $this->title;
?>

 <?php // echo $this->render('_reportsearch', ['model' => $searchModel]); ?>



<link href="<?= Yii::$app->homeUrl?>js/DataTables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<br>

        
      
       

 <div class="plot-view">
     <div class="container-fluid">
     <div class="card shadow mb-4"> 
    
              <div class="card-body">
              <div class="table-responsive">
              <table class="table table-bordered" id="table" width="100%" cellspacing="0">
                      <thead>
                          <tr>
                                <th>Sector</th>
                                <th>Total Bills</th>
                                <th>PAID BILLS</th>
                                <th>UN-PAID BILLS</th>
                                <th>PAID AMOUNT</th>
                                <th>UN-PAID AMOUNT</th>
    
                          </tr>
                        </thead>
                        <tbody>
                      
                              <tr>
                                 
                              </tr>
                        
                     
                                                  
                        </tbody>  
                     

                        <tfoot>
                             <tr>
                               
                                
                            </tr>

                        </tfoot>      
                        </table>
                       
                            </div>
                        </div>
                    </div>

                </div>
            </div>

<script src="<?= Yii::$app->homeUrl?>js/jQuery/jquery-3.3.1.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/datatables.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/Buttons/js/buttons.dataTables.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/Buttons/js/buttons.print.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/pdfmake/pdfmake.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/JSZip/jszip.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/Buttons/js/buttons.html5.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/pdfmake/vfs_fonts.js"></script>
<script type="text/javascript">
   $('#table').DataTable({
   dom: 'Bfrtip',

   buttons: [ {extend: 'print', className: 'btn-outline-primary'},
              {extend: 'excel', className: 'btn-outline-warning'},
              {extend: 'pdf', className: 'btn-outline-danger'} ]

});
</script>