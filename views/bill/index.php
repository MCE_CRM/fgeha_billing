<?php
ini_set('memory_limit', '-1');
use yii\helpers\Html;

use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\pjax;

 
/* @var $this yii\web\View */
/* @var $searchModel app\models\BillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bills';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bill-index">

     <?php 
    if (yii::$app->session->hasFlash('success')) {?>
        <div class="alert alert-success">
   <strong ><?php echo yii::$app->session->getFlash('success');?></strong> 
</div>
       
 <?php   }
?>
 
    <div class="card-header py-3 panel" style="">
        <!--<?= Html::a('+ Add Bill', ['create'], ['class' => 'btn btn-success']) ?>-->
    </div>


    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
   
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            

            'id',
            [    'header'=> 'Consumer Name',
                'attribute'=>'consumer_id',
                'value'=>'consumerId.allottee_name',
            ],

      

            // 'cnic',
           
            [
                'header'=> 'File No',
                'attribute'=>'consumer_id',
                'value'=> 'fileNo.file_no',
            ],

            [    'header'=> 'Street No',
                'attribute'=>'consumer_id',
                'value'=>'consumerId.street_no',
            ],

            [    'header'=> 'House No',
                'attribute'=>'consumer_id',
                'value'=>'consumerId.house_no',
            ],

            [    'header'=> 'Sub Sector',
                'attribute'=>'sector',
                'value'=>'subsectorName.name',
            ],

            [    'header'=> 'Tenure',
                'attribute'=>'tinure',
                'value'=>'tenureName.tenure_name',
            ],

            


            [    
                'header'=>'Arrears',
                'attribute'=>'arrear',
                'value'=>function ($model) {
                 
                return($model->arrears+$model->debit);

                },

            ],


            'total_bill',
            'payment',

             [  
                'header' => 'Status',
                'attribute' => 'status',
                'value' => function ($model, $widget) {
                    if($model->status == 1){
                return "<span class=\"badge badge-pill badge-success\"> Paid </span>";
                    }

                     else{
                 return "<span class=\"badge badge-pill badge-danger\"> Pending </span>";
                    }
                    
                    },
                    'format' => 'raw',
                ],


            
           
           
            // 'status',
            //'conservancy_amount',
            //'total_water_and_conservancy_amount',
            //'tinure',
            //'dues_amount',
            //'per_month_charges',
            //'no_of_months',
            //'total_amount_of_current_months',
            //'advanced_pay',
          
            //'barcode_id',
            //'arrears_period',
            //'total_bill',
            //'after_due_date_charges',
            //'total_after_due_date',
            //'balance_arears',
            //'remarks',
            //'water_charges_per_month',
            //'conservancy_charges_per_month',
            //'four_months_water_charges',
            //'four_months_conservancy_charges',
            //'billing_months',
            //'issue_date',
            //'due_date',
            //'reference_no',
          ['class' => 'yii\grid\ActionColumn',
            'header'=>'Action',
            'headerOptions' => ['width' => '80'],
            'template' => '{View}{Update}{Delete}',
            'buttons'=> [
                'Delete' => function ($url,$model,$key){
                    return html::a('<i class="fa fa-trash" style="font-size:16px;color:#F05550 "></i>', url::to(['/bill/delete?id='.$model->id])); 
                },
                'View' => function ($url,$model,$key){
                    return html::a('<i class="fa fa-eye" style="font-size:16px;color:#00cc66 "></i>',Url::to(['/bill/view', 'id' => $model->id]),);
                },
                'Update' => function ($url,$model,$key){
                    return html::a('<i class="fa fa-edit" style="font-size:16px;color:#4A94D2 "></i>',url::to(['/bill/update?id='.$model->id]));
                }
            ]
        ], 
        ],
    ]); ?>


           
     
</div>
