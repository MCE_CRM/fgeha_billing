<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\pjax;
use app\models\Bill;
use app\models\Sectors;
use app\models\SubSectors;
use app\models\BillSearch;
use kartik\daterange\DateRangePicker; 
/* @var $this yii\web\View */
/* @var $searchModel app\models\BillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'bill-report';
$this->params['breadcrumbs'][] = $this->title;
?>

 <?php // echo $this->render('_reportsearch', ['model' => $searchModel]); ?>



<link href="<?= Yii::$app->homeUrl?>js/DataTables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<br>

        
      
       

 <div class="plot-view">
     <div class="container-fluid">
     <div class="card shadow mb-4"> 
    
              <div class="card-body">
              <div class="table-responsive">
              <table class="table table-bordered" id="table" width="100%" cellspacing="0">
                      <thead>
                          <tr>
                                <th>Sector</th>
                                <th>Total Bills</th>
                                <th>PAID BILLS</th>
                                <th>UN-PAID BILLS</th>
                                <th>PAID AMOUNT</th>
                                <th>UN-PAID AMOUNT</th>
    
                          </tr>
                        </thead>
                        <tbody>
                         <?php
                         $sector = Sectors::find()->all();
                         foreach ($sector as $val):
                         ?>
                     

                         <?php
                        
                         $paid_amount = Bill::find()->where(['sub_sector'=>$val->id])->andWhere(['status'=>1])->sum('payment');

                         $paid_count = Bill::find()->Where(['status'=>1])->sum('payment');

                         $total_bill = Bill::find()->where(['sub_sector'=>$val->id])->count();
                         $total_bills = Bill::find()->count();
                    
                        ?>

                        <?php
                        
                            $unpaid_amount = Bill::find()->where(['sub_sector'=>$val->id])->andWhere(['status'=>0])->sum('total_bill');

                             $unpaid_count = Bill::find()->Where(['status'=>0])->sum('total_bill');
                       
                        ?>
                        <?php
                        $paid_Bills = Bill::find()->where(['sub_sector'=>$val->id])->andWhere(['status'=>1])->count();
                         $paid_Bill = Bill::find()->Where(['status'=>1])->count();
                        ?>
                        <?php
                        $unpaid_Bills = Bill::find()->where(['sub_sector'=>$val->id])->andWhere(['status'=>0])->count();
                        $unpaid_Bill = Bill::find()->Where(['status'=>0])->count();

                        
                        ?>
                              <tr>
                                  <td><?= $val->sector_name ?></td>
                                  <td><?= $total_bill?></td>
                                  <td><?= $paid_Bills?></td>
                                  <td><?= $unpaid_Bills?></td>
                                  <td><?= $paid_amount?></td>
                                  <td><?= $unpaid_amount?></td>

                              </tr>
                        
                        <?php endforeach; ?>
                                                  
                        </tbody>  
                     

                        <tfoot>
                             <tr>
                                <th>Total</th>
                                <th><?= $total_bills?></th>
                                <th><?= $paid_Bill?></th>
                                <th><?= $unpaid_Bill?></th>
                                <th><?= $paid_count?></th>
                                <th><?= $unpaid_count?></th>
                                
                            </tr>

                        </tfoot>      
                        </table>
                       
                            </div>
                        </div>
                    </div>

                </div>
            </div>

<script src="<?= Yii::$app->homeUrl?>js/jQuery/jquery-3.3.1.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/datatables.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/Buttons/js/buttons.dataTables.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/Buttons/js/buttons.print.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/pdfmake/pdfmake.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/JSZip/jszip.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/Buttons/js/buttons.html5.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>js/pdfmake/vfs_fonts.js"></script>
<script type="text/javascript">
   $('#table').DataTable({
   dom: 'Bfrtip',

   buttons: [ {extend: 'print', className: 'btn-outline-primary'},
              {extend: 'excel', className: 'btn-outline-warning'},
              {extend: 'pdf', className: 'btn-outline-danger'} ]

});
</script>