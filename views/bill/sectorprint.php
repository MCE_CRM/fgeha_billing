<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; 
use yii\helpers\ArrayHelper;
use app\models\Tenure;
use app\models\SubSectors;

/* @var $this yii\web\View */
/* @var $model app\models\BillGenerate */
/* @var $form yii\widgets\ActiveForm */
?> 

<div class="bill-generate-form">

    <div class="card mb-4">
        <div class="card-header">
            Sector Bill View
        </div>
        <div class="card-body">

            <form action="<?=Yii::$app->homeUrl?>bill-generate/sector-view" method="POST">
             
           
                    <div class="col-lg-6">

                        <div class="form-group field-sectorid">
                            <label class="control-label" for="sectorid">Tenure ID</label>
                            <select  class="form-control" name="tenure">
                            <option value="">Select Tenure</option>
                           
                              <option value="29" selected="selected">March-2021 to November-2021</option>

                            </select>

                            <div class="help-block"></div>
                        </div>
                    </div>
                        <div class="col-lg-6">

                        <div class="form-group field-sectorid">
                            <label class="control-label" for="sectorid">Sector</label>
                            <select  class="form-control" name="sector">
                            <option value="">Select sector</option>
                            <option value="G-13/1">G-13/1</option>
                            <option value="G-13/2">G-13/2</option>
                            <option value="G-13/3">G-13/3</option>
                            <option value="G-13/4">G-13/4</option>
                            <option value="G-14/4">G-14/4</option>

                            </select>

                            <div class="help-block"></div>
                        </div>

                     <div class="col-lg-4">
                        <div class="form-group field-plot-phone">
                        <input type="submit"  class="form-control" name="submit">

                        <div class="help-block"></div>
                        </div>                   
                        </div>                        
             

            
            </form>
            <div id="result" style="margin-top:10px"> </div>
            <div id="loader" style="display:none;">
            <br>
              <img src="\billing\web\img\ajax-loader.gif"/>
             </div>
            
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            
 <!-- <script>
  $(document).ready(function(){

    $("#getData").on("click",function(){

     $.ajax({

        type:"GET",
        url: "/billing/web/bill-generate/create",

        beforeSend: function(){
            $("#loader").show();
            $("#result").html("In Progress...");
        },

        complete:function(){
            $("#loader").show();
        },

        success:function(){
           // $("#loader").hide();
            //$("#result").html("Please Wait!");
            
        },

        error:function(){
            $("#loader").hide();
            $("#result").html("Some Error Occured");
           
        }

     });


    });

  });
</script>
 -->


