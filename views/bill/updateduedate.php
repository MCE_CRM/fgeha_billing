<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Tenure;
use app\models\SubSectors;
use app\models\Bill;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\BillGenerate */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Update Bill: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bills', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?> 

<div class="bill-generate-form">

    <div class="card mb-4">
        <div class="card-header">
            Update Due Date
        </div>
        <div class="card-body">
             <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'tinure')->dropDownList(
            ArrayHelper::map(Tenure::find()->all(),'id', 'tenure_name'),
            ['prompt'=>'Select Tenure'])?>
           
            <?= $form->field($model, 'sector')->dropDownList(
            ArrayHelper::map(Bill::find('sector')->all(),'sector', 'sector'),
            ['prompt'=>'Select Sector'])?>
                <div class="form-group">
                    <label for="date">Due Date</label>
                    <input type="Date" class="form-control" name ="due_date" id="date">
                </div>
             

            <?= Html::submitButton(Yii::t('app', 'Update Date'), ['class' => 'btn btn-primary']) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>




