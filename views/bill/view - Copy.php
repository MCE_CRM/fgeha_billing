<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Bill */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bills', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bill-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'consumer_id',
            'total_months_conservancy',
            'water_arrears',
            'water_remarks',
            'conservancy_amount',
            'total_water_and_conservancy_amount',
            'tinure',
            'dues_amount',
            'per_month_charges',
            'no_of_months',
            'total_amount_of_current_months',
            'advanced_pay',
            'payment',
            'payment_date',
            'arrears',
            'arrears_period',
            'total_bill',
            'water_bill',
            'after_due_date_charges',
            'total_after_due_date',
            'balance_arears',
            'remarks',
            'water_charges_per_month',
            'conservancy_charges_per_month',
            'four_months_water_charges',
            'four_months_conservancy_charges',
            'billing_months',
            'issue_date',
            'due_date',
            'reference_no',
        ],
    ]) ?>

</div>
