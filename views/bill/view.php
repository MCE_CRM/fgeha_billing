<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use app\models\Bill; 
use app\models\Sectors; 
use app\models\SubSectors; 
/* @var $this yii\web\View */
/* @var $model app\models\Bill */

$this->title = $val->id;
$this->params['breadcrumbs'][] = ['label' => 'Bills', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bill-view">    
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<!-- Bootstrap Core Css -->
<link href="../../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
<style>
    body {
        padding-top: 8px;
        padding-bottom: 20px;
        background: #fff !important
    }
    .b-line {
        border: 2px solid;
        border-radius:25px;
        border-color: green;
        
    }
    .container1{ margin: 0 auto; width: 730px; background:#fff}
    .row-top{width: 100%;}
    .col-left-reg{ float: left; width: 34%; font-size:8.5pt}
    .pad-left-right{padding-left: 0px; padding-right: 0px;}
    .col-center{float: left; width: 34%;}
    .logo{ width: 100%; height: auto;}
    .col-pic{float: left; width: 30%;}
    .pic{ float: left; width: 140px; height: 130px; margin-right: 5px; margin-top:10px;}
    .pic img{width: 80%; height: 80%}
    .detail{ width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9pt}
    .detail h2{margin: 0 0 5px 0; font-family: "Times New Roman", Georgia, Serif;}
    .span{ background-color: green; padding:2px; margin-top: 7px; text-align:center; color:#fff;}
    .span-primary{
        background-color: span-primary; padding:5px; margin: 0px; text-align:center; color:#fff;
    }

    .d-line{width: 100%; float: left; background:#f5f5f5; padding:3px; margin-top:3px;font-size: 8pt;}
    .d-line span{margin: 3px; font-weight: bold; font-size: 8pt}
    .half-left{ float: left; width: 50%; }
    .half-right{ float: left; width: 50%; }
    .membership{width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9pt;margin-top:8px;}
    .membership h2{margin: 5px 0 5px 0; font-family:"Times New Roman", Georgia, Serif; font-size:14pt}

    .bill-d{width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9pt; margin-top: 10px;}
    .bill-d h2{margin: 5px 0 5px 0; font-family:"Times New Roman", Georgia, Serif; font-size:14pt}
    .bill-d h3{margin: 5px 0 2px 0; font-family:"Times New Roman", Georgia, Serif; font-size:16pt}
    #b-line{width: 100%; float: left; padding:3px; margin-top:2px;}
    .b-d-left{float: left; width: 45%;background:#f5f5f5; padding: 2px; }
    .b-d-right{float: right; width: 43%;background:#f5f5f5; padding: 2px; }
    #b-line span{padding:3px; font-weight: bold}
    .b-d-sign{float: right; width: 43%; }
    .sign{ border-top: 1px solid #000; padding-top: 10px; text-align: center}


    .text-center{ text-align:center;}
    .d-line abel {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 0px;
        font-weight: bold;
    }

    .col-lg-4 {
        width: 33.33333333%;
        float: left;
    }
    @media print {
        .col-md-3{width: 25%;}
        .col-md-6{width: 50%;}


    }

    @media print {
        .hide-print {
            display: none;
        }


    }

    /*Uzair Code*/

    .pad-left-right{padding-left: 0px; padding-right: 0px;}
    .form{float: left; width: 100%;margin-top: 23px; margin-left: 4px;}
    .col-center{margin-top: 13px;}
    .centerpad{padding-left: 10px; padding-right: 10px;margin-top: 0px;margin-bottom:0;}

    .terms{width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:7.9pt; padding-left: 10px;
        padding-top: 4px !important; background: #fff !important;}
    .terms h2{ text-align: center; margin: 3px; }
    .terms ul{list-style-type:decimal;}
    .terms ul li{ margin-bottom: 3px;}
    .detail{ width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif;font-size:9pt}
    .detail h2{margin: 0 0 5px 0; font-family: "Times New Roman", Georgia, Serif; font-size: 20px;}
    .subspan{font-size: 9pt;}

    .half-left{ float: left; width: 50%; }
    .half-right{ float: left; width: 50%; }

    .secondpage{ }
    .bill-d h2{margin: 5px 0 5px 0; font-family:"Times New Roman", Georgia, Serif; }
    .bill-d h3{margin: 5px 0 2px 0; font-family:"Times New Roman", Georgia, Serif; font-size:16pt}

    #b-line span{padding:5px 0px; font-weight: bold}
    .b-d-sign{float: right; width: 43%; }
    .sign{ border-top: 1px solid #000; padding-top: 10px; text-align: center;background: transparent !important;}

    .text-center{ text-align:center;}



    .top10{
        margin-top: 0px !important;
    }
    /************ For Printing *****/
    @media print {

        body{background: #fff !important}

        .container1{ margin: 0 auto; width: 730px; background:#fff !important}
        .form{float: left !important; width: 100% !important;margin-top: 20px !important; margin-top: 23px !important;margin-left: 4px !important;}
        .centerpad{padding-left: 10px; padding-right: 20px;margin-top: 0px;margin-bottom:0 !important;}
        .d-line{width: 100%; float: left; background:#f5f5f5 !important; padding:3px; margin-top:3px;font-size: 8pt}
        .d-line span{padding:3px; font-weight: bold; font-size: 8pt}
        .col-pic{float: left; width: 30%;}
     .pic img{width: 80%; height: 80%;}
        .span{ background-color: green; padding:5px; margin: 0px; text-align:center; color:#fff;}


        .logo{ width: 100% !important; height: auto !important;}

        .detail{ width: 100% !important; float: left !important; font-family: Verdana, Arial, Helvetica, sans-serif !important; font-size:9pt !important}
        .detail h2{margin: 0 0 5px 0 !important; font-family: "Times New Roman", Georgia, Serif !important;}
        .subspan{font-size: 9pt !important;}
        .d-line span{padding:3px !important; font-weight: bold !important}
        .half-left{ float: left !important; width: 60% !important; }
        .half-right{ float: left !important; width: 40% !important; }
        .p-d1{float: left !important; width: 30% !important;}
        .p-d2{float: left !important; width: 30% !important;}
        .p-d3{float: left !important; width: 40% !important;}
        .p-d21{float: left !important; width: 25% !important;}
        .p-d22{float: left !important; width: 25% !important;}
        .p-d23{float: left !important; width: 25% !important;}
        .p-d24{float: left !important; width: 25% !important;}
        .bill-d h2{margin: 5px 0 5px 0 !important; font-family:"Times New Roman", Georgia, Serif !important;}
        .bill-d h3{margin: 5px 0 2px 0 !important; font-family:"Times New Roman", Georgia, Serif !important; font-size:16pt !important}
        #b-line{width: 100% !important; float: left !important; padding:1px 0px !important;}
        .b-d-left{float: left !important; width: 43% !important; background:#f5f5f5 !important; padding: 2px !important;}
        .b-d-right{float: right !important; width: 43% !important;background:#f5f5f5 !important; padding: 2px !important; }
        #b-line span{padding:5px 0px !important; font-weight: bold !important}
        .b-d-sign{float: right !important; width: 43% !important; }
        .sign{ border-top: 1px solid #000 !important; padding-top: 10px !important; text-align: center !important;background: transparent !important;}
        .d-line abel {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 0px;
            font-weight: bold;
        }
        .row-top {
            margin-bottom:10px !important;    margin-top: -30px !important;
        }
        .col-top-1 {border-right: 1px solid #cccccc !important;

        }
        .cat-form{ border-bottom: 1px solid #000 !important; display: inline !important; }

        .text-center{ text-align:center !important;}

        .cal-bottom{margin-bottom:8px !important;}

        .row-eq-height {
            display: -webkit-box !important;
            display: -webkit-flex !important;
            display: -ms-flexbox !important;
            display:         flex !important;
        }

        .calculator-button,
        .calculator-button:focus {
            background-color: rgb(233,76,111) !important;
            padding: 21px !important;
            font-size: 26px !important;
        }

        .calculator-button:hover {
            background-color: rgb(198,213,205) !important;
        }

        .calculator-button,
        .calculator-button:hover,
        .calculator-button:focus {
            border:none !important;
            outline:none !important;
        }

        #outer-calc {
            background-color: rgb(84,39,51) !important;
            padding-left: 50px !important;
            padding-right: 50px !important;
            padding-top: 50px !important;
            padding-bottom: 50px !important
        }

        .button-column{
            margin-bottom: 8px !important;
        }
        .p-btn-column{
            padding: 2px 1px !important;
        }

        .amount {
            color: #fff !important;
            background-color: orange !important;
            border-color: orange !important;
        }

        .liter {
            color: #fff !important;
            background-color: #60c441 !important;
            border-color: #60c441 !important;
        }

        .rate {
            color: #fff !important;
            background-color: #434343 !important;
            border-color: #434343 !important;
        }
        .bottom{    margin-bottom: 7px !important;}
        .navbar {
            margin-bottom: 20px !important;
        }
        .jumbotron {
            text-align: center !important;
            background-color: transparent !important;
        }
        .footer {
            padding-top: 10px !important;
            padding-bottom: 10px !important;
            margin-top: 20px !important;
            border-top: 1px solid #eee !important;
            background: #f8f8f8 !important;
            text-align: center !important;
        }
        @page {size: portrait !important;margin: inherit !important;}
        .top10{
        }
        .mainhead {
            border-bottom: 1px black solid !important;
            padding-bottom: 5px !important;
            margin-bottom: 5px !important;
        }
        .headtext{
            
    font-size: 18px;
    text-align: center;
    color: green;
    font-family: monospace;
    font-weight: 800;

        }
    }
    @media print {
         body { 
    -webkit-print-color-adjust: exact !important; 
  }
        #headtext{
            
    font-size: 18px !important;
    text-align: center !important;
    color: green !important;
    font-family: monospace !important;
    font-weight: 800 !important;

        }
        #lowertext { font-size: 14px !important;
        text-align: center !important;
        color: green !important;
        font-family: monospace !important;
        font-weight: 700 !important;
            
        }
        
        #duedate{ 
           color:red !important;
        }
        .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12 {
            float: left!important;
        }
        .col-lg-12 {
            width: 100% !important;
        }
        .col-lg-11 {
            width: 91.66666666666666% !important;
        }
        .col-lg-10 {
            width: 83.33333333333334% !important;
        }
        .col-lg-9 {
            width: 75% !important;
        }
        .col-lg-8 {
            width: 66.66666666666666% !important;
        }
        .col-lg-7 {
            width: 58.333333333333336% !important;
        }
        .col-lg-6 {
            width: 50% !important;
        }
        .col-lg-5 {
            width: 41.66666666666667% !important;
        }
        .col-lg-4 {
            width: 33.33333333333333% !important;
        }
        .col-lg-3 {
            width: 25% !important;
        }
        .col-lg-2 {
            width: 16.666666666666664% !important;
        }
        .col-lg-1 {
            width: 8.333333333333332% !important;
        }
        /*.watermark {
            color: BLACK !important;
            font-size: 96px !important;
            left: 35% !important;
            opacity: 0.21 !important;
            position: fixed !important;
            top: 50% !important;
            -ms-transform: rotate(-66deg) !important;
            -webkit-transform: rotate(-66deg) !important;
            transform: rotate(-66deg) !important;
            display: block !important;
        }*/
        .remarks{margin-top: 15px !important; margin-bottom: 15px !important; background: #fff !important;}
        .remarks .billtitle {  font-size: 19px !important;  }
        .remarks .remarksblock {  height: 100px !important;border: 1px solid black !important;padding: 5px !important; float: left !important; white-space:pre-wrap !important  }
    }
    #Panes {
        position: relative;
        z-index: 10;
    }
    .note {
        width: 100%;
        float: left;
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 6.9pt;
        padding-left: 10px;
        padding-top: 4px !important;
        background: #fff !important;
        color: red;
        font-style: italic;
        font-weight: 500;
        margin-top: -28px;
    }
    .digit{
        width: 2rem;
        margin: -2px;
    }
    .dashed-input {
        width: 2rem;
        margin-right: -3px;
        margin-left: -3px;
        margin-bottom: -3px;
        border: 2px dashed green;
    }
    .checkbx{
        width: 1.5rem;  
        margin-right: 5px;
    }
    input, button, select, textarea {
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
        width:97%;
    
}
    .div1 {
        width: 90%;
        height: 100px;
        border: 1px solid black;
    }
    .box-recipt{
        width: 100%;
        height: 330px;
        border: 2px solid black;
    }
    .div2 {
        width: 90%;
        height: 100px;
        border: 1px solid blue;
    }
    .recipt {
        color: white;
        margin-top: -10;
        width: 100%;
        background-color: green;
        font-size: 15px;
        height: 30px;
        padding-top: 7px;
        padding-left: 10px;
        font-family: none;
    }
    .recipt-button {
        width: 15%;
        margin-top: -3px;
        float: right;
        margin-right: 9px;
        color: black;
        background-color: #fff;
        font-family: inherit;
    }
    .thumbnail {
        display: block;
        padding: 4px;
        margin-bottom: 2px !important;
        line-height: 1.42857143;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        -webkit-transition: border .2s ease-in-out;
        -o-transition: border .2s ease-in-out;
        transition: border .2s ease-in-out;
    }
    .table-cat, table, th, td{
        font-weight: 100;
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 2px;
        text-align: center;
        font-size: 12px;
    }

    .thead-cat {
        background-color: #0D47A1;
        color: fff;

    }
    .table-cat{
        margin: auto;
        width: 50%;
        margin-top: -30px;
        margin-bottom: 20px;
    }
    .app-point{
        font-size: 10px;
        margin-top: 10px;
    }
    .choice-stat {
        font-size: 12;
        font-weight: 600;
        font-style: italic;
        text-align: center;
        background-color: #0072ff;
        color: fff;
    }
    .ques-top {
        font-size: 12;
        font-weight: 600;

        text-align: center;
        background-color: #0072ff;
        color: #000;
    }
    .ques{
        font-size: 11px;
        font-weight: 600;


        background-color: #90EE90;
        color: #000;
    }
    .ans{
        font-size: 10px;

    }
    .buttons{
        color:#fff;
        width:20%;
        margin: 5px;
    }
</style>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
<center>
<a class="btn btn-default hide-print" id="download"  onClick="printToPDF();">Download</a>&nbsp;
<a class="btn btn-danger hide-print" id="print" onClick="window.print()" >Print</a>&nbsp;
</center>

     
<div class="container1" id="printable">
   <div class="centerpad">
    <div class="card">
        <div class="row">
            <div class="col-lg-12">
              <div class="row">
                <div class="col-lg-3">
                    <div class="">
                        <div class=" pic">
                           <?= Html::img(Url::to('@web/img/Federal-Government-Employees-Housing-Foundation.png'), ['alt' => 'My logo']) ?>
                        </div>
                    </div>
                </div>

                    <div class="col-lg-9">
                        <h2 id="headtext"style= "font-size: 18px; text-align: center;  color: green;  font-family: monospace; font-weight: 800;">
                            FEDERAL GOVERNMENT EMPLOYEES HOUSING AUTHORITY <br>
                            10-MAUVE AREA, G-10/4, ISLAMABAD<br>

                            <strong style= "color: Black; font-family: times-new-romen; font-size: 14px; ">****<br>CONSERVANCY & WATER CHARGES BILL</strong>
                        </h2>
                    </div>
              </div>
                    <h4 style= "font-size: 16px; text-align: center; margin: 0;">Customer Copy</h4>
                    <hr style="    margin-top: 5px; margin-bottom: 0px;">
                    <br>
                    
                        <div class="row">
                            <div class="col-lg-6">
                                <br>
                                <table style="width: 100%;">
                                         <?php
                                        $tenure = \app\models\Category::find()->select('category_name')->from('category')->where('id='.$val->consumerId->cat_id)->one();
                                        if ($tenure->category_name=='Category-5 25x40') {
                                            $tenure->category_name='CAT-5';
                                        }
                                        elseif ($tenure->category_name=='Category-4 30x60') {
                                            $tenure->category_name='CAT-4';
                                        }
                                        elseif ($tenure->category_name=='Category-3 35x70') {
                                            $tenure->category_name='CAT-3';
                                        }
                                        elseif ($tenure->category_name=='Category-2 40x80') {
                                            $tenure->category_name='CAT-2';
                                        }
                                        elseif ($tenure->category_name=='Category-1 50x90') {
                                            $tenure->category_name='CAT-1';
                                        }
                                         $sector = \app\models\Sectors::find()->select('id')->from('sectors')->where('id='.$val->consumerId->sector)->one();
                                        $sub_sector = \app\models\SubSectors::find()->select('name')->from('sub_sectors')->where('sector_id='.$sector->id)->one();
                                        ?>
                                       <tr>
                                        <td ><strong>Consumer No.</strong> <td><strong></strong> <?=$tenure->category_name.'/'.$sub_sector->name.'-'.str_pad($val->consumerId->id, 4, '0', STR_PAD_LEFT);?></td></td>
                                      </tr>                              
                                      <tr>
                                        <td ><strong>F No.</strong> <td><strong></strong> <?=$val->consumerId->file_no?></td></td>
                                      </tr>
                                    </table>
                                  
                                   
                            
                                    <p><strong>Owner Name</strong> <?=$val->consumerId->allottee_name?></p> 
                                    
                                    <p style="margin: 0;"><strong>House # </strong> <?=$val->consumerId->house_no?></p>
                                    <p style="margin: 0;"><strong>Street # </strong> <?=$val->consumerId->street_no?> </p>
                                    <p style="margin: 0;">
                                     <strong>Sector # </strong> <?=$val->consumerId->sub_sector?></p> 
                            </div>
                            <div class="col-lg-3">
                                
                                   
                            </div>  
                            <div class="col-lg-6">
                                <p style="text-align: center; margin: 0;"><strong>Billing Months: </strong> <?=$val->billing_months?></p>
                                <table style="text-align: center; width: 100%;">
                                  <tr>
                                    <td>Issue Date</td>
                                    <td >Due Date</td>
                                  </tr>
                                  <tr>
                                    <td><?=date('d-m-Y', strtotime($val->issue_date));?></td>
                                    <td><?=date('d-m-Y', strtotime($val->due_date));?></td>
                                  </tr>
                                </table>

                                <!--<p style="text-align: center; font-size: 11px; background: #c5c5c5; background-color: red ">Due date is only for the current dues. Non-payment of arrears is liable to be disconnection of water supply at any time without notice.</p>-->
                            </div>           
                        </div>
            </div>
        </div><!-- close row-top-->
                        <hr style="    margin-top: 5px; margin-bottom: 0px;">
                                <table style="text-align: center; width: 100%;">
                                  <tr style="background: #c5c5c5">
                                    <th colspan="3">Current Bill</th>
                                    <th colspan="5">Payment Detail</th>
                                  </tr>
                                   <tr>
                                    <td><strong>Water (<?=$val->water_bill / $val->no_of_months ?>)</strong></td>
                                    <td><strong>Conservancy (<?=round($val->conservancy_amount / $val->no_of_months) ?>)</strong></td>
                                    <td colspan="3"><strong>Period</strong></td>
                                    <td colspan="2"><strong>Amount</strong></td>
                                  </tr>
                                  <tr>
                                    <td rowspan="3"><?=$val->water_bill?></td>
           
                                    <td rowspan="3"><?=round($val->conservancy_amount)?></td>
                                    <!--<td colspan="3">Payable till <?=$val->due_date?> </td>-->
                                    <!--<td></td>-->
                                  </tr>
                                  <tr>
                                    <!--<td colspan="3">Paid Amount</td>-->
                                    <!--<td>0</td>-->
                                  </tr>
                                  <tr>
                                    <td colspan="3">ARREARS</td>
                                    <td><?=$val->arrears + $val->debit?></td>
                                  </tr>
                                  <tr>
                                      <td rowspan="3" colspan="2"><p style="text-align: center;">Payment is Accepted Only in Askari Bank Branch<br>
                                        G-13/1 & Sub Br. FGEHA G-10/4 <br>
                                        Account# <strong>0081480800125</strong><br>
                                        For complaints Please <strong>Contact 051-9334302</strong><br>
                                        <strong>Print your Bills: </strong>http://fgeha.gov.pk/billing/web/site/print-bill</p>
                                      </td>
                                      <td colspan="2" style="background: #c5c5c5;">Credit</td>
                                      <td colspan="3" style="background: #c5c5c5;">Amount Payable</td>
                                  </tr>
                                  
                                  <tr>
                                       <td>Advanced</td>
                                      <td>Current Bill</td>
                                      <td>With Due Date</td>
                                      <!--<td>Total Bill</td>-->
                                      <td id="duedate" style="color:red">After Due Date</td>
                                  </tr>
                                  <tr>
                                     <td><?=$val->advanced_pay?></td>
                                      <td><?=$val->water_bill+$val->conservancy_amount?></td>
                                      <!--<td><?=$val->water_bill+$val->conservancy_amount?></td>-->
                                      <td><?=$val->water_bill+$val->conservancy_amount - $val->advanced_pay + $val->arrears + $val->debit?></td>
                                      <td><?=$val->total_after_due_date?></td>
                                      
                                  </tr>
                                </table>
                                <br>
                                <table width="100%;">
                                    <tr style="background: #c5c5c5;">
                                        <td colspan="5">Payment History</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Month</strong></td>
                                        <td><strong>Current Bill</strong></td>
                                        <td><strong>Amount Due</strong></td>
                                        <td><strong>Payment</strong></td>
                                    </tr>
                                       <?php
                                        $label_arry =  array();
                                         $k =0;
                                        $history = \app\models\Bill::find()->select('billing_months,total_bill,payment')->from('bill')->where(['consumer_id'=>$val->consumerId]) ->limit(3)->orderBy(['id' =>SORT_DESC])->all(); foreach ($history as $valu):?>
                                        <tr>
                                            <!--<td><?=$valu->billing_months?></td>-->
                                            <!--<td><?=$valu->total_bill?></td>-->
                                            <!--<td><?=$valu->total_bill?></td>-->
                                            <!--<td><?=$valu->payment?></td>-->
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            
                                        </tr>
                                         <?php endforeach; ?>    
                                </table>

        <div class="blur-content">
            <div id="">     
                <!-- personal Detail End-->
                <div class="detail">
                    <p style="text-align: center; padding-top:;font-size:10px;">Note: For queries regarding particulars, please contact Revenue Wing at inquery office St# 137, Bazar No. 08, G13-4
                        <br>
                        <br>
                        --------------------------------------------------------------------------------------------------
                    </p>
                    </div>
                    <h4 style= "font-size: 18px; text-align: center;">Office Copy</h4>
                    <h2 id="lowertext"style= "font-size: 14px; text-align: center; color: green;  font-family: monospace; font-weight: 700;">
                        FEDERAL GOVERNMENT EMPLOYEES HOUSING AUTHORITY <br>
                        <strong style= "color: Black;">CONSERVANCY & WATER CHARGES BILL</strong>
                    </h2>
                    </div>
                    <div class="row">
                            <div class="col-lg-6">                           
                                    <table style="width: 100%;">
                                           <tr>
                                        <td><strong>Consumer No.</strong> <td> <?=$tenure->category_name.'/'.$sub_sector->name.'-'.str_pad($val->consumerId->id, 4, '0', STR_PAD_LEFT);?></td></td>
                                      </tr>                              
                                      <tr>
                                        <td ><strong>F No.</strong> <td> <?=$val->consumerId->file_no?></td></td>
                                      </tr>
                                    </table>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row" >
                                                <div class="col-lg-4">
                                                    <p style="margin: 0; text-align: center;"><strong>H# <?=$val->consumerId->house_no?></strong></p>
                                                </div>
                                                <div class="col-lg-4">
                                                    <p style="margin: 0; text-align: center;"><strong>St# <?=$val->consumerId->street_no?></strong></p> 
                                                </div>
                                                <div class="col-lg-4">
                                                    <p style="margin: 0; text-align: center;"><strong><?=$val->consumerId->sub_sector?></strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                 
                            </div>    
                           <div class="col-lg-2">     
                           </div>
                            <div class="col-lg-6">
                                <p style="text-align: center;"><strong>Billing Months: </strong><?=$val->billing_months?></p>
                                <table style="text-align: center; width: 100%;">
                                  <tr>
                                    <td> <strong>Issue Date</strong></td>
                                    <td><strong>Due Date</strong></td>
                                  </tr>
                                  <tr>
                                    <td><?=date('d-m-Y', strtotime($val->issue_date));?></td>
                                    <td><?=date('d-m-Y', strtotime($val->due_date));?></td>
                                  </tr>
                                </table>
                            </div>      
                        </div>
                      <table class="table">
                        <thead>
                            <tr style="background: #c5c5c5;">
                                <td colspan="2"><strong>Credit</strong></td>
                                <td colspan="2"><strong>Amount Payable</strong></td>
                            </tr>
                       </thead>
                        <tbody>
                            <tr>
                                <th>Advanced</th>
                                <th>Current Bill</th>
                                <th>With Due Date</th>
                                <th id="duedate" style="color:red">After Due Date</th>
                            </tr>
                            <tr>
                                <td><?=$val->advanced_pay?></td>
                                 <td><?=$val->water_bill+$val->conservancy_amount?></td>
                                 <td><?=$val->water_bill+$val->conservancy_amount - $val->advanced_pay + $val->arrears + $val->debit?></td>
                               <td><?=$val->total_after_due_date?></td>
                            </tr>
                       </tbody>
                      </table>   
                     <td><?='<img src="data:image/png;base64,'.$val->barcode_id.'" />'?></td>
            <!-- VERIFICATION BY PARENT DEPARTMENT End-->
            <div class="uzair" style="page-break-before: always; float: left">&nbsp;
            </div>
            </div>
        </div>
   </div>
   </div><!--close centerpad-->
</div><!-- close container1-->
<!-- SVG Blur Filter -->
<!-- 'stdDeviation' is the blur amount applied -->
<svg id="svg-filter">
    <filter id="svg-blur">
        <feGaussianBlur in="SourceGraphic" stdDeviation="4"></feGaussianBlur>
    </filter>
</svg>
<!--<div style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif !important;color: BLACK !important; font-size: 96px !important; left: 25% !important; opacity: 0.21 !important; position: fixed !important; top: 45% !important; -ms-transform: rotate(-40deg) !important; -webkit-transform: rotate(-40deg) !important; transform: rotate(-40deg) !important;display: block !important;">Kistpay</div>-->
<script >
    $(document).ready(function () {
        $('#agree_2').hide();
    })
    $('#agree').click(function () {
        $('#agreed_1').hide();
        $('#agree_2').show();
    }); 
    function printToPDF() {
        console.log('converting...');
        var printableArea = document.getElementById('printable');
        html2canvas(printableArea, {
            useCORS: true,
            onrendered: function(canvas) {
                var pdf = new jsPDF('p', 'pt', 'letter');
                var pageHeight = 980;
                var pageWidth = 900;
                for (var i = 0; i <= printableArea.clientHeight / pageHeight; i++) {
                    var srcImg = canvas;
                    var sX = 0;
                    var sY = pageHeight * i; // start 1 pageHeight down for every new page
                    var sWidth = pageWidth;
                    var sHeight = pageHeight;
                    var dX = 0;
                    var dY = 0;
                    var dWidth = pageWidth;
                    var dHeight = pageHeight;
                    window.onePageCanvas = document.createElement("canvas");
                    onePageCanvas.setAttribute('width', pageWidth);
                    onePageCanvas.setAttribute('height', pageHeight);
                    var ctx = onePageCanvas.getContext('2d');
                    ctx.drawImage(srcImg, sX, sY, sWidth, sHeight, dX, dY, dWidth, dHeight);
                    var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);
                    var width = onePageCanvas.width;
                    var height = onePageCanvas.clientHeight;
                    if (i > 0) // if we're on anything other than the first page, add another page
                        pdf.addPage(612, 791); // 8.5" x 11" in pts (inches*72)
                    pdf.setPage(i + 1); // now we declare that we're working on that page
                    pdf.addImage(canvasDataURL, 'PNG', 20, 40, (width * .62), (height * .62)); // add content to the page
                }
                pdf.save('test.pdf');
            }
        });
    }
</script>
</div>