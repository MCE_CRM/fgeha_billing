<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    
    <?php 
$form = ActiveForm::begin([
    'action' => ['create'],
    'options' => [
        'class' => 'comment-form'
    ]
]); 
?>
   <div class="card">
        <div class="row">
            <div class="col-lg-12">

                <div class="row" style="margin: 10px;">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'category_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-12">
                        <?= $form->field($model, 'siz_width')->textInput(['maxlength' => true]) ?>
                    </div>

                
                    
                    <div class="col-lg-12">
                        <?= $form->field($model, 'siz_height')->textInput() ?>
                    </div>
                    <div class="col-lg-12">
                        <?= $form->field($model, 'conservancy')->textInput(['maxlength' => true]) ?>
                    </div>

                     <div class="col-lg-12">
                     <?= $form->field($model, 'cat_status')->dropDownList(['active'=>'Active','inactive'=>'Inactive'], ['prompt'=>'Select Status'])?>
                 </div>
                </div>

               



            </div>
        </div>
    </div>

    <div class="form-group" style="margin: 10px"; >
        <?= Html::submitButton('Save', ['class' => 'btn btn-success pull-right']) ?>
    </div>



    <?php ActiveForm::end(); ?>

</div>
