<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

     <div class="card">
        <div class="row">
            <div class="col-lg-12">

                <div class="row" style="margin: 10px;">

                    <?php //echo $form->field($model, 'id') ?>

                     <div class="col-lg-3">

                    <?php echo $form->field($model, 'category_name') ?>

                       </div>

                    <?php //echo $form->field($model, 'water_bill') ?>

                    <div class="col-lg-3">

                    <?php echo $form->field($model, 'conservancy') ?>

                      </div>

                     <div class="col-lg-3">

                    <?php echo $form->field($model, 'siz_width') ?>

                       </div>
  
                     <div class="col-lg-3">

                    <?php  echo $form->field($model, 'siz_height') ?>

                        </div>


                    <?php // echo $form->field($model, 'created_by') ?>

                    <?php // echo $form->field($model, 'created_on') ?>

                    <?php // echo $form->field($model, 'updated_by') ?>

                    <?php // echo $form->field($model, 'updated_on') ?>

   <div class="col-lg-3">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>
      </div>
        </div>
          </div>
            </div>

    <?php ActiveForm::end(); ?>

</div>
