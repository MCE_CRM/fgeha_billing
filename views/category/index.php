<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
 use yii\bootstrap\Modal;
 use app\models\Category;

 
// @var $this yii\web\View 
// @var $searchModel app\models\CategorySearch 
//@var $dataProvider yii\data\ActiveDataProvider 

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">


    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

   <?php

            Modal::begin([

                'toggleButton' => [

                    'label' => '<i class="glyphicon glyphicon-plus"></i> + Add Category',

                    'class' => 'btn btn-success',
                      'id' =>'modalButton',

                ],

                'closeButton' => [

                  'label' => 'Close',

                  'class' => 'btn btn-danger btn-sm pull-right',


                ],

                'size' => 'modal-md',


            ]);

            $myModel = new Category();

            echo $this->render('create', ['model' => $myModel]);

            Modal::end();

        ?>




     
    </div>
    <div class="card" style="padding:20px; border-radius: 0 0 4px 4px; ">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'summary' => "",
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'category_name',
            'siz_width',
            'siz_height',
            'conservancy',
            //'created_by',
            //'created_on',
            //'updated_by',
            //'updated_on',
             [  
                  'header' => 'Status',
                'attribute' => 'cat_status',
                'value' => function ($model, $widget) {
                    if($model->cat_status == 'active'){
                return "<span class=\"badge badge-pill badge-success\"> Active </span>";
                    }

                     else{
                 return "<span class=\"badge badge-pill badge-danger\"> Inactive </span>";
                    }
                    
                    },
                    'format' => 'raw',
                ],

            ['class' => 'yii\grid\ActionColumn',
            'header'=>'Action',
            'headerOptions' => ['width' => '80'],
            
            'template' => '{View}{Update}{Delete}',

            'buttons'=> [
                'Delete' => function ($url,$model,$key){
                    return html::a('<i class="fa fa-trash" style="font-size:16px;color:#F05550 "></i>' ,url::to(['/category/delete?id='.$model->id]));
                },
                'View' => function ($url,$model,$key){
                    return html::a('<i class="fa fa-eye" style="font-size:16px;color:#00cc66"></i>' ,url::to(['/category/view?id='.$model->id]));
                },
                'Update' => function ($url,$model,$key){
                    return html::a('<i class="fa fa-edit" style="font-size:16px;color:#4A94D2"></i>',url::to(['/category/update?id='.$model->id]));
                }
            ]
        ],
        ],
    ]); ?>

    </div>
