<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\consumerservices */

$this->title = 'Create Consumerservices';
$this->params['breadcrumbs'][] = ['label' => 'Consumerservices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consumerservices-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
