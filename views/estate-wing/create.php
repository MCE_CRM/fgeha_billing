<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EstateWing */

$this->title = 'Create Estate Wing';
$this->params['breadcrumbs'][] = ['label' => 'Estate Wings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estate-wing-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
