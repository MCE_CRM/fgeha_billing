<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EstateWing */

$this->title = 'Update Estate Wing: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Estate Wings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estate-wing-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
