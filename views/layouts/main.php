
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= $this->title?></title>

  <!-- Custom fonts for this template-->
  <link href="<?= Yii::$app->homeUrl?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= Yii::$app->homeUrl?>css/sb-admin-2.min.css" rel="stylesheet">
    <link href="<?= Yii::$app->homeUrl?>css/site.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=Yii::$app->homeUrl?>site/index">

        
       <!--  <li class="nav-item">
                                <a class="nav-link" href="javascript:void();">
                                    <div class="media align-items-center">
                                        <img src="<?= Yii::$app->homeUrl ?>img/Federal-Government-Employees-Housing-Foundation.png" class="logo-icon" alt="logo icon">
                                        <div class="media-body">
                                            <h5 class="logo-text">FGEHA</h5>
                                        </div>
                                    </div>
                                </a>
                            </li> -->
                            <img src="<?= Yii::$app->homeUrl ?>img/Federal-Government-Employees-Housing-Foundation.png" style="height: 35px; width: 35px" class="logo-icon" alt="logo icon" >
       
        <div  class="sidebar-brand-text mx-3" >
         FGEHA WBMS</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?=Yii::$app->homeUrl?>site/index">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">


        <li class="nav-item">
            <a class="nav-link" href="<?=Yii::$app->homeUrl?>plot/index">
                <i class="fas fa-fw fa-folder"></i>
                <span>Consumer</span></a>
        </li>

        <hr class="sidebar-divider">

         <li class="nav-item dropdown ">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                <i class="fas fa-fw fa-table"></i>
                <span>Bills</span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right " aria-labelledby="userDropdown">

                 <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>bill/index">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  View Bill
                </a>

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>bill-generate/index">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  Bill Generate
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>bill/sector-print">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sector Print
                </a>

                 <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>bill/updateduedate">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  Update Date
                </a>
              
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>payable/index">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  Pending Bills
                </a>

                 <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>bill/barcode">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  BarCode Form
                </a>

                
              </div>

            </li>


         <li class="nav-item dropdown ">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                <i class="fas fa-fw fa-table"></i>
                <span>Reports</span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right " aria-labelledby="userDropdown">

                 <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>bill/bill-report">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sub-Sector Report
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>bill/correction-report">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                 Bill Correction Report
                </a>
              

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>bill/overall-report">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                 Overall Report
                </a>
               
              </div>
             

            </li>

<!-- 
        <li class="nav-item"> 
            <a class="nav-link" href="<?=Yii::$app->homeUrl?>payment/index">
                <i class="fas fa-fw fa-google-pay"></i>
                <span>Payment</span></a>
        </li>
 -->

       

          <li class="nav-item dropdown ">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                <i class="fa fa-cog"></i>
                <span>Settings</span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right " aria-labelledby="userDropdown">

                 <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>category/index">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  Category
                </a>

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>sector/index">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sector
                </a>
              
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>subsector/index">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sub-Sector
                </a>

                 <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>tenure/index">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  Tenure
                </a>

                 <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>services/index">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  Services
                </a>

                 
              </div>

              
            </li>

       <!--  <li class="nav-item active">
        <a class="nav-link" href="<?=Yii::$app->homeUrl?>site/report">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Report</span></a>
      </li>  -->  


          <li class="nav-item dropdown ">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                <i class="fas fa-fw fa-user"></i>
                <span>User</span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right " aria-labelledby="userDropdown">

                 <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>rbac/">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  Roles
                </a>
                 <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>auth-item/index">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  Auth-Item
                </a>

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>plot/user">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                  View user
                </a> 
                
                 <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>activity/index">
                  <i class=" fa-sm fa-fw mr-2 text-gray-400"></i>
                 User Logs
                </a>

               
              
              </div>

              
            </li>
      

      <!-- Heading -->


      <!-- Nav Item - Pages Collapse Menu -->
      <!--<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Management</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Management </h6>
            <a class="collapse-item" href="<?/*= Yii::$app->homeUrl*/?>plot/index">Plot</a>
            <a class="collapse-item" href="<?/*= Yii::$app->homeUrl*/?>bill/index">Bill</a>
            <a class="collapse-item" href="<?/*= Yii::$app->homeUrl*/?>payable/index">Payable</a>
            <a class="collapse-item" href="<?/*= Yii::$app->homeUrl*/?>payment/index">Payment</a>
          </div>
        </div>
      </li>-->

      <!-- Nav Item - Utilities Collapse Menu -->
      <!--<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Utilities</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Utilities:</h6>
            <a class="collapse-item" href="utilities-color.html">Colors</a>
            <a class="collapse-item" href="utilities-border.html">Borders</a>
            <a class="collapse-item" href="utilities-animation.html">Animations</a>
            <a class="collapse-item" href="utilities-other.html">Other</a>
          </div>
        </div>
      </li>-->

      <!-- Divider -->
      <!--<hr class="sidebar-divider">

      <!-- Heading -->


      <!-- Nav Item - Pages Collapse Menu -->
      <!--<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Pages</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
            <a class="collapse-item" href="login.html">Login</a>
            <a class="collapse-item" href="register.html">Register</a>
            <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Other Pages:</h6>
            <a class="collapse-item" href="404.html">404 Page</a>
            <a class="collapse-item" href="blank.html">Blank Page</a>
          </div>
        </div>
      </li>-->

      <!-- Nav Item - Charts -->
     <!-- <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Charts</span></a>
      </li>-->

      <!-- Nav Item - Tables -->
     <!-- <li class="nav-item">
        <a class="nav-link" href="tables.html">
          <i class="fas fa-fw fa-table"></i>
          <span>Tables</span></a>
      </li>
-->
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <!--<form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">-->
          <!--  <div class="input-group">-->
          <!--    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">-->
          <!--    <div class="input-group-append">-->
          <!--      <button class="btn btn-primary" type="button">-->
          <!--        <i class="fas fa-search fa-sm"></i>-->
          <!--      </button>-->
          <!--    </div>-->
          <!--  </div>-->
          <!--</form>-->

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

           

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">

                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>site/profile">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
              
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=Yii::$app->homeUrl?>site/logout">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
         <?= $content?>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>&copy; Maaliksoft 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= Yii::$app->homeUrl?>vendor/jquery/jquery.min.js"></script>
  <script src="<?= Yii::$app->homeUrl?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= Yii::$app->homeUrl?>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= Yii::$app->homeUrl?>js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?= Yii::$app->homeUrl?>vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= Yii::$app->homeUrl?>js/demo/chart-area-demo.js"></script>
  <script src="<?= Yii::$app->homeUrl?>js/demo/chart-pie-demo.js"></script>

</body>

</html>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
?>