<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Bill;
use app\models\Plot;
use app\models\BillSearch;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PayableSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payable-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="card">
        <div class="row">
            <div class="col-lg-12">

                <div class="row" style="margin: 10px;">

                        <div class="col-lg-3">

                        <?php  echo $form->field($model, 'id') ?>

                    </div>

                        <div class="col-lg-3">

                        <?= $form->field($model, 'consumer_id') ?>

                          </div>

                        <?php //echo $form->field($model, 'total_months_conservancy') ?>

                        <?php //echo $form->field($model, 'water_arrears') ?>

                        <?php //echo $form->field($model, 'water_remarks') ?>

                        <?php // echo $form->field($model, 'conservancy_amount') ?>

                        <?php // echo $form->field($model, 'total_water_and_conservancy_amount') ?>

                        <?php // echo $form->field($model, 'tinure') ?>

                        <?php // echo $form->field($model, 'dues_amount') ?>

                        <?php // echo $form->field($model, 'per_month_charges') ?>

                        <?php // echo $form->field($model, 'no_of_months') ?>

                        <?php // echo $form->field($model, 'total_amount_of_current_months') ?>

                        <?php // echo $form->field($model, 'advanced_pay') ?>

                        <?php // echo $form->field($model, 'arrears') ?>

                        <?php // echo $form->field($model, 'arrears_period') ?>

                        <?php // echo $form->field($model, 'total_bill') ?>

                        <?php // echo $form->field($model, 'after_due_date_charges') ?>

                        <?php // echo $form->field($model, 'total_after_due_date') ?>

                        <?php // echo $form->field($model, 'balance_arears') ?>

                        <?php // echo $form->field($model, 'remarks') ?>

                        <?php // echo $form->field($model, 'water_charges_per_month') ?>

                        <?php // echo $form->field($model, 'conservancy_charges_per_month') ?>

                        <?php // echo $form->field($model, 'four_months_water_charges') ?>

                        <?php // echo $form->field($model, 'four_months_conservancy_charges') ?>

                        <div class="col-lg-3">
       
         <?= $form->field($model, 'billing_months')->dropDownList( ArrayHelper::map(bill::find()->all(),'billing_months', 'billing_months'), ['prompt'=>'All Tenure'])?>

                           </div>

                           <div class="col-lg-3">

            <?= $form->field($model, 'remarks')->dropDownList( ArrayHelper::map(bill::find()->all(),'sector', 'sector'), ['prompt'=>'All sectors'])?>

                       </div>

                        <?php // echo $form->field($model, 'issue_date') ?>

                   <!------------------------------ Date Range Picker Starts------------------>

<!--
                  <div class="col-lg-3">
                    <label>Due Date</label>
                <input type="text" id="picker" name="BillSearch[issue_date]" class="form-control">


             <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>

            <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

            <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

            <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>

            <script type="text/javascript">
    
                $('#picker').daterangepicker({

                    startDate:moment().subtract(29,'days'),
                    endDate:moment(),
                    Current: false, 
                    'opens':'right',
                    'ranges': {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],

                },
                });
            </script>
     
            </div> --> 

            <!------------------------------ Date Range Picker Ends------------------>

                          </div>
                          <div class="col-lg-3"></div>

   <div class="col-lg-3">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>
     </div>
      </div>
       </div>
        </div>


    <?php ActiveForm::end(); ?>


