<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Payable */

$this->title = 'Create Payable';
$this->params['breadcrumbs'][] = ['label' => 'Payables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payable-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
