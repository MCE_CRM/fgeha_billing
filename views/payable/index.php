<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PayableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payable-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Payable', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel, 
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            //  
            
            'id',
            [    'header'=> 'Consumer Name',
                'attribute'=>'consumer_id',
                'value'=>'consumerId.allottee_name',
            ],
            // 'total_months_conservancy',
            // 'water_arrears',
            // 'water_remarks',
            // 'conservancy_amount',
            //'total_water_and_conservancy_amount',
            //'tinure',
            //'dues_amount',
            //'per_month_charges',
            //'no_of_months',
            //'total_amount_of_current_months',
            //'advanced_pay',
            'arrears',
            //'arrears_period',
            'total_bill',
            //'after_due_date_charges',
            'total_after_due_date',
            //'balance_arears',
            //'remarks',
            //'water_charges_per_month',
            //'conservancy_charges_per_month',
            //'four_months_water_charges',
            //'four_months_conservancy_charges',
            'billing_months',
            //'issue_date',
            [
                'attribute'=>'due_date',
                 'format' => ['date', 'php:d-m-Y']
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
