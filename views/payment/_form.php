<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'consumer_id')->textInput() ?>

    <?= $form->field($model, 'total_months_conservancy')->textInput() ?>

    <?= $form->field($model, 'water_arrears')->textInput() ?>

    <?= $form->field($model, 'water_remarks')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'conservancy_amount')->textInput() ?>

    <?= $form->field($model, 'total_water_and_conservancy_amount')->textInput() ?>

    <?= $form->field($model, 'tinure')->textInput() ?>

    <?= $form->field($model, 'dues_amount')->textInput() ?>

    <?= $form->field($model, 'per_month_charges')->textInput() ?>

    <?= $form->field($model, 'no_of_months')->textInput() ?>

    <?= $form->field($model, 'total_amount_of_current_months')->textInput() ?>

    <?= $form->field($model, 'advanced_pay')->textInput() ?>

    <?= $form->field($model, 'arrears')->textInput() ?>

    <?= $form->field($model, 'arrears_period')->textInput() ?>

    <?= $form->field($model, 'total_bill')->textInput() ?>

    <?= $form->field($model, 'after_due_date_charges')->textInput() ?>

    <?= $form->field($model, 'total_after_due_date')->textInput() ?>

    <?= $form->field($model, 'balance_arears')->textInput() ?>

    <?= $form->field($model, 'remarks')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'water_charges_per_month')->textInput() ?>

    <?= $form->field($model, 'conservancy_charges_per_month')->textInput() ?>

    <?= $form->field($model, 'four_months_water_charges')->textInput() ?>

    <?= $form->field($model, 'four_months_conservancy_charges')->textInput() ?>

    <?= $form->field($model, 'billing_months')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'issue_date')->textInput() ?>

    <?= $form->field($model, 'due_date')->textInput() ?>

    <?= $form->field($model, 'amount_paid')->textInput() ?>

    <?= $form->field($model, 'payment_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
