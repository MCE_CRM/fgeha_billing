<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'consumer_id') ?>

    <?= $form->field($model, 'total_months_conservancy') ?>

    <?= $form->field($model, 'water_arrears') ?>

    <?= $form->field($model, 'water_remarks') ?>

    <?php // echo $form->field($model, 'conservancy_amount') ?>

    <?php // echo $form->field($model, 'total_water_and_conservancy_amount') ?>

    <?php // echo $form->field($model, 'tinure') ?>

    <?php // echo $form->field($model, 'dues_amount') ?>

    <?php // echo $form->field($model, 'per_month_charges') ?>

    <?php // echo $form->field($model, 'no_of_months') ?>

    <?php // echo $form->field($model, 'total_amount_of_current_months') ?>

    <?php // echo $form->field($model, 'advanced_pay') ?>

    <?php // echo $form->field($model, 'arrears') ?>

    <?php // echo $form->field($model, 'arrears_period') ?>

    <?php // echo $form->field($model, 'total_bill') ?>

    <?php // echo $form->field($model, 'after_due_date_charges') ?>

    <?php // echo $form->field($model, 'total_after_due_date') ?>

    <?php // echo $form->field($model, 'balance_arears') ?>

    <?php // echo $form->field($model, 'remarks') ?>

    <?php // echo $form->field($model, 'water_charges_per_month') ?>

    <?php // echo $form->field($model, 'conservancy_charges_per_month') ?>

    <?php // echo $form->field($model, 'four_months_water_charges') ?>

    <?php // echo $form->field($model, 'four_months_conservancy_charges') ?>

    <?php // echo $form->field($model, 'billing_months') ?>

    <?php // echo $form->field($model, 'issue_date') ?>

    <?php // echo $form->field($model, 'due_date') ?>

    <?php // echo $form->field($model, 'amount_paid') ?>

    <?php // echo $form->field($model, 'payment_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
