<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Payment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'consumer_id',
            'total_months_conservancy',
            'water_arrears',
            'water_remarks',
            //'conservancy_amount',
            //'total_water_and_conservancy_amount',
            //'tinure',
            //'dues_amount',
            //'per_month_charges',
            //'no_of_months',
            //'total_amount_of_current_months',
            //'advanced_pay',
            //'arrears',
            //'arrears_period',
            //'total_bill',
            //'after_due_date_charges',
            //'total_after_due_date',
            //'balance_arears',
            //'remarks',
            //'water_charges_per_month',
            //'conservancy_charges_per_month',
            //'four_months_water_charges',
            //'four_months_conservancy_charges',
            //'billing_months',
            //'issue_date',
            //'due_date',
            //'amount_paid',
            //'payment_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
