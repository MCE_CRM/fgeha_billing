<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PlotDocument */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plot-document-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($model, 'flieNameId')->dropDownList(
    ArrayHelper::map(\app\models\EstateWing::find()->asArray()->all(), 'id', 'name')
    ) ?>
      <?= $form->field($model, 'document[]')->fileInput(['multiple'=> true]) ?>
    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
