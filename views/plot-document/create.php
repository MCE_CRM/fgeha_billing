<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PlotDocument */

$this->title = 'Create Plot Document';
$this->params['breadcrumbs'][] = ['label' => 'Plot Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plot-document-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php


use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use app\Controllers\PlotDocumentController;

/* @var $this yii\web\View */
/* @var $model app\models\Plot */
 
$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Plots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="plot-view">
     <div class="container-fluid">
     <div class="card shadow mb-4"> 
      <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary" style="padding-left: 40%">Bill History</h6>
      </div>
              <div class="card-body">
              <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                          <tr>
                                <th>Name</th>
                                <th>Document</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          //print_r($bills);
                          //die;
                          foreach ($bills as $val):
                            // print_r($val);
                            // die;
                          $tenure = \app\models\Plot::find()->where(['id'=>$val->plotId])->all();
                        
                          $result=json_decode($val['document']); 
                          //yaha apny documents ko get kr liya aik id ka against jitny bhi thy
                          //  echo "<pre>";
                          // print_r($result);
                          // exit;
                          $file = scandir('uploads'); //ya apky pas wo directory scan krta ha jaha ap files store krwaty ho
                          foreach ($tenure as $val1):?>
                          
                              <tr>
                                 
                                  <td><?php echo PlotDocumentController::getName($val['flieNameId'])?></td>
                                  
                                  <?php if (!empty($result)): ?>
                                    
                                  
                                     <td >  <?php   foreach($result as  $row):?><a  href="../uploads/<?php echo $row;?>"><?php echo $row;?></a> &nbsp&nbsp&nbsp&nbsp<?php endforeach; ?></td>
                                     <?php else:?>
                                        <!-- agr aik sy zayda document thy to foreach wala part chaly ga nai to else mai jo single document ha wo chaly ga -->
                                      <td><a  href="/billing/uploads/<?php echo $val['document']?>"><?php echo $val['document']?></a></td>
                                  <?php endif; ?> 
                              </tr>
                        </tbody>  
                                   <?php endforeach; ?>
                                   <?php endforeach; ?>   
                        </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
</div>
