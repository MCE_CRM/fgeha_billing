<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Modal; 



/* @var $this yii\web\View */
/* @var $model app\models\Plot */
/* @var $form yii\widgets\ActiveForm */
?>
<?php 
    if (yii::$app->session->hasFlash('danger')) {?>
        <div class="alert alert-danger">
   <strong ><?php echo yii::$app->session->getFlash('danger');?></strong> 
</div>
       
 <?php   }
?>

<?php 
    if (yii::$app->session->hasFlash('success')) {?>
        <div class="alert alert-success">
   <strong ><?php echo yii::$app->session->getFlash('success');?></strong> 
</div>
       
 <?php   }
?>
   
<div class="plot-form">

    <?php $form = ActiveForm::begin([
                'options' => [
                    'id' => 'Addsector'
                ]


    ]); ?>
    
    <div class="card">
        <div class="row">
            <div class="col-lg-12">
               

                <div class="row" style="margin: 10px;">
                    <div class="col-lg-6">
                        <?= $form->field($model, 'file_no')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-lg-6">
                        <?= $form->field($model, 'allottee_name')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-lg-6">
                         <?= $form->field($model, 'email')->input('email') ?>
                    </div>

                    <div class="col-lg-6">
                        <?= $form->field($model, 'cnic')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-lg-6">
                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                    </div>
                           <div class="col-lg-6">
                        <?= $form->field($model, 'covered_area')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'cat_id')->dropDownList(
                            ArrayHelper::map(\app\models\Category::find()->asArray()->all(), 'id', 'category_name')

                        ) ?>
                    </div>

                    <div class="col-lg-6">

                        <?= $form->field($model, 'sector')->dropDownList(
                            ArrayHelper::map(\app\models\Sectors::find()->asArray()->all(), 'id', 'sector_name'),
                            [   
                                'id'=>'sectorid',
                                'prompt'=>'Select sector',
                                'onchange'=>'$.post("\list?id='.'"+$(this).val(),function(data){
                                    $("select#plot-sub_sector"). html(data);
                                })'
                            ]);?>

                         <?= Html::a(' Add Sector',['sector/create'],['class'=>'btn btn-success btn-sm'])?>
                    </div>

                 
                   

                    <div class="col-lg-6">
                          <?= $form->field($model, 'sub_sector')->dropDownList(
                            ArrayHelper::map(\app\models\SubSectors::find()->asArray()->all(), 'name', 'name'),
                            [
                                'prompt'=>'Select sub-sector',
                            ]); ?>

                            <?= Html::a(' Add Sub-Sector',['subsector/create'],['class'=>'btn btn-success btn-sm'])?> 
                   </div>

                   <div class="col-lg-6">
                    <br>
                   <?= $form->field($model, 'services')->checkboxlist( ArrayHelper::map(\app\models\Services::find()->all(), 'id', 'service_name'));
                     
                    ?>
                    </div>

                    <div class="col-lg-6">
                    <?= $form->field($model, 'plot_type')->dropDownList([ 'Residential' => 'Residential', 'Commercial' => 'Commercial', ], ['prompt' => 'Select plot type']) ?>
                    </div>

                    <div class="col-lg-6">
                        <br>
                    <label for="date">Connection Date</label>
                     <input type="Date" class="form-control" name ="connection_date" id="date" value="<?=$model->connection_date?>">
                      
                    </div>


                    

                    <div class="col-lg-6">
                        <label for="date">Possession Date</label>
                    <input type="Date" class="form-control" name ="possession_date" id="date" value="<?=$model->possession_date?>">
                        
                    </div>

                    <div class="col-lg-6">
                        <?= $form->field($model, 'house_no')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-lg-6">
                        <?= $form->field($model, 'street_no')->textInput(['maxlength' => true]) ?>
                    </div>

                      <div class="col-lg-6">
                        <?= $form->field($model, 'balance')->textInput(['maxlength' => true]) ?>
                    </div>

                      <div class="col-lg-6">
                        <?= $form->field($model, 'debit')->textInput(['maxlength' => true]) ?>
                    </div>
 
                     <div class="col-lg-6">
                        <?= $form->field($model, 'instalment')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-lg-6">
                        <?= $form->field($model, 'no_of_installement')->dropDownList([ '1' => '1', '2' => '2','3' => '3', '4' => '4','5' => '5','6' => '6', '7' => '7', ], ['prompt' => 'Select Months']) ?>
                    </div>


                    <div class="col-lg-6">
                        <?= $form->field($model, 'reference_no')->textInput(['maxlength' => true]) ?>
                    </div>

                   <!--  <div class="col-lg-6">
                        <?= $form->field($model, 'consumer_no')->textInput(['maxlength' => true]) ?>
                    </div> -->
                </div>

            </div>
        </div>
    </div>

    <div class="form-group" style="margin: 10px"; >
        <?= Html::submitButton('Save', ['class' => 'btn btn-success pull-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
