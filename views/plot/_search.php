<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Bill;
use app\models\Plot;
use app\models\BillSearch;
use app\models\Sectors;
use app\models\SubSectors;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PlotSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plot-search"> 

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="card">
        <div class="row">
            <div class="col-lg-12">

                <div class="row" style="margin: 10px;">

                    <?php //echo $form->field($model, 'id') ?>

                    <div class="col-lg-2">

                    <?= $form->field($model, 'file_no') ?>

                      </div>

                  <div class="col-lg-2">

                    <?= $form->field($model, 'cat_id') ?>

                      </div>

                      <div class="col-lg-2">

                     <?= $form->field($model, 'allottee_name') ?>

                       </div>
 
                    <?php //echo $form->field($model, 'connection_date') ?>

                    <?php //echo $form->field($model, 'created_by') ?>

                    <div class="col-lg-2">

            <?= $form->field($model, 'sector')->dropDownList( ArrayHelper::map(Plot::find()->all(),'sectorName.sector_name', 'sectorName.sector_name'), ['prompt'=>'All'])?>

                       </div>
 
                    <div class="col-lg-2">

                    <?= $form->field($model, 'sub_sector')->dropDownList( ArrayHelper::map(bill::find()->all(),'subsectorName.name', 'subsectorName.name'), ['prompt'=>'All'])?>

                        </div>

                    <?php // echo $form->field($model, 'possession_date') ?>

                    <?php // echo $form->field($model, 'end_date') ?>

                    <?php // echo $form->field($model, 'created_date') ?>

                    <div class="col-lg-2">

                    <?php  echo $form->field($model, 'house_no') ?>

                      </div>
                          <div class="col-lg-2">

                    <?php  echo $form->field($model, 'street_no') ?>

                      </div>
                   

                    <?php // echo $form->field($model, 'street_no') ?>

                    <?php // echo $form->field($model, 'reference_no') ?>

                    <?php // echo $form->field($model, 'consumer_no') ?>

    <div class="col-lg-2">
        <br>
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>
     </div>
      </div>
       </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
