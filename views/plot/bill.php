<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Plot */
 
$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Plots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="plot-view">
     <div class="container-fluid">
     <div class="card shadow mb-4"> 
      <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary" style="padding-left: 40%">Bill History</h6>
      </div>
              <div class="card-body">
              <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                          <tr>
                                <th>Name</th>
                                <th>Water Ch</th>
                                <th>Conservancy Ch</th>
                                <th>Arrears</th>
                                <th>Total Bill</th>
                                <th>Payment</th>
                                <th>Document</th>
                               
                                <th>Status</th>
                                   <!--<th>Action</th>-->
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          foreach ($model as $val):
                          $tenure = \app\models\Plot::find()->where(['id'=>$val->consumer_id])->all();
                          $result=json_decode($val['document']);
                          //  echo "<pre>";
                          // print_r($result);
                          // exit;
                          

                          $file = scandir('uploads');
                          foreach ($tenure as $val1):?>
                              <tr>
                                  <td><?= $val1['allottee_name']?></td>
                                  <td><?= $val['water_bill']?></td>
                                  <td><?= $val['conservancy_amount']?></td>
                                  <td><?= $val['arrears']?></td>
                                  <td><?= $val['total_bill']?></td>
                                  <td><?= $val['payment']?></td>
                                  
                              
                                  <?php if (!empty($result)): ?>
                                    
                                  
                                     <td >  <?php   foreach($result as  $row):?><a  href="/uploads/<?php echo $row;?>"><?php echo $row;?></a> &nbsp&nbsp&nbsp&nbsp<?php endforeach; ?></td>
                                     <?php else:?>
                                      <td><a  href="/uploads/<?php echo $val['document']?>"><?php echo $val['document']?></a></td>
                                  <?php endif; ?>
                                  <td><?php if($val['status']==1): ?>
                                      <?php echo "paid";?>
                                  <?php else: echo "pending"; ?>    
                                  <?php endif; ?>
                                  </td>  
                              </tr>
                        </tbody>  
        <!--  <?php  [
          'class' => 'yii\grid\ActionColumn',
            'header'=>'Action',
            'headerOptions' => ['width' => '80'],
            
            'template' => '{View}{Update}{Delete}',

            'buttons'=> [
                'View' => function ($url,$model,$key){
                    return html::a('<i class="fa fa-eye" style="font-size:16px;color:#00cc66"></i>',url::to(['/plot/view?id='.$model->id]));
                    },
                              
               ],
            ]
        ?>
 -->

                                  
                                   <?php endforeach; ?>
                                   <?php endforeach; ?>   
                        </table>
                        
                            </div>
                        </div>
                    </div>

                </div>
            </div>

    
