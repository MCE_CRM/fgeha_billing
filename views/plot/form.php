<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Modal;



/* @var $this yii\web\View */
/* @var $model app\models\Plot */
/* @var $form yii\widgets\ActiveForm */
?>

    
<div class="plot-form">

    <?php $form = ActiveForm::begin([
                'options' => [
                    'id' => 'Addsector'
                ]


    ]); ?>
     <?= $form->field($model, 'document')->fileInput() ?>
                <?= Html::submitButton('import-excel', ['class' => 'btn btn-success pull-right']) ?>
    
    <div class="form-group" style="margin: 10px"; >
        <?= Html::submitButton('Save', ['class' => 'btn btn-success pull-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
