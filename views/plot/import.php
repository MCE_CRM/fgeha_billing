<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html; 
use yii\bootstrap\ActiveForm;

$this->title = 'file Upload';
$this->params['breadcrumbs'][] = $this->title;
?>

        <?php $form = ActiveForm::begin([
                'id' => 'search-form',
                'action'=>'/billing/web/plot/import-excel'
                
            
            ]); ?>


<?= $form->field($model, 'document')->fileInput() ?>

 <?= Html::submitButton('Upload', ['class' => 'btn btn-success pull-right']) ?>
<?php ActiveForm::end() ?>