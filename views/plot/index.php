  <?php
ini_set('memory_limit', '-1');
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;  
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use app\models\Plot;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Plots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bill-index">
  

    <div class="card-header py-3 panel" style="">
       
        <?= Html::a('+ Add consumer', ['create'], ['class' => 'btn btn-success']) ?>
       

          <?= Html::a('Import Excel File', ['import'], ['class' => 'btn btn-success']) ?>
         

       
          

        
    </div>
  <?php  echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        // ['class' => 'yii\grid\SerialColumn'],

         'file_no',
            //'created_by',
            'allottee_name',
             [    
                'attribute'=>'cat_id',
                'value'=>'categoryName.category_name',
            ],
            'balance',
            'debit',

            // 'cat_id',
        //   [
        //     'header'=>'Sector',
        //   'attribute'=> 'sectorName.sector_name',
        // ],
            // 'Sector.sector_name',
           // 'possession_date',
            //'end_date',
            //'created_date',
            'house_no',
            'street_no',
            'sub_sector',
            'plot_type',
            'covered_area',
          

            //'reference_no',
            //'consumer_no',
            [
                'header' =>'History',
                'format' => 'raw',
               // 'visible'=> (int)Yii::$app->user->can('payments/createvoucher'),
                'value' => function($model, $key, $index, $column) {
                    return Html::a(
                         '<i class="fa fa-server"></i>',
                        Url::to(['plot/bill', 'id' => $model->id]),
                        [
                            'id'=>'grid-custom-button',
                            'data-pjax'=>true,
                            'action'=>Url::to(['plot/bill', 'id' => $model->id]),
                            'class'=>'button btn btn-default',
                            'title'=>"History Bill",
                        ]
                    );
                },
            ],      

            ['class' => 'yii\grid\ActionColumn',
            'header'=>'Action',
            'headerOptions' => ['width' => '80'],
            
            'template' => '{View}{Update}{Delete}',

            'buttons'=> [
                'Delete' => function ($url,$model,$key){
                    return html::a('<i class="fa fa-trash" style="font-size:16px;color:#F05550 "></i>',url::to(['/plot/delete?id='.$model->id]));
                },
                'View' => function ($url,$model,$key){
                    return html::a('<i class="fa fa-eye" style="font-size:16px;color:#00cc66"></i>',url::to(['/plot/view?id='.$model->id]));
                },
                'Update' => function ($url,$model,$key){
                    return html::a('<i class="fa fa-edit" style="font-size:16px;color:#4A94D2 "></i>',url::to(['/plot/update?id='.$model->id]));
                }
            ]
        ],
    ],
]); 
?>

</div>
