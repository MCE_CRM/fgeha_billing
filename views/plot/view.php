<?php
 
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Plot */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Plots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="plot-view">
    <?php 
    if (yii::$app->session->hasFlash('success')) {?>
        <div class="alert alert-success">
   <strong ><?php echo yii::$app->session->getFlash('success');?></strong> 
</div>
       
 <?php   }
?>
    

<!--     <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'file_no',
            'connection_date',
            'allottee_name',
            'cnic',
            'phone',
            'email',
            'balance',
            'sectorName.sector_name',
            'sub_sector',
            'possession_date',
            'end_date',
            'created_date',
            'house_no',
            'street_no',
            'consumer_no',
        ],
    ]) ?>

</div>
