<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\sectors */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sectors-form">
	    <?php 
$form = ActiveForm::begin([
    'action' => ['create'],
    'options' => [
        'class' => 'comment-form'
    ]
]); 
?>


    <?= $form->field($model, 'sector_name')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
