<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\sectorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sectors-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="card">
        <div class="row">
            <div class="col-lg-12">

                <div class="row" style="margin: 10px;">

                  

                          <div class="col-lg-2">

                        <?= $form->field($model, 'sector_name') ?>

                          </div>


                     <div class="col-lg-2">

                        <?= $form->field($model, 'created_by') ?>

                    </div>

                 
                     <div class="col-lg-2"></div>
                      <div class="col-lg-2"></div>
                       <div class="col-lg-2"></div>
                        <div class="col-lg-2"></div>


                        <?php // echo $form->field($model, 'updated_by') ?>

     <div class="col-lg-2">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>
      </div>
  </div>
</div>
</div>

    <?php ActiveForm::end(); ?>

</div>
