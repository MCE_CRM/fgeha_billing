<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\sectors */

$this->title = Yii::t('app', 'Create Sectors');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sectors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sectors-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
