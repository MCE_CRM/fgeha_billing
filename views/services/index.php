<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\servicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Services', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
             

            // 'id',
            'service_name',
            'charges',
          
            [
                'attribute'=>'created_on',
                'format' => ['date', 'php:d-m-Y']
            ],
             'un.username',
            //'updated_on',
            //'name.username',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
