<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="site-login">
    <h1><?/*= Html::encode($this->title) */?></h1>

    <p>Please fill out the following fields to login:</p>

    <?php /*$form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); */?>

        <?/*= $form->field($model, 'username')->textInput(['autofocus' => true]) */?>

        <?/*= $form->field($model, 'password')->passwordInput() */?>

        <?/*= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ]) */?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?/*= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) */?>
            </div>
        </div>

    <?php /*ActiveForm::end(); */?>

    <div class="col-lg-offset-1" style="color:#999;">
        You may login with <strong>admin/admin</strong> or <strong>demo/demo</strong>.<br>
        To modify the username/password, please check out the code <code>app\models\User::$users</code>.
    </div>
</div>-->

        <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                
            
            ]); ?>




    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-5 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">FGEHA WBMS</h1>
                                </div>
                                
                                    <div class="form-group">
                                        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                                    </div>

                                     <?= $form->field($model, 'password')->passwordInput() ?>


                                   
                                    <div class="form-group">
                                         <div class="custom-control custom-checkbox small">
                                             <?= $form->field($model, 'rememberMe')->checkbox([]) ?>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <div class="col-lg-offset-1 col-lg-11">
                                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-user btn-block', 'name' => 'login-button']) ?>
                                        </div>
                                    </div>
                                     <div class="text-center">
                                          <a class="small" href="forgot-password.html">Forgot Password?</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  <?= Html::a('printbill', ['/site/print-bill'], ['target'=>'printbill']);?>

                                </div>

                                 <?php ActiveForm::end(); ?>
                                <hr>
                                <div class="text-center">
                                   
                                </div>

                               

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


