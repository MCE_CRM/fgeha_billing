<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

        <?php $form = ActiveForm::begin([
                'id' => 'search-form',
                'action'=>'/billing_server/web/site/bill-print'
                
            
            ]); ?>




    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-5 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">FGEHA WBMS</h1>
                                </div>
                                

                                <div class="form-group">
                                        <input type="text" class="form-control" name="searchbill" placeholder="Enter File No">

                                    </div>
                                   
                                    <div class="form-group">
                                        <div class="col-lg-offset-1 col-lg-11">
                                            <?= Html::submitButton('BillPrint', ['class' => 'btn btn-success btn-user btn-block ' , 'name' => 'search-button']) ?>
                                        </div>
                                    </div>
                                     

                                 <?php ActiveForm::end(); ?>
                                
                               

                               

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


