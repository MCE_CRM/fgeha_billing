<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Modal; 

/* @var $this yii\web\View */
/* @var $model app\models\Plot */

$this->title = 'Profile';
$this->params['breadcrumbs'][] = ['label' => 'site', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="plot-view">


   
     <div class="container-fluid">

                    <!-- Page Heading -->
                    
                   
     <div class="card shadow mb-4">
                        <div class="card-header py-3">

                            <h6 class="m-0 font-weight-bold text-primary">
                              <?= Html::a('+ Add User',['create'],['class'=>'btn btn-success'])?></i>
                              <strong style="padding-left: 40%">Profile Details</strong></h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                                <!-- <th>  id </th> -->
                                                <th>  username</th>
                                                <th>  email </th>
                                                <!-- <th> status</th> -->
                                                <th> Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                        <?php 
                    
                    foreach ($model as $val):
            //               echo "<pre>";
            // print_r($val);
            // exit();
                        ?>

                        <tr>    
                                 
                                <td><?= $val->username?></td>
                                <td><?= $val->email?></td>
                                <td><?= Html::a('Update',['userupdate', 'id'=> $val->id],['class'=>'btn btn-success'])?><!-- |
                                    <?=html::a('Delete',url::to(['/site/user?id='.$val->id]),['class'=>'btn btn-danger'])?> -->
                                   
                                </td>
                                
                        </tr>
                    </tbody>    
                               <?php endforeach; ?>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

    