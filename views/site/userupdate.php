<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\subsectors */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="">

    <?php $form = ActiveForm::begin(); ?>
 
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>


     <?= $form->field($model, 'email')->input('email') ?>

     <?= $form->field($model, 'password_hash')->passwordInput() ?>

     <div class="form-group" style="margin: 10px"; >
        <?= Html::submitButton('Userupdate', ['class' => 'btn btn-success pull-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
