<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Sectors;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\subsectors */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subsectors-form">

        <?php 
$form = ActiveForm::begin([
    'action' => ['create'],
    'options' => [
        'class' => 'comment-form'
    ]
]); 
?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sector_id')->dropDownList(ArrayHelper::map(\app\models\Sectors::find()->asArray()->all(), 'id', 'sector_name')) ?>
                    


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
