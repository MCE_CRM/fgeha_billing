<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\subsectors */

$this->title = Yii::t('app', 'Create Subsectors');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Subsectors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subsectors-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
