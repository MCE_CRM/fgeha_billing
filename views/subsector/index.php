<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
 use yii\bootstrap\Modal;
 use app\models\Subsectors;

/* @var $this yii\web\View */
/* @var $searchModel app\models\subsectorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Subsectors');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subsectors-index">

    <h1><?= Html::encode($this->title) ?></h1>

   
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php
 
            Modal::begin([

                'toggleButton' => [

                    'label' => '<i class="glyphicon glyphicon-plus"></i> + Add Subsectors',

                    'class' => 'btn btn-success',
                      'id' =>'modalButton',

                ],

                'closeButton' => [

                  'label' => 'Close',

                  'class' => 'btn btn-danger btn-sm pull-right',


                ],

                'size' => 'modal-md',


            ]);

            $myModel = new Subsectors();

            echo $this->render('create', ['model' => $myModel]);

            Modal::end();

        ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'header'=>'Subsector',
            'attribute'=>'name',
        ],
            // 'sector_id',
            'un.username',
             [
                'attribute'=>'created_on',
                'format' => ['date', 'php:d-m-Y']
            ],
            //'name.username',
            //'updated_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
