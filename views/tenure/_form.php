<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; 

/* @var $this yii\web\View */
/* @var $model app\models\Tenure */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenure-form">

    <?php $form = ActiveForm::begin(); ?> 

    <?= $form->field($model, 'tenure_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 't_interval')->textInput(['maxlength' => true]) ?>

    

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
