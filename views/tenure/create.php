<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tenure */

$this->title = Yii::t('app', 'Create Tenure');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tenures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenure-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
