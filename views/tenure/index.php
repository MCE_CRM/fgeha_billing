<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TenureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tenures');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenure-index"> 

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Tenure'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
           
 
            'id',
            'tenure_name',
            't_interval',
            [
                'attribute'=>'created_on',
                'format' => ['datetime', 'php:d-m-Y H:i:s']
            ],
             'un.username',
             
            // 'updated_on',
            // 'name.username',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
